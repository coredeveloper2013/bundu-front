DP Market version 2.01
Copyright 2001 DanPromote


1) License information

2) Terms of use

3) Before uploading

4) Where to put the files

5) Adapting the script

6) Adding and removing categorys

7) Adding input-fields

8) Contact information


----- 1 - License information -----

DP Market is free for use on your personal non-commercial web site. Using the script on a professionel and/or commercial site requires a commercial license obtained from DanPromote - the price is currently (january 2001) 30 USD.

Using the script on a professionel and/or commercial site without obtaining a commercial license will result in legal steps towards the responsible.

----- 2 - Terms of use -----

This script is provided "as is". This means that DanPromote can not be held responsible for any failure or malfunction what so ever.

If you don't accept or understand these terms - don't use the script !

----- 3 - Before uploading -----

Before uploading anything check the few things:

In the file show.pl:

	Set the line #!/usr/bin/perl according to your server.

In the file update.pl:

	Set the line #!/usr/bin/perl according to your server.

	If you want notification:
	  check the path to sendmail - something like
	  $mailprogram = '/usr/sbin/sendmail';

	  Type in the receiving mail address:
	  $notify_email = 'webmaster@yoursite.com';

	  Turn on notification:
	  $donotify="yes";

	Decide how many days postings should live:
	  $livedays=31; (could be any number of days)

----- 4 - Where to put the files -----

Now it's time to get the script 'on the air' !

These files should be uploaded in a directory af your choice:

	addapost.html	(contains form for adding posts)
	index.html	(main file with link to showing and adding posts)
	updated.html	(confirmation on adding a new post)

These files should be uploaded in a subdirectory named "cgi-bin":

	buying.txt	(example datafile)
	counter.txt	(holds the ID of last post)
	selling.txt	(example datafile)
	
	show.pl		(script to display posts)
	template.html	(template for show.pl)
	update.pl	(script to add a new post)

In "cgi-bin" chmod the .txt-files to "666" and the .pl-files to "755"

Point your browser to index.html and DP Market is on the run.

----- 5 - Adapting the script -----

In the main directory of your market place you should edit the .html-files to look nice and fit your existing site. All files are kept in standard html, so you should have no problem editing them.

To change the appearence of postings you should edit the template.html file in the "cgi-bin" directory. This file is also kept in standard html, but contains a number of special tags used by show.pl to insert the data in the correct place. Here's a list of possible tags and their use:

##CATEGORY##	Is replaced by the selected category

##LOOP-START##	Marks the starting point of the list of postings
		If you are using a table for holding the output,
		placing this tag the correct place is very important.
		Pease note: This tag should be sorrounded by <!--- and --->

##LOOP-END##	Marks the ending point of the list of postings.
		Pease note: This tag should be sorrounded by <!--- and --->

##HEADLINE##	Is replaced by the headline of the post.

##TEXT##	Is replaced by the text of the post.

##DD##		The day part of date of the post.
##MM##		The month part of date of the post.
##YY##		The year part (2 digits) of date of the post.
##YYYY##	The year part (4 digits) of date of the post.
		Examples on displaying dates could be:
		##MM##/##DD##/##YY##    -  12/31/99
					-  1/1/00
		##MM##/##DD##/##YYYY##  -  12/31/1999
					-  1/1/2000

		Information on who added the post:

##NAME##	The name

##TELEPHONE##	The phone number (if entered)

##EMAIL##	The amil address (if entered)
		To have a mail-link use the following form:
		<a href="mailto:##EMAIL##">##EMAIL##</a>

----- 6 - Adding and removing categorys -----

To add a new category (for example "jobs") edit the file addapost.html. In the form section you should add aa "<option>Jobs" between <select name="category"> and </select>

Second you should add a link to "cgi-bin/show.pl?jobs" on the index.html file.

Third create an empty text-file and save/upload this as "jobs.txt" in the "cgi-bin" directory and chmod it to "666" (read/write all)

Note: If the name of the new category contains blank spaces (like "cars and bikes") the like to show.pl should be: "cgi-bin/show.pl?cars_and_bikes" and the name of the text-file should be "cars_and_bikes.txt". The option on addapost.html although should be "cars and bikes".

To remove a category (for example "buying"), simply remove the link to "cgi-bin/show.pl?buying" from index.html and delete the file buying.txt in the "cgi-bin" directory.

----- 7 - Adding input-fields -----

In the addapost.html file, you are able to add up to 5 more fields, that can be used freely for any extra information, you want from the user. The extra fields should be named "res1", "res2", "res3", "res4" and "res5" - remember no capitals !

To have the script DISPLAY the information in these fields, you also need to modify the template.html file. In this file you may add one of more of the following tags (must be in capitals):

##RES1## ##RES2## ##RES3## ##RES4## ##RES5## 

----- 8 - Contact information -----

To get in touch with DanPromote send an email to: info@danpromote.dk

Please accept that even though we try hard to get back to you as soon as possible our answer could take up to a couple of days.

Also remember that our support forum and faq section can be found at:

www.danpromote.dk/int/

