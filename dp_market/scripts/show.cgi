#!/usr/bin/perl
####################################################################
#
# Show.pl
#
# Displays postings in the category set in the URL (show.pl?category)
#
# License and contact information can be found at www.danpromote.dk/int/
#
# Copyright 2001 DanPromote
# Last modified 01/17/2001
####################################################################

# Gets the category from the URL and sets the filename
  $category = lc($ENV{QUERY_STRING});
  $catprint = $ENV{QUERY_STRING};
  $catprint =~ s/_/ /g;

# Reads the template
  $tplfile="template.html";
  open(FILE,"$tplfile") || die "Cant open $tplfile !\n";
  @TEMPLATE = <FILE>;
  close(FILE);

# Locates ##LOOP-START## and ##LOOP-END##
  $tempsize=@TEMPLATE;
  for($n=0; $n<$tempsize; $n++) {
    $line=$TEMPLATE[$n];
    if($line =~ /##LOOP-START##/i)    {
        $loopstart=$n;                }
    if($line =~ /##LOOP-END##/i)      {
        $loopend=$n;                  }
                            }

# prints the top of the page
  print "Content-type: text/html\n\n";
  for($n=0; $n<$loopstart; $n++)    {
    $line=$TEMPLATE[$n];
    $line =~ s/##CATEGORY##/$catprint/gi;
    print $line;                    }

# Reads from datafile
  $filename = $category . ".txt";
  open(FILE,"$filename") || die "Cant open $filename !\n";
  @DATA = <FILE>;
  close(FILE);
  $datasize=@DATA;

# Loops throug the records
  for($n=0; $n<$datasize; $n++)    {
    $line=$DATA[$n];
    chop($line);
    @LP=split(/\|/, $line);
    $ID=$LP[0];
    $time=$LP[1];
    $headline=$LP[2];
    $text=$LP[3];
    $name=$LP[4];
    $telephone=$LP[5];
    $email=$LP[6];
    $res1=$LP[7];
    $res2=$LP[8];
    $res3=$LP[9];
    $res4=$LP[10];
    $res5=$LP[11];

# Calculates DAY, MONTH & YEAR (DD MM YY / YYYY)
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
    $dd = $mday;  
    $mm = $mon+1;
    $yyyy = $year + 1900;
    $yy = substr($yyyy,2,2);
    
# Prints this record looping throug the template
    for($tpline=$loopstart; $tpline<$loopend; $tpline++) {
        $line=$TEMPLATE[$tpline];
        $line =~ s/##DD##/$dd/gi;
        $line =~ s/##MM##/$mm/gi;
        $line =~ s/##YY##/$yy/gi;
        $line =~ s/##YYYY##/$yyyy/gi;
        $line =~ s/##CATEGORY##/$catprint/gi;
        $line =~ s/##HEADLINE##/$headline/gi;
        $line =~ s/##TEXT##/$text/gi;
        $line =~ s/##NAME##/$name/gi;
        $line =~ s/##TELEPHONE##/$telephone/gi;
        $line =~ s/##EMAIL##/$email/gi;
        $line =~ s/##RES1##/$res1/gi;
        $line =~ s/##RES2##/$res2/gi;
        $line =~ s/##RES3##/$res3/gi;
        $line =~ s/##RES4##/$res4/gi;
        $line =~ s/##RES5##/$res5/gi;
        print $line;                                    }

                                                                }

# Prints the rest of the page
  for($n=$loopend; $n<$tempsize; $n++)  {
    $line=@TEMPLATE[$n];
    $line =~ s/##CATEGORY##/$catprint/gi;
    print $line;                        }

exit;