#!/usr/bin/perl
####################################################################
#
# Update.pl
#
# Saves new advert to the selected category
# Redirects to updated.html
#
# License and contact information can be found at www.danpromote.dk/int/
#
# Copyright 2001 DanPromote
# Last modified 01/17/2001
####################################################################

# Points $mailprogram to sendmail - used to send notificaiton
  $mailprogram = '/usr/sbin/sendmail';

# Where should notification be send ?
  $notify_email = 'yellowstonelodging@yellowstonelodging.biz';

# Should notification be send (yes/no) ?
  $donotify="no";

# How many days should new postings "live" ?
  $livedays=999;

# Changing anything below this line is not recommened

# Calculates todays number
  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
  $today=$yday;

# Gets the data from the form
  &parse_form;
  $headline = $FORM{'headline'};
  $text = $FORM{'text'};
  $category = lc($FORM{'category'});
  $category =~ s/ /_/g;
  $name = $FORM{'name'};
  $telephone = $FORM{'telephone'};
  $email = $FORM{'email'};
  $res1 = $FORM{'res1'};
  $res2 = $FORM{'res2'};
  $res3 = $FORM{'res3'};
  $res4 = $FORM{'res4'};
  $res5 = $FORM{'res5'};

# Calls subroutine to validate the input
  &validate;

# Opens the counter-file and finds the last ID
  $countfile = "counter.txt";
  open(COUNT,"$countfile") || die "Cant open $countfile !\n";
  $ID = <COUNT>;
  close(COUNT);

# Gets the next ID and saves the counter
  $ID++;
  open(COUNT,">$countfile") || die "Cant open $countfile !\n";
  print (COUNT "$ID\n");
  close(COUNT);

# Converts < to &lt; to avoid abuse (<script etc)
  $headline =~ s/</\&lt;/g;
  $text =~ s/</\&lt;/g;
  $name =~ s/</\&lt;/g;
  $telephone =~ s/</\&lt;/g;
  $email =~ s/</\&lt;/g;
  $res1 =~ s/</\&lt;/g;
  $res2 =~ s/</\&lt;/g;
  $res3 =~ s/</\&lt;/g;
  $res4 =~ s/</\&lt;/g;
  $res5 =~ s/</\&lt;/g;

# Converts linebreaks (\n) to <br>
  $text =~ s/\cM\n/<br>/g;

# Reads existing posts 
  $filename = $category . ".txt";
  open (FILE,"$filename") || die "Cant open $filename\n";
  @LINES=<FILE>;
  close(FILE);
  $SIZE=@LINES;

# Writes the new post and then the existing posts
  open (FILE,">$filename") || die "Cant open $filename\n";

  # New post
  $newline=$ID."|".(time)."|".$headline."|".$text."|".$name."|".$telephone."|".$email."|".$res1."|".$res2."|".$res3."|".$res4."|".$res5."\n";
  print FILE $newline;

  # Existing posts
  for ($i=0;$i<=$SIZE;$i++) 	 {
    $post = $LINES[$i];
    @parts=split(/\|/,$post);
    $postdate=$parts[1];
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($postdate);
    $daysold=int((time-$postdate)/60/60/24);
    if($daysold <= $livedays)        {
        print FILE $post;            }
                               }
  close(FILE);

# Calls notification-routine if selected
  if(lc($donotify) eq "yes")	{
	&notify;	            }

# Redirects to updated.html

print "Location: ../updated.html\n\n";
exit;


sub validate {

  $errors="";

  if(length($headline) == 0)	{
    $errors=$errors . "Please enter a headline !\\n";
                              }

  if(length($text) == 0)	{
    $errors=$errors . "Please enter a text !\\n";
                              }

  if(length($category) == 0)	{
    $errors=$errors . "Please select a category !\\n";
                              }

  if(length($name) == 0){
    $errors=$errors . "Please enter your name !\\n";
                              }

  if((length($telephone) == 0) && (length($email) == 0))	{
    $errors=$errors . "Please enter either telephone or email !\\n";
                              }

  if ($errors ne "")	{
    print "Content-type: text/html\n\n";
    print "<html>\n";
    print "<head>\n";
    print "<title>DP Market</title>\n";
    print "</head>\n";
    print "<body>\n";
    print "<script language=\"JavaScript\">\n";
    print "{\n";
    print "alert(\"$errors\");\n";
    print "history.back();\n";
    print "}\n";
    print "</script>\n";
    print "</body></html>";
    exit;                }

}


sub notify {

if($email ne "")   {
    $from = $email; }
else                {
    $from = $notify_email;
                    }

open(MAIL, "|$mailprogram -t") || die "Cant open $mailprogram\n";
print MAIL "From: $from\n";
print MAIL "To: $notify_email\n";
print MAIL "Subject: $header\n\n";

print MAIL "$text\n\n";
print MAIL "$category\n\n";
print MAIL "$name - $telephone - $email\n\n";
close(MAIL);
}


sub parse_form {

   read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
   if (length($buffer) < 5) {
         $buffer = $ENV{QUERY_STRING};
    }
 
  @pairs = split(/&/, $buffer);
   foreach $pair (@pairs) {
      ($name, $value) = split(/=/, $pair);

      $value =~ tr/+/ /;
      $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;

      $FORM{$name} = $value;
   }
}
