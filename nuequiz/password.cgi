#!/usr/bin/perl

# (c) NueDream Inc. 2000-2001
use CGI::Carp qw(fatalsToBrowser);

##### CHANGE THESE VARIABLES #####

# This is the password you are going to use to administrate your quiz.
# Try to use a password that is not easy for crackers to guess. For
# example your name is a bad pasword. Also try not to use spaces.

$password = "nuedream";                                         # Change this to be your password.
$editurl = "http://www.yellowstonelodging.biz/nuequiz/edit.cgi";  # Location of edit.cgi file.

###### DONT CHAGE THIS CODE ######

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {  
	($name, $value) = split(/=/, $pair);
  	$value =~ tr/+/ /;
  	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
  	$value =~ s/~!/ ~!/g;  
	$FORM{$name} = $value;
}

if ($FORM{'pass'} eq $password) {
	print "Location: $editurl\n\n";
} else {
	print "Content-type:text/html\n\n";
	print "Invalid Password! Please go back and try again!";
}

############## END ###############
