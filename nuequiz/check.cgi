#!/usr/bin/perl
use CGI::Carp qw(fatalsToBrowser);

# (c) NueDream Inc. 2000-2001 (www.nuedream.com)

##############    GLOBAL VARS AND CONSTS    ############

$file     = "questions.db"; # This is the name of your questions file.
$quizname = "NueQuiz";      # This is what appears in the TITLE.

############   THESE ARE OPTIONAL CHANGES    ###########

$spacer    = '|'; # leave it unless you already have a database
$anticheat = 1;   # Enables or disables the anti-cheating feature (1-Enabled, 0-Disabled)  .     

##############   GET INFO   ############

$content = $ENV{'QUERY_STRING'};
($temp, $temp2) = split(/&/, $content);
($question, $answer) = split(/=/, $temp);
($type, $question) = split(/=/, $temp2);

$start = $question+1;
$url   = "quiz.cgi?$type=$start";
$tempv = "";

(@cookies) = split(/;/, $ENV{'HTTP_COOKIE'});

foreach $crum (@cookies) {
	($name, $value) = split(/=/, $crum);
	if($name =~ /$type/) {
		$tempv = $value;
		last;
	}
}

##############    MAIN PROGRAM    ############

open(DATA,$file) or dienice("Couldn't open $file :: $!\n");
#flock(DATA,0); # uncomment this line if using unix based server
seek(DATA,0,0);
@data = <DATA>;
close(DATA);

foreach $line (@data) {
 	chomp($line);
 	($quiz,$num,$q,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\|/,$line);
	if ($quiz eq $type && $question eq $num) {
 		if($tempv ne "") {
			if ($tempv =~ /$question/) {
		    	if($anticheat == 1) {
					print "Content-type: text/html\n\n";
            		print "<br><br><center><font face=\"verdana\" size=\"2\"><b>You Cheated!!!</b><br><br>If you did not try to cheat, try again. This time do not double click the next button.<br> Click once and wait until the next question is fully displayed before clicking again.</font></center>";
				} else {
					print "Location: $url\n\n";
				}
			} else {
                $tempcookie = "$tempv" . "-" . $question . "_" . $answer . $ans;
		        print "Set-cookie: $type=$tempcookie\n";
				print "Location: $url\n\n";
			}
		} else {
			$tempcookie = $question . "_" . $answer . $ans;
			print "Set-cookie: $type=$tempcookie\n";
			print "Location: $url\n\n";
		}
	}
}