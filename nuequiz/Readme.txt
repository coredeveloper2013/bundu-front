
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+      -= NueDream Inc. Presents =-         +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+ NueQuiz - Version 2.21 (April 14, 2001)   +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+ I.   Software Agreement                   +
# II.  How to install                       #
+ III. Known bugs                           +
# IV.  Disclaimer                           #
+ V.  Contact                               +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+ (I.) -= Software Agreement =-             +
#                                           #
+ This software is FREE, it may be used on  +
# any web site and must include a link back #
+ to NueDream's website www.nuedream.com.   +
# NueQuiz may also be distributed on any    #
+ site as long as permission obtained from  +
# NueDream first. Please do not e-mail us   #
+ asking for help using the quiz.           +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+ (II.) -= How to install =-                +
#                                           #
+ 1. Unzip all the files into any           +
#    directory.                             #
+ 3. Open up quiz.cgi, edit.cgi, check.cgi  +
#    and change the $quizname variable to   #
+    the title of the quiz.                 +
# 4. Upload all the cgi files into your     #
+    cgi-bin and CHMOD them to 755.         +
# 5. Upload the questions.db to your cgi-bin#
+    directory and CHMOD the file to 777.   +
# 6. Upload the password.htm to any         #
+    directory. This is $passurl in edit.cgi+
# 7. Change all the required variables in   #
+    the password.cgi and edit.cgi          +
#    files. Make sure you don't leave them  #
+    out.                                   +
# 8. Create some questions in 1 of the 3    #
+    categories (Beginner, Novice, Expert). +
# 9. You can access the quiz by typing in   #
+    the location of your CGI bin, then     +
#    the name of the quiz file (quiz.cgi)   #
+    then the type of quiz followed by the  +
#    first question. Example:               #
+    www.abc.com/cgi-bin/quiz.cgi?expert=1  +
#    This will display the first question   #
+    of the expert quiz.                    +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+ (III.) -= Known Bugs =-                   +
#                                           #
+ 1. You cannot put quotation marks ," ",   +
#    or vertical bars , | , into the        #
+    question. If you do the quiz will not  +
#    function properly and will require you #
+    to manually fix the problem by         +
#    deleting the | or the "" from the      #
+    database.                              +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+ (IV.) -= Disclaimer =-                    +
#                                           #
+ By using NueQuiz you agree that NueDream  +
# will not be held responsible for any      #
+ aspect of privacy and security. Our       +
# product is secure to the best of our      #
+ knowledge, however it may contain flaws.  +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
#                                           #
+ (V.) -= Contact =-                        +
#                                           #
+ NueDream Inc. (www.nuedream.com)          +
#  - info@nuedream.com                      #
+                                           +
# NueQuiz (www.nuedream.com/nuequiz/)       #
+  - nuequiz@nuedream.com                   +
#                                           #
+ NueQuiz PRO (N/A)                         +
#  - nuequizpro@nuedream.com                #
+                                           +
# Report Bugs (www.nuedream.com/contact.htm)#
+  - nqbugs@nuedream.com                    +
#                                           #
+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+=#=+
