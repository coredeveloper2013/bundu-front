#!/usr/bin/perl
use CGI::Carp qw(fatalsToBrowser);

# (c) NueDream Inc. 2000-2001

##############    GLOBAL VARS AND CONSTS    ############

$file     = "questions.db";                                      # This is the name of your questions file
$quizname = "NueQuiz";				                             # This is what appears in the TITLE
$editurl  = "yellowstonelodging.biz/nuequiz/edit.cgi";  # Location of the edit page
$passurl  = "yellowstonelodging.biz/nuequiz/password.htm";          # Location of Password Check file 

############   THESE ARE OPTIONAL CHANGES    ###########

$spacer = '|';                                                   # leave it unless you already have a database
@list = ('a','b','c','d','e','f');

##############    GET INFO FROM WEBPAGE    ############

if($ENV{'HTTP_REFERER'} =~ /$passurl/ || $ENV{'HTTP_REFERER'} =~ /$editurl/) {

print "Content-type: text/html\n\n";

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
	($name,$value) = split (/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("c", hex($1))/eg;
	$value =~ s/\n/ /g; # newlines are now spaces
	$value =~ s/\r//g; # removes hard returns
	$value =~ s/\cM//g; # deletes ^M's
	$FORM{$name} = $value;
}

##############    MAIN PROGRAM    ############
if ($FORM{'action'} eq "") {

print <<EndADMIN
<html>
<head>
<title>:::: NueQuiz Administration Page ::::</title>
</head>

<!-- (c) NueDream Inc. 2000-2001 -->

<body bgcolor="white">
<font size="1" face="verdana">
<font size="4"><b>Administrative Menu</b></font><br>
<hr color="#000000" width="450" align="left" noshade>
<b>Choose what action you wish to perform and click on the Submit button.</b><br>
<hr color="#000000" width="450" align="left" noshade>
<form action="/nuequiz/edit.cgi" method="POST">
<input type="radio" name="edit" value="modbeg" checked>Modify Beginner Quiz Questions.<br>
<input type="radio" name="edit" value="modnov">Modify Novice Quiz Questions<br>
<input type="radio" name="edit" value="modexp">Modify Expert Quiz Questions<br>
<br>
<input type="radio" name="edit" value="addbeg">Add Beginner Quiz Question<br>
<input type="radio" name="edit" value="addnov">Add Novice Quiz Question<br>
<input type="radio" name="edit" value="addexp">Add Expert Quiz Question<br>
<br>
<input name="action" value="Submit" type="submit">
</Form>
<hr color="#000000" width="450" align="left" noshade>
<b>NueQuiz - (c) 2000 <a href="http://www.nuedream.com">NueDream Inc.</a></b>
</font>
</body>
</html>
EndADMIN


} elsif ( $FORM{'action'} eq "Submit" ) {
	
	$edit = $FORM{'edit'};
	if ($edit =~ /\w+beg$/) {
		$quiz = "beginner";
	} elsif ($edit =~ /\w+nov$/) {
		$quiz = "novice";
	} elsif ($edit =~ /\w+exp$/) {
		$quiz = "expert";
	} else {
		dienice("Invalid quiz type go to the <a href=\"http://$editurl\">Admin Page</a> and try again");
	}
	
	if ($edit =~ /^mod\w+/) {
		$type = "modify";
	} elsif ($edit =~ /^add\w+/)  {
		$type = "add";
	} else {
		dienice("Invalid action to perform go to the <a href=\"http://$editurl\">Admin Page</a> and try again");
	}
	
	
	open(DATA,$file) or dienice("Couldn't open $file :: $!\n");
	#flock(DATA,0); # uncomment this line if using unix based server
	seek(DATA,0,0);
	@data = <DATA>;
	close(DATA);

##### GENERAL ADMIN DISPLAY #####		
	
	print "<html><head><title>:::: Quiz Administration Page :::: $ENV{'HTTP_REFERER'}</title></head>\n<body bgcolor=\"white\">";
	
	print "<font size=\"1\" face=\"verdana\">";
	print "<font size=\"4\"><b>Administrative Menu</b></font><br>";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><b>You are ".$type."ing a record in the $quiz quiz.</b></font>\n";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	
	if ($type eq "add") {
		print "Complete the required(<font color=\"red\">*</font>) information and hit add.<br>";
		print "You may leave some of the questions empty if you choose.<br>";
		print "To display no reason just use a blank space in the Reason area.<br>";
		print "<form action=\"edit.cgi\" method=\"POST\">";
		print "<input type=\"Hidden\" name=\"quiz\" value=\"$quiz\">";
		print "<table><tr><td colspan=\"3\"><font size=\"2\" face=\"verdana\"><font color=\"red\">*</font>Question:</font></td></tr>\n";
		print "<tr><td colspan=\"3\"><input type=\"text\" name=\"question\" size=\"45\"></td></tr>\n";
		print "<tr><td><font size=\"2\" face=\"verdana\"><font color=\"red\">*</font>Answer</font></td><td><font size=\"2\" face=\"verdana\">#</font></td><td><font size=\"2\" face=\"verdana\"><font color=\"red\">*</font>Choices</font></td></tr>\n";
		foreach $i (@list) {
			print "<tr><td><input type=\"radio\" name=\"answer\" value=\"$i\"></td><td><font size=\"2\" face=\"verdana\">$i)</font></td>\n";
			print "<td><input type=\"text\" name=\"$i\" size=\"30\"></td></tr>\n";
		}
		print "<tr><td colspan=\"3\"><font size=\"2\" face=\"verdana\"><font color=\"red\">*</font>Reasoning for answer</font></td></tr>\n";
		print "<tr><td colspan=\"3\"><textarea cols=\"34\" rows=\"3\" name=\"reason\"></textarea></td></tr>\n";
		print "<tr><td colspan=\"3\" align=\"right\"><input type=\"submit\" name=\"action\" value=\"Add\"></td></tr>";
		print "</table>\n</form>\n";
		print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
		print "<font size=\"1\"><b>NueQuiz - (c) 2000 <a href=\"http://www.nuedream.com\">NueDream Inc.</a></b></font>\n";
		
	} elsif ($type eq "modify") {# modifying a database of quizzes
		print "<table>";
		
		foreach $line (@data) {
			($quizin,$num,$question,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\Q$spacer/,$line);
			if ($quizin eq $quiz) {
				print "<form action=\"edit.cgi\" method=\"POST\">";
				print "<tr><td><font size=\"2\" face=\"verdana\">$num) $question<input type=\"hidden\" name=\"question\" value=\"$question\"><input type=\"hidden\" name=\"number\" value=\"$num\"><input type=\"hidden\" name=\"quiz\" value=\"$quiz\"></font></td></tr>\n";
				print "<tr><td nowrap><input type=\"Submit\" name=\"action\" value=\"Edit\"><input type=\"Submit\" name=\"action\" value=\"Remove\"></form></td></tr>";
				
			}
		}
		print "</table>";
		print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
		print "<font size=\"1\"><b>NueQuiz - (c) 2000 <a href=\"http://www.nuedream.com\">NueDream Inc.</a></b></font>\n";
	
	}
	
} elsif ($FORM{'action'} eq "Add") {
	#check if input is correct and put it into the database
	if ($FORM{'reason'} eq "" || $FORM{'answer'} eq "" || $FORM{$FORM{'answer'}} eq "" || $FORM{'question'} eq ""){
		print "<br><h1>ERROR :: Information Incomplete</h1><br>Please hit your browser's back button and complete the information";
	} else {
		open(DATA,$file) or dienice("Couldn't open $file :: $!\n");
		#flock(DATA,0); # uncomment this line if using unix based server
		@data = <DATA>;
		close(DATA);
		$count = 0;
		foreach $line (@data) {
			($quizin,$num,$question,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\Q$spacer/,$line);
			if ($quizin eq $FORM{'quiz'}) {
				if ($count < $num) {
					$count = $num;
				}
			}
		}
		$count++;
		$line = $FORM{'quiz'}.$spacer.($count).$spacer.$FORM{'question'}.$spacer.$FORM{'answer'}.$spacer;
		foreach $i (@list){
			$line .= $FORM{$i}.$spacer;
		}
		$line .= $FORM{'reason'};
		
		open(DATA,">>".$file) or dienice("Couldn't open $file :: $!\n");
		print DATA $line."\n";
		close(DATA);
		print "<html><head><title>:::: $quizname Administration Page ::::</title></head>\n<body bgcolor=\"white\">";
		print "<font size=\"1\" face=\"verdana\">";
		print "<font size=\"4\"><b>Administrative Menu</b></font><br>";
		print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
		print "<font size=\"1\"><b>You have successfully Added a question in the $FORM{'quiz'} $quizname quiz</b></font><br>\n";
		print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
		print "<font size=\"1\"><br>You may now return to the <a href=\"http://$editurl\">administration page</a>.</font><br><br>";
		print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
		print "<font size=\"1\"><b>NueQuiz - (c) 2000 <a href=\"http://www.nuedream.com\">NueDream Inc.</a></b></font>\n";
		
	}
} elsif ($FORM{'action'} eq "Update") {# going to update a specific question
	open(DATA,$file) or dienice("Couldn't open $file :: $!\n");
	#flock(DATA,0); # uncomment this line if using unix based server
	seek(DATA,0,0);
	@data = <DATA>;
	close(DATA);
	
	foreach $line (@data) {
		chomp($line);
		($quizin,$num,$question,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\Q$spacer/,$line);
		
		if ($quizin eq $FORM{'quiz'} && $num eq $FORM{'number'}) {
			$string = $FORM{'quiz'}.$spacer.$FORM{'number'}.$spacer.$FORM{'question'}.$spacer.$FORM{'answer'}.$spacer;
			foreach $i (@list){
				$string .= $FORM{$i}.$spacer;
			}
			$string .= $FORM{'reason'};
			push (@result, $string);
		} else {
			push (@result, $line);
		}
	}
	
	open(DATA,">".$file) or dienice("Couldn't open $file for overwriting:: $!\n");
	#flock(DATA,0); # uncomment this line if using unix based server
	seek(DATA,0,0);
	sort @result;
	foreach $line (@result) {
		print DATA $line."\n";
	}
	close(DATA);

##### MODIFIED QUESTION DISPLAY #####	
	
	print "<html><head><title>:::: $quizname Administration Page ::::</title></head>\n<body bgcolor=\"white\">";
	print "<font size=\"1\" face=\"verdana\">";
	print "<font size=\"4\"><b>Administrative Menu</b></font><br>";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><b>You have successfully Modified a question in the $FORM{'quiz'} $quizname quiz</b></font><br>\n";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><br>You may now return to the <a href=\"http://$editurl\">administration page</a>.</font><br><br>";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><b>NueQuiz - (c) 2000 <a href=\"http://www.nuedream.com\">NueDream Inc.</a></b></font>\n";
		
} elsif ($FORM{'action'} eq "Edit") {# going to edit a specific question
	
	open(DATA,$file) or dienice("Couldn't open $file :: $!\n");
	#flock(DATA,0); # uncomment this line if using unix based server
	seek(DATA,0,0);
	@data = <DATA>;
	close(DATA);

##### EDITING QUESTION DISPLAY #####		

	print "<html><head><title>:::: $quizname Administration Page ::::</title></head>\n<body bgcolor=\"white\">";
	print "<font size=\"1\" face=\"verdana\">";
	print "<font size=\"4\"><b>Administrative Menu</b></font><br>";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><b>You are Editing a record in the $FORM{'quiz'} quiz.</b></font>\n";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	
	foreach $line (@data) {
		($quizin,$num,$question,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\Q$spacer/,$line);
		
		if ($quizin eq $FORM{'quiz'} && $question eq $FORM{'question'} && $num eq $FORM{'number'}) {
			print "<form action=\"edit.cgi\" method=\"POST\">";
			print "<input type=\"hidden\" name=\"number\" value=\"$FORM{'number'}\">";
			print "<input type=\"hidden\" name=\"quiz\" value=\"$FORM{'quiz'}\">";
			print "<table><tr><td colspan=\"3\"><font size=\"2\" face=\"verdana\">Question:</font></td></tr>\n";
			print "<tr><td colspan=\"3\"><input type=\"text\" name=\"question\" size=\"45\" value=\"$question\"></td></tr>\n";
			print "<tr><td><font size=\"2\" face=\"verdana\">Answer</font></td><td><font size=\"2\" face=\"verdana\">#</font></td><td><font size=\"2\" face=\"verdana\">Choices</font></td></tr>\n";
			
			$addon = ($ans eq "a")?" checked":"";
			print "<tr><td><input type=\"radio\" name=\"answer\" value=\"a\"$addon></td><td>a)</td>\n";
			print "<td><input type=\"text\" name=\"a\" size=\"30\" value=\"$a\"></td></tr>\n";
			$addon = ($ans eq "b")?" checked":"";
			print "<tr><td><input type=\"radio\" name=\"answer\" value=\"b\"$addon></td><td>b)</td>\n";
			print "<td><input type=\"text\" name=\"b\" size=\"30\" value=\"$b\"></td></tr>\n";
			$addon = ($ans eq "c")?" checked":"";
			print "<tr><td><input type=\"radio\" name=\"answer\" value=\"c\"$addon></td><td>c)</td>\n";
			print "<td><input type=\"text\" name=\"c\" size=\"30\" value=\"$c\"></td></tr>\n";
			$addon = ($ans eq "d")?" checked":"";
			print "<tr><td><input type=\"radio\" name=\"answer\" value=\"d\"$addon></td><td>d)</td>\n";
			print "<td><input type=\"text\" name=\"d\" size=\"30\" value=\"$d\"></td></tr>\n";
			$addon = ($ans eq "e")?" checked":"";
			print "<tr><td><input type=\"radio\" name=\"answer\" value=\"e\"$addon></td><td>e)</td>\n";
			print "<td><input type=\"text\" name=\"e\" size=\"30\" value=\"$e\"></td></tr>\n";
			$addon = ($ans eq "f")?" checked":"";
			print "<tr><td><input type=\"radio\" name=\"answer\" value=\"f\"$addon></td><td>f)</td>\n";
			print "<td><input type=\"text\" name=\"f\" size=\"30\" value=\"$f\"></td></tr>\n";
			
			print "<tr><td colspan=\"3\"><font size=\"2\" face=\"verdana\">Reasoning for answer</font></td></tr>\n";
			print "<tr><td colspan=\"3\"><textarea cols=\"34\" rows=\"3\" name=\"reason\">$reason</textarea></td></tr>\n";
			print "<tr><td colspan=\"3\" align=\"right\"><input type=\"submit\" name=\"action\" value=\"Update\"></td></tr>";
			print "</table>\n</form>\n";
			print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
			print "<font size=\"1\"><b>NueQuiz - (c) 2000 <a href=\"http://www.nuedream.com\">NueDream Inc.</a></b></font>\n";
	
			last;
		}
	}
	
} elsif ($FORM{'action'} eq "Remove") {# going to remove a specific question
	
	open(DATA,$file) or dienice("Couldn't open $file :: $!\n");
	#flock(DATA,0); # uncomment this line if using unix based server
	seek(DATA,0,0);
	@data = <DATA>;
	close(DATA);
	
	foreach $i ("beginner", "novice", "expert") {
		$nextnum{$i} = 0;
	}
		
	foreach $line (@data) {
		chomp($line);
		($quizin,$num,$question,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\Q$spacer/,$line);
		
		if ($quizin eq $FORM{'quiz'} && $question eq $FORM{'question'} && $num eq $FORM{'number'}) {
		} else {
			$nextnum{$quizin}++;
			$string = $quizin.$spacer;
			$string .= $nextnum{$quizin}.$spacer.$question.$spacer.$ans.$spacer;
			$string .= $a.$spacer.$b.$spacer.$c.$spacer.$d.$spacer.$e.$spacer.$f.$spacer;
			$string .= $reason;
			push (@result, $string);
		}
	}
	
	open(DATA,">".$file) or dienice("Couldn't open $file for overwriting:: $!\n");
	#flock(DATA,0); # uncomment this line if using unix based server
	seek(DATA,0,0);
	sort @result;
	foreach $line (@result) {
		print DATA $line."\n";
	}
	close(DATA);

##### REMOVED QUESTION DISPLAY #####

	print "<html><head><title>:::: $quizname Administration Page ::::</title></head>\n<body bgcolor=\"white\">";
	print "<font size=\"1\" face=\"verdana\">";
	print "<font size=\"4\"><b>Administrative Menu</b></font><br>";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><b>You have successfully Removed a question in the $FORM{'quiz'} $quizname quiz</b></font><br>\n";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><br>You may now return to the <a href=\"http://$editurl\">administration page</a>.</font><br><br>";
	print "<hr color=\"#000000\" width=\"450\" align=\"left\" noshade>";
	print "<font size=\"1\"><b>NueQuiz - (c) 2000 <a href=\"http://www.nuedream.com\">NueDream Inc.</a></b></font>\n";
		


} else {
	print "Content-type: text/html\n\n";	
	dienice("You have not used a valid way to access this program go to the <a href=\"http://$editurl\">Admin Page</a>");
}

} else {
	print "Content-type: text/html\n\n";
	dienice("You have not used a valid way to access this program.");
}
##############    DYING SUBROUTINE    ############

sub dienice {
	my($msg) = @_;
	print ("<br><hr><br><h1>FATAL ERROR</h1><hr><br>\n");
	print ($msg);
	exit;
}
