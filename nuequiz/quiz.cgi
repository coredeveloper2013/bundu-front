#!/usr/bin/perl
use CGI::Carp qw(fatalsToBrowser);

# (c) NueDream Inc. 2000-2001 (www.nuedream.com)

##############    GLOBAL VARS AND CONSTS    ############

$file = "questions.db"; 							      							  # This is the name of your questions file.
$quizname = "NueQuiz";                                                                # This is what appears in the TITLE.

############   THESE ARE OPTIONAL CHANGES    ###########

$spacer = '|';				                                              # Leave it unless you already have a database.
@list = ('a','b','c','d','e','f');
$result1 = "I think you need to think a little harder before you answer next time.";  # Message Displayed when user gets below 50%.
$result2 = "You did pretty good. Some of those questions are harder than they look."; # Message Displayed when user gets between 51%-90%.
$result3 = "Wow! You did excellent, looks like you know your stuff.";                 # Message Displayed when user get above 90%.
$cheater = "Cheating won't get you anywhere in life.";                                # Message Displayed when user cheated. 
$fontface = "verdana, tahoma, arial";                                                 # Font Type.

##############    GET COOKIE AND GET INFO   ############

$cookie    = $ENV{'HTTP_COOKIE'};
(@cookies) = split(/;/, $cookie);

$content = $ENV{'QUERY_STRING'};
($type, $question) = split(/=/, $content);

$current = $question-1;
$correct = 0;

##############    MAIN PROGRAM    ############

print "Content-type: text/html\n\n";

print "<html>\n<head>\n<title>$quizname</title>\n</head>\n\n<body bgcolor=\"white\">\n";
print "\n<!-- NueQuiz. (c) NueDream Inc. 2000-2001. (www.nuedream.com) -->\n\n";
print "<center>\n<table width=\"450\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n<tr>\n<td>\n";
print "<font face=\"$fontface\" size=\"2\">\n";

open(DATA,$file) or dienice("Couldn't open $file :: $!\n");
#flock(DATA,0); # uncomment this line if using unix based server
seek(DATA,0,0);
@data = <DATA>;
close(DATA);

$total = 0;
foreach $line (@data) {
	($quiz,$num,$q,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\|/,$line);
	if ($quiz eq $type) {
		$total++;
	}
}

$total++;

if ($question == $total) {				

	print "<br><b>QUIZ Results:</b><br><hr width=\"450\" align=\"left\" color=\"black\" NOSHADE><br>";
	print "<font size=\"1\">";
	foreach $part (@cookies) {
		if ($part =~ /$type/) {
			($name, $value) = split(/=/,$part);
			(@answers) = split(/-/,$value);
					
			foreach $a (@answers) {
				($questionc, $answerc) = split(/_/,$a);
		                
		                foreach $line (@data) {
		       			chomp($line);
					($quiz,$num,$q,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\|/,$line);
			        
					if ($quiz eq $type && $questionc eq $num) {
		   
						if(substr($answerc,0,1) eq substr($answerc,1,1)) {
							print "$questionc. $q<br><font color=\"#33cc00\"><b>CORRECT!</b></font><br><br>";
							$correct++;
						} else {
							print "$questionc. $q<br><font color=\"red\"><b>INCORRECT!</b></font><ol>$reason</ol>";
						}
					
					}
				}
			}
		} 
	}
			
			
	print "</font>";
	print "<br><hr width=\"450\" align=\"left\" color=\"black\" NOSHADE>";
			
	$percent = ($correct/$current)*100;
	printf ("<b>$correct</b> / <b>$current</b> = <b> %4.1f", $percent, "%</b><br><br>");
	print "%</b><br><br>";		
			
	if ( ($correct/$total) < 0.5) { 
		print "$result1";
	} elsif (($correct/$total) < 0.9) {
		print "$result2";
	} elsif (($correct/$total) > 0.9) {
		print "$result3";
	} else {
		print "$cheater";
	}

	} else {
			
		foreach $line (@data) {
			chomp($line);
			($quiz,$num,$q,$ans,$a,$b,$c,$d,$e,$f,$reason) = split(/\|/,$line);
			        
			if ($quiz eq $type && $question eq $num) {
			        
			        print "<form action=\"check.cgi?$type=$question\" method=\"GET\">\n"; 
			        
 				print "<br><b>$question. $q</b><br>";
   				
   				if ($a ne "") { print "<input type=\"radio\" name=\"$num\" value=\"a\" checked>A. $a<br>\n"; }
   				if ($b ne "") { print "<input type=\"radio\" name=\"$num\" value=\"b\">B. $b<br>\n"; }
   				if ($c ne "") { print "<input type=\"radio\" name=\"$num\" value=\"c\">C. $c<br>\n"; }
   				if ($d ne "") { print "<input type=\"radio\" name=\"$num\" value=\"d\">D. $d<br>\n"; }
   				if ($e ne "") { print "<input type=\"radio\" name=\"$num\" value=\"e\">E. $e<br>\n"; }
   				if ($f ne "") { print "<input type=\"radio\" name=\"$num\" value=\"f\">F. $f<br>\n"; }
   				
   				print "<br><input type=\"hidden\" name=\"$type\" value=\"$num\">\n";
				print "<input type=\"submit\" value=\"Continue �\">\n</form>\n";
			}
		}
	}

print "</font>\n</td>\n</tr>\n</table>\n</center>\n</body>\n</html>";

##############    DYING SUBROUTINE    ############

sub dienice {
	my($msg) = @_;
	print ("<br><hr><br><h1>FATAL ERROR</h1><hr><br>\n");
	print ($msg);
	exit;
}