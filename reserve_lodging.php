<? // This script written by Dominick Bernal - www.bernalwebservices.com

/*if($_SERVER["HTTP_HOST"] != "www.bundubashers.com" || !isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
	header("Location: https://www.bundubashers.com/reserve_lodging.php?".$_SERVER["QUERY_STRING"]);
	exit;
	}*/

error_reporting(0);
@date_default_timezone_set('America/Denver');
$time = time(); //mktime();

@ini_set("session.gc_maxlifetime","10800");
if(!isset($_SESSION)){ session_start();	}

if(!isset($_SESSION['http_referer'])){ $_SESSION['http_referer'] = ''; }
if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "" && parse_url($_SERVER['HTTP_REFERER'],PHP_URL_HOST) != $_SERVER["HTTP_HOST"]){
    $_SESSION['http_referer'] = $_SERVER['HTTP_REFERER'];
}

$db = mysql_connect('localhost', 'root', '');
mysql_select_db('bund');
@mysql_query("SET NAMES utf8");
@mysql_query('SET time_zone = "'.date("P").'"');

$fullwidth = "780";
$tablewidth = "900";
$leftcol = "300";
$rightcol = ($tablewidth-$leftcol);

function printmsgs($successmsg,$errormsg){
    //if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<IMG SRC="spacer.gif" BORDER="0"><BR>'."\n"; endif;
    if(isset($successmsg) && count($successmsg) > 0){
        echo '<TABLE BORDER="0" BGCOLOR="#000C7F" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Update Successful</FONT></TD></TR>
	<TR BGCOLOR="#D9DBEC"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$successmsg).'</I><BR></FONT></TD></TR>
	</TABLE>';
        if(isset($errormsg) && count($errormsg) > 0): echo '<BR>'; endif;
        echo "\n\n";
    }
    if(isset($errormsg) && count($errormsg) > 0){
        echo '<TABLE BORDER="0" BGCOLOR="#B00000" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Error:</FONT></TD></TR>
	<TR BGCOLOR="#F3D9D9"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$errormsg).'</I></FONT></TD></TR>
	</TABLE>'."\n\n";
    }
}

function bgcolor($i){
    if(!isset($GLOBALS['bgcolor']) || $i == "reset"): $GLOBALS['bgcolor'] = "DDDDDD"; endif;
    if($i != "reset" && $i != ""): $GLOBALS['bgcolor'] = $i; endif;
    if($GLOBALS['bgcolor'] == "FFFFFF"): $GLOBALS['bgcolor'] = "DDDDDD"; else: $GLOBALS['bgcolor'] = "FFFFFF"; endif;
    return $GLOBALS['bgcolor'];
}

$successmsg = array();
$errormsg = array();
$adminsuccessmsg = array();
$adminerrormsg = array();

if(isset($_REQUEST['start_year']) && $_REQUEST['start_year'] != ""): $syear = $_REQUEST['start_year']; else: $syear = date("Y",$time); endif;


//GET AVAILABLE DATES
$query = 'SELECT MIN(`startdate`) as `earliest`, MAX(`enddate`) as `latest`, MIN(`min_nights`) as `min_nights`, MAX(`max_nights`) as `max_nights` FROM `lodging_pricing`';
$query .= ' WHERE `enddate` >= "'.strtotime('today').'"';
if(isset($_REQUEST['type']) && $_REQUEST['type'] != ""): $query .= ' AND `lodgeid` = "'.$_REQUEST['type'].'"'; endif;
$result = mysql_query($query);
$availdates = mysql_fetch_assoc($result);

//echo mysql_error();
?>

<HTML>

<HEAD>
	<TITLE>Yellowstone Lodging Reservations</TITLE>
	<style><!--
	.res1 { font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #000000; }
	.check_in, .check_out {background-color: initial !important;}
	</style>
<!--	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
<!--	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css" integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous" />-->
    <meta name="viewport" content="width=device-width">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



</HEAD>
<?php
echo '<BODY BGCOLOR="#E1E1E1" TEXT="#000000" TOPMARGIN="0" style="background-color: #E1E1E1">

<CENTER>'."\n\n";

//
//if ($_REQUEST['end_date'] and $_REQUEST['start_date'] = null){
//    header("Location: https://localhost/bund/index.php");
//}
$d1 = $_REQUEST['start_date'];
$d2 = $_REQUEST['end_date'];
$start =  strtotime($_REQUEST['start_date']);
$end =  strtotime($_REQUEST['end_date']);
$diff = abs($start - $end);
$days = floor($diff/ (60*60*24));
$_REQUEST['nights'] = $days;
$_REQUEST['end_date'] = null;
$_REQUEST['start_date'] = null;
//echo '<PRE>'; print_r($_POST); echo '</PRE>';


echo '<TABLE BORDER="0" WIDTH="'.$fullwidth.'" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF" style="background-color: white">
<TR><TD ALIGN="center">
	<div class="jumbotron text-center">
            <h1 ><a href="index.php" style="color: #FFA500">Yellowstone lodging</a></h1>
    </div>'."\n";

    $start = mktime(0,0,0,$_REQUEST['start_month'],$_REQUEST['start_day'],$_REQUEST['start_year']);
    if(!is_numeric($_REQUEST['nights'])): $_REQUEST['nights'] = $availdates['min_nights']; else: $_REQUEST['nights'] = number_format($_REQUEST['nights'],0,'',''); endif;
    $end = strtotime("+".$_REQUEST['nights']." days",$start);
    //echo $start.' : '.$seasons[0].'<BR>'.$end.' : '.end($seasons);

    $total = ($_REQUEST['nights'] * $_REQUEST['price']*$_REQUEST['total_room']);

    //echo '<PRE>'; print_r($prices); echo '</PRE>';
    //$total = array_sum($prices);
    $total = number_format($total, 2, '.', '');

    echo '<FORM style="display: block;width: 100%;text-align: left;padding: 0 107px;" NAME="resform" METHOD="post">'."\n";
    echo '<a class="btn btn-primary"  href="index.php">Change</a>'."\n";
    echo '</FORM>'."\n\n";

    echo '<TABLE class="table table-bordered" BORDER="0" style="width: 80%;" CELLPADDING="2" CELLSPACING="1">'."\n";
    echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="center" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Your Reservation:</B></FONT></TD></TR>'."\n";

    echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Room:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">'.$_REQUEST['type_name'].'</FONT></TD></TR>'."\n";
    echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Dates:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">'.date("M j, Y",$start);
    echo ' - '.date("M j, Y",$end).' ('.$_REQUEST['nights'].' Night';
    if($_REQUEST['nights'] > 1){ echo 's'; }
    echo ')';
    echo '</FONT></TD></TR>'."\n";
    echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Guests:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">'.$_REQUEST['guests'].'</FONT></TD></TR>'."\n";
    echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Rooms:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">'.$_REQUEST['total_room'].'</FONT></TD></TR>'."\n";

    echo '<TR BGCOLOR="#E1E1E1"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Subtotal:&nbsp;&nbsp;$'.$total.'&nbsp;&nbsp;</B></FONT></TD></TR>'."\n";

    $tax = ($total*.10) + $_REQUEST['nights']*1;
    echo '<TR BGCOLOR="#E1E1E1"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Tax:&nbsp;&nbsp;&nbsp;$'.number_format($tax, 2, '.', '').'&nbsp;&nbsp;</B></FONT></TD></TR>'."\n";

    $total = $total+$tax;
    $total = number_format($total, 2, '.', '');
    echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Total:&nbsp;&nbsp;$'.$total.'&nbsp;&nbsp;</B></FONT></TD></TR>'."\n";

    echo '</TABLE>'."\n\n";


//    echo '<FORM NAME="resform" style="padding : 0 150px 0 0" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="javascript:return checkexp();">'."\n";
    echo '<FORM id="checkoutForm" NAME="resform" style="padding : 0 150px 0 0" onSubmit="checkexp(event);">'."\n";
//    echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="process">'."\n";
    echo '<INPUT TYPE="hidden" NAME="type" ID="type" VALUE="'.$_REQUEST['type'].'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="start_month" ID="start_month" VALUE="'.$_REQUEST['start_month'].'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="start_day" ID="start_day" VALUE="'.$_REQUEST['start_day'].'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="start_year" ID="start_year" VALUE="'.$_REQUEST['start_year'].'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="nights"  ID="nights" VALUE="'.$_REQUEST['nights'].'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="guests"  ID="guests" VALUE="'.$_REQUEST['guests'].'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="end_date"  ID="guests" VALUE="'.$end.'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="total" ID="total" VALUE="'.$total.'">'."\n";
    echo '<INPUT TYPE="hidden" NAME="total_room" ID="total" VALUE="'.$_REQUEST['total_room'].'">'."\n";
    echo '	<TABLE BORDER="0" WIDTH="'.$tablewidth.'" CELLPADDING="0" CELLSPACING="2">'."\n";
    //echo '	<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2"><B>Contact / Payment Information</B></FONT></TD></TR>'."\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Name<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="30" NAME="name" ID="name" required VALUE="'; if(isset($_REQUEST['name'])): echo $_REQUEST['name']; endif; echo '"></div></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Home/Business Phone&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="30" ID="phone_homebus" NAME="phone_homebus" VALUE="'; if(isset($_REQUEST['phone_homebus'])): echo $_REQUEST['phone_homebus']; endif; echo '"></div></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Cell Phone<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="30" required NAME="phone_cell" ID="phone_cell" VALUE="'; if(isset($_REQUEST['phone_cell'])): echo $_REQUEST['phone_cell']; endif; echo '"></div></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Email Address<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="30" required NAME="email" ID="email" VALUE="'; if(isset($_REQUEST['email'])): echo $_REQUEST['email']; endif; echo '"></div></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Re-type Email Address<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="30" required NAME="email2" ID="email2" VALUE="'; if(isset($_REQUEST['email'])): echo $_REQUEST['email']; endif; echo '"></div></TD></TR>'."\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Credit card number<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="30" NAME="cc_num" ID="cc_num" VALUE="'; if(isset($_REQUEST['cc_num'])): echo $_REQUEST['cc_num']; endif; echo '"></div></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Expiration date<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="row"><div class="col-md-6"><div class="form-group"><SELECT class="form-control" NAME="cc_expdate_month" ID="cc_expdate_month">';   for($i=1; $i<13; $i++){   echo '<OPTION VALUE="'.date("m",mktime("0","0","0",$i,"1","2005")).'"'; if( isset($_REQUEST['cc_expdate_month']) && $_REQUEST['cc_expdate_month'] == $i || date("m") == $i ): echo " SELECTED"; endif; echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';   }   echo '</SELECT></div></div><div class="col-md-6"><div class="form-group"><SELECT class="form-control" NAME="cc_expdate_year" ID="cc_expdate_year">';   for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){   echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"'; if( isset($_REQUEST['cc_expdate_year']) && $_REQUEST['cc_expdate_year'] == date("y",mktime("0","0","0","1","1",$i)) ): echo " SELECTED"; endif; echo '>'.$i.'</OPTION>';   }   echo '</SELECT></div></div></div></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Security code<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="5" NAME="cc_scode" ID="cc_scode" VALUE="'; if(isset($_REQUEST['cc_scode'])): echo $_REQUEST['cc_scode']; endif; echo '"></div></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">Zip/Postal code<span class="text-danger" style="font-size: 16px"> *</span>&nbsp;&nbsp;&nbsp;<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Please provide the zip or postal code&nbsp;&nbsp;&nbsp;<BR>associated with your credit card.&nbsp;&nbsp;</SPAN>&nbsp;</TD><TD><div class="form-group"><INPUT class="form-control" TYPE="text" SIZE="8" NAME="cc_zip" ID="cc_zip" VALUE=""></div></TD></TR>'."\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
    echo '	<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><FONT FACE="Arial" SIZE="2"><B>Comments&nbsp;&nbsp;</B>&nbsp;</FONT></TD><TD ALIGN="left" WIDTH="'.$rightcol.'"><div class="form-group"><TEXTAREA class="form-control" NAME="comments" ID="comments" COLS="30" ROWS="3">'; if(isset($_REQUEST['comments'])): echo $_REQUEST['comments']; endif; echo '</TEXTAREA></div></TD></TR>'."\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="right"><INPUT class="btn btn-lg btn-success" TYPE="submit" VALUE="Submit Reservation"></TD></TR>'."\n";
//    echo '	<TR><TD COLSPAN="2" ALIGN="right"><INPUT class="btn btn-lg btn-success" data-toggle="modal" data-target="#confirm-submit" VALUE="Submit Reservation"></TD></TR>'."\n";
    echo '	</TABLE>'."\n";
?>
    <button style="display: none" class="btn btn-default" data-toggle="modal" data-target="#confirm-submit" id="confirmSubmit">dfdf</button>
    <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3  class="modal-title">By submitting this reservation, I understand:  </h3>
                </div>
                <div class="modal-body">
                    <p><br>
                    <ul class="text-left text-success">
                        <li>No changes or cancellations are permitted, and refunds will not be given.</li>
                        <li>Children under 18 and pets are not permitted.</li>
                        <li>Check in after 9.30 pm are not permitted.</li>
                        <li>Refunds will not be given if  you do not check in by 9.30 pm.</li>
                    </ul>
                    <br>
                                 <h4>Thank you!</h4></p>
                </div>
                <div class="modal-footer">
                    <button  class="btn btn-success" id="confirmFinalReservation">Confirm Reservation </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

<?php

    echo '</FORM>'."\n";

echo "</div>";
?>
    <button style="display: none " class="btn btn-default" data-toggle="modal" data-target="#confirm-submit1" id="error">Submit</button>
    <div class="modal fade" id="confirm-submit1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3  class="modal-title">Please Input valid Information !</h3>
                </div>
                <div class="modal-body">
                    <ul class="text-left text-warning" id="error_message">

                    </ul>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js" integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>
    <SCRIPT>


        $(".check_in").flatpickr({
            minDate: '<?php echo date('Y-m-d') ?>',
            defaultDate: '<?php echo $d1 ?>',
            altInput: true,
            disableMobile: true,
            altFormat: 'd M, Y'

        });

        $(".check_out").flatpickr({
            defaultDate: '<?php echo $d2 ?>',
            minDate: new Date($(".check_in").val()).fp_incr(1), // 1 days from check_in
            altInput: true,
            disableMobile: true,
            altFormat: 'd M, Y'
        });

        function selectNight() {
            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            var check_in = new Date($(".check_in").val());
            var check_out = new Date($(".check_out").val());
            var diffDays = Math.round(Math.abs((check_in.getTime() - check_out.getTime())/(oneDay)));
            $('#nights').val(diffDays);
        }

        function selectCheckout() {
            $('#nights').val(1);
            var check_in = new Date($(".check_in").val());
            $(".check_out").prop('disabled', false);
            if ($(".check_in")) {
                $(".check_out").flatpickr({
                    defaultDate: new Date($(".check_in").val()).fp_incr(1),
                    minDate: new Date($(".check_in").val()).fp_incr(1), // 1 days from check_in
                    altInput: true,
                    disableMobile: true,
                    altFormat: 'd M, Y'
                });
            }

            // Loop over each select option
            $("#start_year > option").each(function(){
                // Check for the matching data
                if ($(this).val() == check_in.getFullYear()){
                    // Select the matched option
                    $(this).prop("selected", true);
                }
            });

            // Loop over each select option
            $("#start_month > option").each(function(){
                // Check for the matching data
                if ($(this).val() == check_in.getMonth() + 1){
                    // Select the matched option
                    $(this).prop("selected", true);
                }
            });

            // Loop over each select option
            $("#start_day > option").each(function(){
                // Check for the matching data
                if ($(this).val() == check_in.getDate()){
                    // Select the matched option
                    $(this).prop("selected", true);
                }
            });
        }

        function openhelp(){
            open("alswestwardhelp.html", "helpwin", "width=100%,height=auto,location=no,menubar=no,resizable=yes,status=no,toolbar=no");
        }

        //CC Validation Script Author: Simon Tneoh (tneohcb@pc.jaring.my)
        var Cards = new Array();
        Cards[0] = new CardType("MasterCard", "51,52,53,54,55", "16");
        var MasterCard = Cards[0];
        Cards[1] = new CardType("VisaCard", "4", "13,16");
        var VisaCard = Cards[1];
        Cards[2] = new CardType("AmExCard", "34,37", "15");
        var AmExCard = Cards[2];
        //Cards[3] = new CardType("DinersClubCard", "30,36,38", "14");
        //var DinersClubCard = Cards[3];
        //Cards[4] = new CardType("DiscoverCard", "6011", "16");
        //var DiscoverCard = Cards[4];
        //Cards[5] = new CardType("enRouteCard", "2014,2149", "15");
        //var enRouteCard = Cards[5];
        //Cards[6] = new CardType("JCBCard", "3088,3096,3112,3158,3337,3528", "16");
        //var JCBCard = Cards[6];
        var LuhnCheckSum = Cards[3] = new CardType();

        function checkexp(e){
            e.preventDefault();
            e.stopPropagation();

            var r = true;
            var msg = '';
            if(document.getElementById("cc_num").value == ""){
                msg += ' <li>Please enter your credit card number.'+"</li>";
                r =  false;
            }
            if(document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == ""){
                msg += ' <li>Please choose an expiration date for your credit card.'+"</li>";
                r =  false;
            }
            if(r){
                var tmpyear;
                if(document.getElementById("cc_expdate_year").value > 96){
                    tmpyear = "19" + document.getElementById("cc_expdate_year").value;
                } else if(document.getElementById("cc_expdate_year").value < 21){
                    tmpyear = "20" + document.getElementById("cc_expdate_year").value;
                } else {
                    msg += 'The card expiration year is not valid.'+"\n";
                }
                tmpmonth = document.getElementById("cc_expdate_month").value;
                if (!(new CardType()).isExpiryDate(tmpyear, tmpmonth)) {
                    msg += '<li>Your credit card has already expired.'+"</li>";
                }
                card = false;
                for(var n = 0; n < Cards.length; n++){
                    if(Cards[n].checkCardNumber(document.getElementById("cc_num").value, tmpyear, tmpmonth)){
                        card = true;
                        break;
                    }
                }
                if(!card){
                    msg += ' <li>Your credit card number does not appear to be valid.'+"</li>";
                }
            }
            if(document.getElementById("cc_zip").value == ""){
                msg += '<li> Please provide the zip or postal code  associated with your credit card .'+"</li>";
            }
            if(document.getElementById("email").value != document.getElementById("email2").value){
                msg += ' <li>Email Address and Re-type Email Address do not match.</li> <li> Please check carefully to be sure you have entered the correct email address in both places.'+"</li>";
            }
            if (msg != "") {
                // alert(msg);
                $('#error_message').html(msg)
                $('#error').click()
            }else {


                $('#confirmSubmit').click()
                $('#confirmFinalReservation').on('click',function () {
                    $.ajax({
                        type: 'POST',
                        url: 'http://localhost/bundu-admin/api/NewReserve.php',
                        data : $('#checkoutForm').serialize(),
                        success: function(res){
                            swal("Reservation Success!", "Thanks for Choosing us!", "success")
                                .then((value) => {
                                    if (value){
                                        window.location = 'http://localhost/bund/reserve_success.php?id=' + res;
                                    }
                                });

                        }
                    });
                });
            }


        }


        /*************************************************************************\
         Object CardType([String cardtype, String rules, String len, int year,
         int month])
         cardtype    : type of card, eg: MasterCard, Visa, etc.
         rules       : rules of the cardnumber, eg: "4", "6011", "34,37".
         len         : valid length of cardnumber, eg: "16,19", "13,16".
         year        : year of expiry date.
         month       : month of expiry date.
         eg:
         var VisaCard = new CardType("Visa", "4", "16");
         var AmExCard = new CardType("AmEx", "34,37", "15");
         \*************************************************************************/
        function CardType() {
            var n;
            var argv = CardType.arguments;
            var argc = CardType.arguments.length;

            this.objname = "object CardType";

            var tmpcardtype = (argc > 0) ? argv[0] : "CardObject";
            var tmprules = (argc > 1) ? argv[1] : "0,1,2,3,4,5,6,7,8,9";
            var tmplen = (argc > 2) ? argv[2] : "13,14,15,16,19";

            this.setCardNumber = setCardNumber;  // set CardNumber method.
            this.setCardType = setCardType;  // setCardType method.
            this.setLen = setLen;  // setLen method.
            this.setRules = setRules;  // setRules method.
            this.setExpiryDate = setExpiryDate;  // setExpiryDate method.

            this.setCardType(tmpcardtype);
            this.setLen(tmplen);
            this.setRules(tmprules);
            if (argc > 4)
                this.setExpiryDate(argv[3], argv[4]);

            this.checkCardNumber = checkCardNumber;  // checkCardNumber method.
            this.getExpiryDate = getExpiryDate;  // getExpiryDate method.
            this.getCardType = getCardType;  // getCardType method.
            this.isCardNumber = isCardNumber;  // isCardNumber method.
            this.isExpiryDate = isExpiryDate;  // isExpiryDate method.
            this.luhnCheck = luhnCheck;// luhnCheck method.
            return this;
        }

        /*************************************************************************\
         boolean checkCardNumber([String cardnumber, int year, int month])
         return true if cardnumber pass the luhncheck and the expiry date is
         valid, else return false.
         \*************************************************************************/
        function checkCardNumber() {
            var argv = checkCardNumber.arguments;
            var argc = checkCardNumber.arguments.length;
            var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
            var year = (argc > 1) ? argv[1] : this.year;
            var month = (argc > 2) ? argv[2] : this.month;

            this.setCardNumber(cardnumber);
            this.setExpiryDate(year, month);

            if (!this.isCardNumber())
                return false;
            if (!this.isExpiryDate())
                return false;

            return true;
        }
        /*************************************************************************\
         String getCardType()
         return the cardtype.
         \*************************************************************************/
        function getCardType() {
            return this.cardtype;
        }
        /*************************************************************************\
         String getExpiryDate()
         return the expiry date.
         \*************************************************************************/
        function getExpiryDate() {
            return this.month + "/" + this.year;
        }
        /*************************************************************************\
         boolean isCardNumber([String cardnumber])
         return true if cardnumber pass the luhncheck and the rules, else return
         false.
         \*************************************************************************/
        function isCardNumber() {
            var argv = isCardNumber.arguments;
            var argc = isCardNumber.arguments.length;
            var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
            if (!this.luhnCheck())
                return false;

            for (var n = 0; n < this.len.size; n++)
                if (cardnumber.toString().length == this.len[n]) {
                    for (var m = 0; m < this.rules.size; m++) {
                        var headdigit = cardnumber.substring(0, this.rules[m].toString().length);
                        if (headdigit == this.rules[m])
                            return true;
                    }
                    return false;
                }
            return false;
        }

        /*************************************************************************\
         boolean isExpiryDate([int year, int month])
         return true if the date is a valid expiry date,
         else return false.
         \*************************************************************************/
        function isExpiryDate() {
            var argv = isExpiryDate.arguments;
            var argc = isExpiryDate.arguments.length;

            year = argc > 0 ? argv[0] : this.year;
            month = argc > 1 ? argv[1] : this.month;

            if (!isNum(year+""))
                return false;
            if (!isNum(month+""))
                return false;
            today = new Date();
            expiry = new Date(year, month);
            if (today.getTime() > expiry.getTime())
                return false;
            else
                return true;
        }

        /*************************************************************************\
         boolean isNum(String argvalue)
         return true if argvalue contains only numeric characters,
         else return false.
         \*************************************************************************/
        function isNum(argvalue) {
            argvalue = argvalue.toString();

            if (argvalue.length == 0)
                return false;

            for (var n = 0; n < argvalue.length; n++)
                if (argvalue.substring(n, n+1) < "0" || argvalue.substring(n, n+1) > "9")
                    return false;

            return true;
        }

        /*************************************************************************\
         boolean luhnCheck([String CardNumber])
         return true if CardNumber pass the luhn check else return false.
         Reference: http://www.ling.nwu.edu/~sburke/pub/luhn_lib.pl
         \*************************************************************************/
        function luhnCheck() {
            var argv = luhnCheck.arguments;
            var argc = luhnCheck.arguments.length;

            var CardNumber = argc > 0 ? argv[0] : this.cardnumber;

            if (! isNum(CardNumber)) {
                return false;
            }

            var no_digit = CardNumber.length;
            var oddoeven = no_digit & 1;
            var sum = 0;

            for (var count = 0; count < no_digit; count++) {
                var digit = parseInt(CardNumber.charAt(count));
                if (!((count & 1) ^ oddoeven)) {
                    digit *= 2;
                    if (digit > 9)
                        digit -= 9;
                }
                sum += digit;
            }
            if (sum % 10 == 0)
                return true;
            else
                return false;
        }

        /*************************************************************************\
         ArrayObject makeArray(int size)
         return the array object in the size specified.
         \*************************************************************************/
        function makeArray(size) {
            this.size = size;
            return this;
        }

        /*************************************************************************\
         CardType setCardNumber(cardnumber)
         return the CardType object.
         \*************************************************************************/
        function setCardNumber(cardnumber) {
            this.cardnumber = cardnumber;
            return this;
        }

        /*************************************************************************\
         CardType setCardType(cardtype)
         return the CardType object.
         \*************************************************************************/
        function setCardType(cardtype) {
            this.cardtype = cardtype;
            return this;
        }

        /*************************************************************************\
         CardType setExpiryDate(year, month)
         return the CardType object.
         \*************************************************************************/
        function setExpiryDate(year, month) {
            this.year = year;
            this.month = month;
            return this;
        }

        /*************************************************************************\
         CardType setLen(len)
         return the CardType object.
         \*************************************************************************/
        function setLen(len) {
// Create the len array.
            if (len.length == 0 || len == null)
                len = "13,14,15,16,19";

            var tmplen = len;
            n = 1;
            while (tmplen.indexOf(",") != -1) {
                tmplen = tmplen.substring(tmplen.indexOf(",") + 1, tmplen.length);
                n++;
            }
            this.len = new makeArray(n);
            n = 0;
            while (len.indexOf(",") != -1) {
                var tmpstr = len.substring(0, len.indexOf(","));
                this.len[n] = tmpstr;
                len = len.substring(len.indexOf(",") + 1, len.length);
                n++;
            }
            this.len[n] = len;
            return this;
        }

        /*************************************************************************\
         CardType setRules()
         return the CardType object.
         \*************************************************************************/
        function setRules(rules) {
// Create the rules array.
            if (rules.length == 0 || rules == null)
                rules = "0,1,2,3,4,5,6,7,8,9";

            var tmprules = rules;
            n = 1;
            while (tmprules.indexOf(",") != -1) {
                tmprules = tmprules.substring(tmprules.indexOf(",") + 1, tmprules.length);
                n++;
            }
            this.rules = new makeArray(n);
            n = 0;
            while (rules.indexOf(",") != -1) {
                var tmpstr = rules.substring(0, rules.indexOf(","));
                this.rules[n] = tmpstr;
                rules = rules.substring(rules.indexOf(",") + 1, rules.length);
                n++;
            }
            this.rules[n] = rules;
            return this;
        }

        </SCRIPT>
<? echo "\n\n";


echo '
<br><br><br>
<br><br><br><FONT FACE="Arial" SIZE="3">Need <A HREF="null" onClick="openhelp(); return false">help</A>?<BR>'."\n";

echo '	</TD></TR>'."\n\n";

echo '<TR><TD ALIGN="center"><HR SIZE="1" WIDTH="'.$tablewidth.'">
<FONT FACE="Arial" SIZE="2"><B>Return to:&nbsp;&nbsp;<A HREF="http://www.yellowstonelodging.biz">Yellowstone Lodging</A></B><BR></FONT>
<FONT FACE="Arial" SIZE="1"><I>Yellowstone Lodging will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.</I></FONT><BR><BR>
<br><br><br>
<br><br><br>
</TD></TR>'."\n";

echo '</TABLE>

</FORM> 


</CENTER>
</BODY>

</HTML>'."\n\n";


if(isset($adminsuccessmsg) && count($adminsuccessmsg) > 0 || isset($adminerrormsg) && count($adminerrormsg) > 0){
    if(isset($adminsuccessmsg) && count($adminsuccessmsg) > 0){
        $logentry = implode("\n",$adminsuccessmsg);
        $logentry = addslashes($logentry);
        @mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("'.$time.'","Web User","'.$pageid.'","'.$logentry.'")');
    }
    if(isset($adminerrormsg) && count($adminerrormsg) > 0){
        $logentry = implode("\n",$adminerrormsg);
        $logentry = addslashes($logentry);
        @mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("'.$time.'","Web User","'.$pageid.'","Error: '.$logentry.'")');
    }
}

?>