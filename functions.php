<?php

error_reporting(0);

function api_call($url, $post=array(), $params=array()) {
	if(!function_exists('curl_init')) {
	    return 'Error: curl is not installed.';
	}

	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_URL, $url);
	//curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
	//curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
	curl_setopt($ch, CURLOPT_HEADER, 0); // Include header in result? (0 = yes, 1 = no)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //true = return, false = print
	curl_setopt($ch, CURLOPT_TIMEOUT, 30); //seconds

	if(@$params['include_credentials'] !== false) {
		$post['username'] = api_username;
		$post['password'] = api_password;
	}

	if(is_array($post) && count($post) > 0) {
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	}

	$out = curl_exec($ch);
	curl_close($ch);

	return $out;
}
