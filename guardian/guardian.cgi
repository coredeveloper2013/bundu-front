#!/usr/bin/perl

# Apache Guardian, Version 1.0
#	Copyright 1997 by Fluid Dynamics <xav.com>
#	You are free to use the script, but please ask before you
#	distribute it.
#
# For latest version and help files, visit:
#	http://www.xav.com/scripts/guardian
# __________________________________________________________________


# Enter your email address:

	$email = 'yellowstonelodging@yellowstonelodging.biz';

# Enter the URL of your main page:

	$main_page = '/';

# Enter the URL to your search page, if you have one.  If not, just
# delete this line:

	$search_page = '/cgi-bin/search.cgi';

# Enter the location of sendmail on your system if it differs from
# the default:

	$mailprog = '/usr/sbin/sendmail';

# No further editing is necessary, but feel free to play around...
#
# __________________________________________________________________


# First label the error:
$code = "Mysterious Reason";
$code = "Lost File" if ($ENV{'QUERY_STRING'} eq '404');
$code = "Restricted Access" if ($ENV{'QUERY_STRING'} eq '401');
$code = "Failed Script" if ($ENV{'QUERY_STRING'} eq '500');

# Next mail a summary to the site administrator:
open(MAIL,"|$mailprog -t");
print MAIL "To: $email\n";
print MAIL "From: guardian\@xav.com (Apache Guardian)\n";
print MAIL "Subject: Guardian Report [$code]\n";
print MAIL "X-Priority: 1 (Highest)\n\n";
print MAIL "Flags tripped for attempted access to $ENV{'REDIRECT_URL'}\n";
print MAIL "by $ENV{'REMOTE_HOST'}.\n\n";
if ($ENV{'HTTP_REFERER'})
	{
	@terms = split(/\//,$ENV{'HTTP_REFERER'});
	print MAIL "Visitor linked in from $ENV{'HTTP_REFERER'}. ";
	print MAIL "You may wish to contact the administrator of $terms[0]//$terms[2].\n\n";
	}
else
	{print MAIL "No referring URL was logged in association with this visit.\n\n";}
print MAIL "Details follow:\n\n";
foreach $key (sort keys %ENV)
	{print MAIL "$key: $ENV{$key}\n";}
close(MAIL);


# Next print an explanation for the visitor:
print "Content-type: text/html\n\n";
print <<EOM;
<HTML>
<HEAD><TITLE>Apache Guardian: $code</TITLE></HEAD>
<BODY BGCOLOR="#FFFFFF" LINK="#CE0000" VLINK="#CE0000" ALINK="#FFFFFF">
<H2><TT>$code (Apache Error $ENV{'QUERY_STRING'})</TT></H2>
<BLOCKQUOTE>\n<B>
EOM
if ($ENV{'QUERY_STRING'} eq '401')
	{&explain_401;}
elsif ($ENV{'QUERY_STRING'} eq '404')
	{&explain_404;}
elsif ($ENV{'QUERY_STRING'} eq '500')
	{&explain_500;}
else
	{&default;}
print "</B></BLOCKQUOTE>\n</BODY>\n</HTML>\n";


sub explain_401
{
print <<EOM;
The document you requested has been password locked for a reason. If
you have misplaced your password, or have a compelling reason to view
the restricted information, please contact the <A
HREF="mailto:$email">site administrator</A>.<P>
Otherwise, please return to the page you were at before.
EOM
}


sub explain_404
{
print <<EOM;
The document you were searching for has either expired or migrated
elsewhere. To find documents which have moved, we suggest you start
at our <A HREF="$main_page">main page</A> or drop us a <A
HREF="mailto:$email">quick note</A>.
EOM
if ($search_page)
	{
	print "\n<P>You may also <A HREF=\"$search_page\">search</A>\n";
	print "for the document with our site's internal search engine.\n";
	}
}



sub explain_500
{
print <<EOM;
The script you're trying to access has suffered a fatal error. If you've
recently altered the code, you may try to isolate the error by running
<TT>perl -w $ENV{'REDIRECT_SCRIPT_FILENAME'}</TT>. Alternately, if you are a
visitor, please try again at a later time. The site administrator has
already been notified of the error; it may be helpful if you could
<A HREF="mailto:$email">contact us</A> with any additional information
about your form input.
EOM
}


sub default
{
print <<EOM;
There was an unexplained error in retrieving your requested file. You
may wish to try again from our <A HREF="$main_page">main page</A>, or
contact the <A HREF="mailto:$email">site administrator</A> with any
additional details about what you did to reach this error message.
EOM
}
