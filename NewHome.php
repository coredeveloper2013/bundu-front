<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Yellowstone Lodging</title>
    <meta charset="UTF-8">
    <meta name="author" content="Yellowstone Lodging">
    <meta name="description" content="All of our West Yellowstone studios, apartments and homes are completely furnished with everything you need for a self catered vacation!">
    <meta name="keywords" content="Yellowstone Lodging">

    <meta property="og:title" content="Yellowstone Lodging"/>
    <meta property="og:url" content="https://yellowstonelodging.biz/"/>
    <meta property="og:description" content="All of our West Yellowstone studios, apartments and homes are completely furnished with everything you need for a self catered vacation!"/>
    <meta property="og:image" content="https://yellowstonelodging.biz/images/banner/banner2.jpeg"/>



    <!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css"
          integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous"/>
    <!--<link rel="stylesheet" href="css/framework7.bundle.min.css">-->
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="container">
    <div id="main_wrapper">
        <ul class="main_menu_">
            <li><a href="">Home</a></li>
            <li><a href="./yellowstone-accommodation.htm">Studios</a></li>
            <li><a href="./yellowstone-apartment.htm">One Bedroom Apartments</a></li>
            <li><a href="./yellowstone-apartments.htm">Two Bedroom Apartments</a></li>
            <li><a href="./yellowstone-house.htm">Modular Home</a></li>
            <li><a href="">Contact</a></li>
        </ul>

        <div class="row">
            <div class="col-md-12">
                <form onsubmit="checkChildren(event)" class="custom_form" action="searched_result.php" method="get"
                      id="lodging_search">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="check_in">Check In Date:</label>
                                <input class="form-control check_in" type="text" name="start_date"
                                       placeholder="Select Check In Date" required
                                       onchange="selectCheckout()" >
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="check_out">Check Out Date:</label>
                                <input class="form-control check_out" type="text" name="end_date"
                                       placeholder="Select Check Out Date" disabled required>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="check_in">Number of Adults:</label>
                                <input class="form-control" type="number" name="adult" placeholder="Number of Adults" onkeyup="hideErrorMessage()"
                                       min="1" required>
                                <input type="hidden" class="form-control" name="rooms" min="1" value="1">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="check_out">Number of Children:</label>
                                <input class="form-control" onkeyup="checkChildren(event)"
                                       onchange="checkChildren(event)"
                                       type="number" name="children" placeholder="Number of Children" min="0" max="0">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-lg btn-primary" onclick="validate()">Continue
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <p>
                All of our West Yellowstone studios, apartments and homes are completely furnished with everything
                you need for a self catered vacation!
            </p>
            <?php
            if (!empty($resultArry['success'])) {
                foreach ($resultArry['success'] as $lodgingid => $eachLodgingArr) {
                    ?>
                    <div style="padding: 3px; border: 1px solid grey; margin: 7px; height: 150px; ">
                        <div style="float: left;"><img
                                    alt="Yellowstone lodging"
                                    src="<?php echo $eachLodgingArr['lodging_arr']['image']; ?>"
                                    style="height: 148px; border: 1px solid black;">
                        </div>

                        <div style="float: left; margin-left: 7px; padding-top: 20px; width:65%"><font
                                    style="font-size: 11pt" face="Arial">
                                <a href="<?php echo $eachLodgingArr['lodging_arr']['url']; ?>?type=<?php echo $eachLodgingArr['lodging_arr']['id'] . '&start_month=' . $_REQUEST['start_month'] . '&start_day=' . $_REQUEST['start_day'] . '&start_year=' . $_REQUEST['start_year'] . '&nights=' . $_REQUEST['nights']; ?>"><?php echo $eachLodgingArr['lodging_arr']['name']; ?></a>
                                <br/> <?php echo $eachLodgingArr['lodging_arr']['description']; ?>&nbsp;&nbsp;&nbsp;
                                <br/><a
                                        href="<?php echo $eachLodgingArr['lodging_arr']['url']; ?>?type=<?php echo $eachLodgingArr['lodging_arr']['id'] . '&start_month=' . $_REQUEST['start_month'] . '&start_day=' . $_REQUEST['start_day'] . '&start_year=' . $_REQUEST['start_year'] . '&nights=' . $_REQUEST['nights']; ?>">Details </a></font>
                        </div>

                        <div style="float: left; margin-left: 7px; padding-top: 20px; "><font
                                    style="font-size: 14pt" face="Arial">
                                Price:
                                $<?php echo array_sum($eachLodgingArr['price_arr']) / count($eachLodgingArr['price_arr']) ?>
                                <br/>
                            </font>
                            <font style="font-size: 11pt" face="Arial">
                                <a
                                        href="https://www.bundubashers.com/reserve_lodging_new.php?type=<?php echo $eachLodgingArr['lodging_arr']['id'] . '&start_month=' . $_REQUEST['start_month'] . '&start_day=' . $_REQUEST['start_day'] . '&start_year=' . $_REQUEST['start_year'] . '&nights=' . $_REQUEST['nights']; ?>">ORDER</a>
                            </font>
                        </div>

                    </div>
                    <div style="clear: both"></div>

                    <?php
                }
            } else if (!empty($resultArry['error'])) echo '<FONT FACE="Arial" SIZE="2"><B>' . implode(' ', $resultArry['error']) . "</B?</FONT><br /><br />";
            ?>
        </div>
        <div class="row">
            <p  style="margin: 0 0 2px 0">
                        <span style="font-size: 11pt"> <img
                                    alt="Yellowstone lodging"
                                    src="yellowstone-lodging-14.gif" width="15"
                                    height="15" border="0"></span>
                <a href="yellowstone-accommodation.htm">Yellowstone
                    studios</a> - fully equipped studios complete with
                kitchen and all you require for your Yellowstone
                holiday!&nbsp;&nbsp;&nbsp; <a
                        href="yellowstone-accommodation.htm">Details </a>
            </p>
        </div>
        <div class="row">
            <p  style="margin: 0 0 2px 0">
                        <span style="font-size: 11pt"> <img
                                    alt="Yellowstone lodging"
                                    src="yellowstone-lodging-14.gif" width="15"
                                    height="15" border="0"></span>
                <a href="west_yellowstone_apartment.htm"> Yellowstone
                    apartments</a> - <a href="yellowstone-apartment.htm">one</a> and <a href="yellowstone-apartments.htm"> two</a> West
                Yellowstone bedrooms apartments.&nbsp;&nbsp; <a
                        href="west_yellowstone_apartment.htm">Details</a>
            </p>
        </div>
        <div class="row">
            <p  style="margin: 0 0 2px 0">
                <img alt="Yellowstone
                  lodging" src="yellowstone-lodging-14.gif" width="15"
                     height="15" border="0">
                <a
                        href="file:///Users/sessel/Google%20Drive/Docs/My%20Web%20Sites/Yellowstone%20Lodging/west-yellowstone-homes.xhtml">Modular


                    homes</a> - these are small, two bedroom,
                prefabricated, self catering houses. We have two of
                them, <a href="yellowstone-house.htm">#9</a> and <a
                        href="west-yellowstone-house.htm">#10</a>
            </p>
        </div>
        <div class="row">
            <p>
                For immediate attention, please contact us at 406 646 1118, or
                <a href="mailto:yellowstonelodging@gmail.com?subject=West%20Yellowstone%20lodging">email<span
                            style="text-decoration: none"> </span> </a>us.&nbsp;
            </p>
        </div>
        <br>
        <div class="row">
            <p>Many who stay with us take one of our &nbsp; <a href="http://www.yellowstonenationalparktours.com/"
                                                               target="_blank"> Yellowstone tours</a>. Let the experts
                show you parts of Yellowstone that you might never find yourself.</p>
        </div>
        <div class="row">
            <p>
                The
                <a href="http://www.bundubus.com/" target="_blank">Grand
                    Canyon bus</a>, the Bundu Bus, also services
                Yellowstone.&nbsp; You can use the bus to travel from
                Las Vegas, Los Angeles, San Francisco, Salt Lake City
                and Phoenix, to and from West Yellowstone.
            </p>
        </div>
        <div class="row">
            <br><br>
            <div class="row">
                <div class="col-md-4">
                    <img style="width: 100%" alt="West Yellowstone studio" src="west-yellowstone-studio-8_10.jpg">
                </div>
                <div class="col-md-4">
                    <img style="width: 100%" src="yellowstone-two-bedroom-apartment-2.jpg" alt="Yellowstone apartment">
                </div>
                <div class="col-md-4">
                    <img style="width: 100%" src="6-yellowstone-apartment-4.jpg" alt="Yellowstone apartment">
                </div>
            </div>
            <br><br>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p class="text-center"><a href="javascript:void(0)" onclick="window.scrollTo(0, 0);">TOP</a></p>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js"
        integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>
<script>
    $(".check_in").flatpickr({
        allowInput: false,
        disableMobile: true,
        minDate: new Date(),
        altInput: true,
        altFormat: 'd F, Y'
    });

    function selectCheckout() {

        $(".check_out").prop('disabled', false);
        if ($(".check_in")) {
            var tomorrow = new Date($(".check_in").val());
            tomorrow.setDate(tomorrow.getDate() + 2);
            $(".check_out").flatpickr({
                allowInput: false,
                disableMobile: true,
                defaultDate: tomorrow,
                minDate: tomorrow, // 1 days from check_in
                altInput: true,
                altFormat: 'd F, Y'
            });
        }
        if ($('input[name=start_date]').val() != '') {
            _this = $('input[name=start_date]');
            _this.closest('.form-group').find('.alert_start_date').remove();
            // $('.alert_start_date').hide();
        }
        if ($('input[name=end_date]').val() != '') {
            _this = $('input[name=end_date]');
            _this.closest('.form-group').find('.alert_end_date').remove();
        }

    }

    $(function () {
        $("input[name=children]")[0].oninput = function () {
            this.setCustomValidity("");
        };
        $(document).mouseup(function(e) {
            var container = $(".alertChildPop");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.remove();
            }
        });
    });

    // -- validate formdata --
    function checkChildren(e) {
        var form = $('#lodging_search');
        var trigger = form.find('input[name="children"]');
        var v = parseInt(trigger.val());
        if (v > 0) {

            e.preventDefault();
            e.stopPropagation();
            trigger.closest('.form-group').addClass('has-error');
            trigger.closest('.form-control').addClass('is-invalid');
            trigger.closest('.form-group').find('.alertChild').remove();
            trigger.closest('.form-group').find('.alertChildPop').remove();
            trigger.closest('.form-group').append('<span class="alertChildPop">Sorry, children, 18 and younger are not permitted.</span>');
        } else if (isNaN(v)) {

            e.preventDefault();
            e.stopPropagation();
            trigger.closest('.form-group').removeClass('has-error');
            trigger.closest('.form-control').removeClass('is-invalid');
            trigger.closest('.form-group').find('.alertChild').remove();
            trigger.closest('.form-group').find('.alertChildPop').remove();
            trigger.closest('.form-group').append('<span class="alertChildPop">Please complete this field, even if the number of kids is zero. Thank you!</span>');
        } else {
            trigger.closest('.form-control').removeClass('is-invalid');
            trigger.closest('.form-group').find('.alertChild').remove();
            trigger.closest('.form-group').find('.alertChildPop').remove();
        }
    }

    // -- validate formdata --
    function validate() {
        $('.text-danger').remove();
        if ($('input[name=start_date]').val() == '') {
            _this = $('input[name=start_date]');
            _this.closest('.form-group').append('<span class="text-danger alert_start_date">Please select a check in date!</span>');
        } else {
            _this = $('input[name=start_date]');
            _this.closest('.form-group').find('.alert_start_date').remove();
        }
        if ($('input[name=end_date]').val() == '') {
            _this = $('input[name=end_date]');
            _this.closest('.form-group').append('<span class="text-danger alert_end_date">Please select a check out date!</span>');
        } else {
            _this = $('input[name=end_date]');
            _this.closest('.form-group').find('.alert_end_date').remove();
        }
        if ($('input[name=adult]').val() == '') {
            _this = $('input[name=adult]');
            _this.addClass('is-invalid');
            _this.closest('.form-group').append('<span class="text-danger alert_adult">Please enter number of adults.</span>');
        } else {
            _this = $('input[name=adult]');
            _this.removeClass('is-invalid');
            _this.closest('.form-group').find('.alert_adult').remove();
        }
    }
    function hideErrorMessage() {

        if ($('input[name=adult]').val() > 0) {
            _this = $('input[name=adult]');
            _this.removeClass('is-invalid');
            _this.closest('.form-group').find('.alert_adult').remove();
        }
        else{
            _this = $('input[name=adult]');
            _this.addClass('is-invalid');
            _this.closest('.form-group').find('.alert_adult').remove();
            _this.closest('.form-group').append('<span class="text-danger alert_adult">Please enter number of adults.</span>');
        }
    }


</script>
</body>
</html>
