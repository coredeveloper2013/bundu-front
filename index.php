<?php
include_once("functions.php");

if (isset($_REQUEST['start_month']) && isset($_REQUEST['start_day']) && isset($_REQUEST['start_year'])) {
    $resultJson = api_call('https://bundubashers.com/api/lodging_price.php?start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights']);
    $resultArry = json_decode($resultJson, true);
    /*echo '<pre>';
    print_r($resultArry);
    echo '</pre>';*/
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Yellowstone lodging, West Yellowstone motel</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css" integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous" />
    <style type="text/css">
#apDiv1 {
	position:absolute;
	left:73px;
	top:52px;
	width:255px;
	height:128px;
	z-index:1;
	color: #FFF;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
#apDiv2 {
	position:absolute;
	left:98px;
	top:355px;
	width:1050px;
	height:732px;
	z-index:2;
}
.check_in, .check_out{
    background-color: initial!important;
}
.submit_button {
    text-align: center;
}
form {
    padding: 40px;
}
.alertChildPop{
    position: absolute;
    right: 0;
    top: -40px;
    padding: 13px;
    background: #ffffff;
    width: 300px;
    box-shadow: 1px 1px 4px #999;
    border-radius: 4px;
    font-size: 13px;
    color: #e26106;
}
.alertChildPop::before{
    content: '';
    position: absolute;
    right: 16px;
    bottom: -7px;
    border-left: 15px solid #fdfdfd;
    border-top: 15px solid transparent;
    z-index: 1;
    transform: rotate(-45deg);
    box-shadow: -3px 4px 6px #e0e0e0;
}

</style>
    <meta content="Yellowstone motel, West Yellowstone lodging "
      name="description">
  </head>
  <body>
    <div style="position: absolute; left: 872px; top: 93px; height:
      114px; width:220px" id="apDiv1"> <font face="Arial"><font
          color="#FFFFFF"> <a name="TOP" href="index.htm"> <font
              color="#FFFFFF">HOME</font></a></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><br>
        </u> <font color="#FFFFFF"> <a
            href="yellowstone-accommodation.htm"><font color="#FFFFFF">
              Studios</font></a></font><u><br>
        </u> <font color="#FFFFFF"> <span style="text-decoration:
            none"><a href="yellowstone-apartment.htm"> <font
                color="#FFFFFF">One Bedroom Apartments</font></a><a
              href="yellowstone-apartments.htm"><font color="#FFFFFF"><br>
                Two Bedroom Apartments </font></a> </span></font><br>
        <a href="west-yellowstone-lodging.htm"><font color="#FFFFFF">Modular


            Homes</font></a><u><br>
        </u> <a href="yellowstone_apartment.htm"><font color="#FFFFFF">
            Contact</font></a></font></div>
    <div style="position: absolute; left: 98px; top: 362px" id="apDiv2">
        <form onsubmit="checkChildren(event)" class="custom_form" action="searched_result.php" method="get" id="lodging_search" >
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="check_in">Check In Date:</label>
                        <input class="form-control check_in" type="text" name="start_date" placeholder="Select Check In Date" readonly="readonly" required onchange="selectCheckout()" >
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="check_out">Check Out Date:</label>
                        <input class="form-control check_out" type="text" name="end_date" placeholder="Select Check Out Date" readonly="readonly" disabled required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="check_in">Number of Adults:</label>
                        <input class="form-control" type="number" name="adult" placeholder="Number of Adults" min="1" required>
                        <input type="hidden" class="form-control" name="rooms" min="1" value="1">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="check_out">Number of Children:</label>
                        <input class="form-control" onkeyup="checkChildren(event)" onchange="checkChildren(event)" type="number" name="children" placeholder="Number of Children" min="0"  max="0"  >
                    </div>
                </div>
            </div>
            <div class="submit_button"><button type="submit" class="btn btn-lg btn-primary" onclick="validate()">Continue</button></div>
        </form>
      <table width="1084" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td rowspan="1" colspan="2"><font style="font-size: 11pt"
                face="Arial">All of our West Yellowstone studios,
                apartments and homes are completely furnished with
                everything you need for a self catered vacation!<br>
              </font></td>
          </tr>

            <tr><td rowspan="1" colspan="2" >

                    
                    
                </td></tr>
            
          <tr>
              <td colspan="2">


              <div>
        <?php
        if (!empty($resultArry['success'])) {
        foreach($resultArry['success'] as $lodgingid => $eachLodgingArr)
        {
        ?>
            <div style="padding: 3px; border: 1px solid grey; margin: 7px; height: 150px; ">
                <div  style="float: left;"> <img
                      alt="Yellowstone lodging"
                      src="<?php echo $eachLodgingArr['lodging_arr']['image'];?>" style="height: 148px; border: 1px solid black;">
                </div>
                
                <div style="float: left; margin-left: 7px; padding-top: 20px; width:65%"><font style="font-size: 11pt" face="Arial">
                    <a href="<?php echo $eachLodgingArr['lodging_arr']['url'];?>?type=<?php echo $eachLodgingArr['lodging_arr']['id'].'&start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights'];?>"><?php echo $eachLodgingArr['lodging_arr']['name'];?></a>
                    <br /> <?php echo $eachLodgingArr['lodging_arr']['description'];?>&nbsp;&nbsp;&nbsp;
                    <br /><a
                      href="<?php echo $eachLodgingArr['lodging_arr']['url'];?>?type=<?php echo $eachLodgingArr['lodging_arr']['id'].'&start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights'];?>">Details </a></font>
                </div>

                <div style="float: left; margin-left: 7px; padding-top: 20px; "><font style="font-size: 14pt" face="Arial">
                        Price: $<?php echo array_sum($eachLodgingArr['price_arr']) / count($eachLodgingArr['price_arr'])?>
                        <br />
                        </font>
                        <font style="font-size: 11pt" face="Arial">
                        <a
                      href="https://www.bundubashers.com/reserve_lodging_new.php?type=<?php echo $eachLodgingArr['lodging_arr']['id'].'&start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights'];?>">ORDER</a>
                    </font>
                </div>    
                
            </div>
            <div style="clear: both"></div>
            
        <?php
        }
        } else if (!empty($resultArry['error'])) echo '<FONT FACE="Arial" SIZE="2"><B>'.implode(' ', $resultArry['error']) . "</B?</FONT><br /><br />";
        ?>
              </div>    
                
        </td>
          </tr>
            
            
          <tr>
            <td width="13"> <span style="font-size: 11pt"> <img
                  alt="Yellowstone lodging"
                  src="yellowstone-lodging-14.gif" width="15"
                  height="15" border="0"></span></td>
            <td width="1060"><font style="font-size: 11pt" face="Arial">
                <a href="yellowstone-accommodation.htm">Yellowstone
                  studios</a> - fully equipped studios complete with
                kitchen and all you require for your Yellowstone
                holiday!&nbsp;&nbsp;&nbsp; <a
                  href="yellowstone-accommodation.htm">Details </a></font></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="13"> <span style="font-size: 11pt"> <img
                  alt="Yellowstone lodging"
                  src="yellowstone-lodging-14.gif" width="15"
                  height="15" border="0"></span></td>
            <td width="1060"><font style="font-size: 11pt" face="Arial"><a
                  href="west_yellowstone_apartment.htm"> Yellowstone
                  apartments</a> - <a href="yellowstone-apartment.htm">one</a>
                and <a href="yellowstone-apartments.htm">two</a> West
                Yellowstone bedrooms apartments.&nbsp;&nbsp; <a
                  href="west_yellowstone_apartment.htm">Details</a> </font>
            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td> <span style="font-size: 11pt"> <img alt="Yellowstone
                  lodging" src="yellowstone-lodging-14.gif" width="15"
                  height="15" border="0"></span></td>
            <td><font style="font-size: 11pt" face="Arial"><a
href="file:///Users/sessel/Google%20Drive/Docs/My%20Web%20Sites/Yellowstone%20Lodging/west-yellowstone-homes.xhtml">Modular


                  homes</a> - these are small, two bedroom,
                prefabricated, self catering houses. We have two of
                them, <a href="yellowstone-house.htm">#9</a> and <a
                  href="west-yellowstone-house.htm">#10</a>. </font></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><font style="font-size: 11pt" face="Arial">For


                immediate attention, please contact us at 406 646 1118,
                or <a
href="mailto:yellowstonelodging@gmail.com?subject=West%20Yellowstone%20lodging">email<span
                    style="text-decoration: none"> </span> </a>us.&nbsp;
                <br>
              </font></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><font style="font-size: 11pt" face="Arial">Many

                who stay with us take one of our <a
                  href="http://www.yellowstonenationalparktours.com/"
                  target="_blank"> Yellowstone tours</a>. Let the
                experts show you parts of Yellowstone that you might
                never find yourself. </font></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><font style="font-size: 11pt" face="Arial">The

                <a href="http://www.bundubus.com/" target="_blank">Grand
                  Canyon bus</a>, the Bundu Bus, also services
                Yellowstone.&nbsp; You can use the bus to travel from
                Las Vegas, Los Angeles, San Francisco, Salt Lake City
                and Phoenix, to and from West Yellowstone.&nbsp; </font></td>
          </tr>
          <tr>
            <td colspan="2">
              <p align="center"><font size="2" face="Arial"><a
                    href="#TOP">TOP</a></font></p>
            </td>
          </tr>
          <tr>
            <td rowspan="1" colspan="2" valign="top">
              <table width="100%" cellspacing="0" cellpadding="2"
                border="0">
                <tbody>
                  <tr>
                    <td valign="top"><img alt="West Yellowstone studio"
                        src="west-yellowstone-studio-8_10.jpg"
                        width="366" height="243" border="1"><br>
                    </td>
                    <td valign="top"><img
                        src="yellowstone-two-bedroom-apartment-2.jpg"
                        alt="Yellowstone apartment" width="324"
                        height="243" border="1"><br>
                    </td>
                    <td valign="top"><img
                        src="6-yellowstone-apartment-4.jpg"
                        alt="Yellowstone apartment" width="366"
                        height="243" border="1"><br>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
            </td>
          </tr>
          <tr align="center">
            <td rowspan="1" colspan="2" valign="top"><font size="2"
                face="Arial"><a
href="file:///Users/sessel/Google%20Drive/Docs/My%20Web%20Sites/Yellowstone%20Lodging/index.htm#TOP">TOP</a></font></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><br>
            </td>
            <td valign="top"><br>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <table width="851" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="847">
            <div align="center"> <img alt="Lodging in Yellowstone"
                src="lodging-in-yellowstone.png" width="1250"
                height="6940" border="0"></div>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js" integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>
    <script>
        $(".check_in").flatpickr({
            allowInput:true,
            disableMobile: true,
            minDate: new Date(),
            altInput: true,
            altFormat: 'd F, Y'
        });

        function selectCheckout() {
            $(".check_out").prop('disabled', false);
            if ($(".check_in")) {
                var tomorrow = new Date($(".check_in").val());
                tomorrow.setDate(tomorrow.getDate() + 1);
                $(".check_out").flatpickr({
                    allowInput:true,
                    disableMobile: true,
                    defaultDate: tomorrow,
                    minDate: tomorrow, // 1 days from check_in
                    altInput: true,
                    altFormat: 'd F, Y'
                });
            }
        }

        $(function(){
            $("input[name=children]")[0].oninput= function () {
                this.setCustomValidity("");
            };
        });
        // -- validate formdata --
        function checkChildren(e) {
            var form = $('#lodging_search');
            var trigger = form.find('input[name="children"]');
            var v = parseInt(trigger.val());
            if(v > 0){

                e.preventDefault();
                e.stopPropagation();
                trigger.closest('.form-group').addClass('has-error');
                trigger.closest('.form-control').addClass('is-invalid');
                trigger.closest('.form-group').find('.alertChild').remove();
                trigger.closest('.form-group').find('.alertChildPop').remove();
                trigger.closest('.form-group').append('<span class="alertChildPop">Sorry, children, 18 and younger are not permitted.</span>');
            } else if(isNaN(v)){

                e.preventDefault();
                e.stopPropagation();
                trigger.closest('.form-group').removeClass('has-error');
                trigger.closest('.form-control').removeClass('is-invalid');
                trigger.closest('.form-group').find('.alertChild').remove();
                trigger.closest('.form-group').find('.alertChildPop').remove();
                trigger.closest('.form-group').append('<span class="alertChildPop">Please complete this field, even if the number of kids is zero. Thank you!</span>');
            } else {
                trigger.closest('.form-control').removeClass('is-invalid');
                trigger.closest('.form-group').find('.alertChild').remove();
                trigger.closest('.form-group').find('.alertChildPop').remove();
            }
        }

        // -- validate formdata --
        function validate() {
            $('.text-danger').remove();
            if ($('input[name=start_date]').val() == '') {
                _this = $('input[name=start_date]');
                _this.closest('.form-group').append('<span class="text-danger alert_start_date">Please select a check in date!</span>');
            }else {
                _this = $('input[name=start_date]');
                _this.closest('.form-group').find('.alert_start_date').remove();
            }
            if ($('input[name=end_date]').val() == '') {
                _this = $('input[name=end_date]');
                _this.closest('.form-group').append('<span class="text-danger alert_end_date">Please select a check out date!</span>');
            }else {
                _this = $('input[name=end_date]');
                _this.closest('.form-group').find('.alert_end_date').remove();
            }
            if ($('input[name=adult]').val() == '') {
                _this = $('input[name=adult]');
                _this.addClass('is-invalid');
                _this.closest('.form-group').append('<span class="text-danger alert_adult">Please enter number of adult!</span>');
            }else {
                _this = $('input[name=adult]');
                _this.removeClass('is-invalid');
                _this.closest('.form-group').find('.alert_adult').remove();
            }
        }
    </script>
</html>
