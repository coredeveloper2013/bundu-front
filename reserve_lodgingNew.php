<?php
$d1 = (isset($_REQUEST['start_date'])) ? $_REQUEST['start_date'] : date('d-m-y');
$d2 = (isset($_REQUEST['end_date'])) ? $_REQUEST['end_date'] : date('d-m-y');
$start = strtotime($d1);
$end = strtotime($d2);
$diff = abs($start - $end);
$days = floor($diff / (60 * 60 * 24));
$_REQUEST['nights'] = $days;
$_REQUEST['end_date'] = null;
$_REQUEST['start_date'] = null;

$start = mktime(0, 0, 0, $_REQUEST['start_month'], $_REQUEST['start_day'], $_REQUEST['start_year']);
$_REQUEST['nights'] = number_format($_REQUEST['nights'], 0, '', '');
$end = strtotime("+" . $_REQUEST['nights'] . " days", $start);
//echo $start.' : '.$seasons[0].'<BR>'.$end.' : '.end($seasons);

$subTotal = ($_REQUEST['nights'] * $_REQUEST['price'] * $_REQUEST['total_room']);
$subTotal = number_format($subTotal, 2, '.', '');

$tax = ($subTotal * .10) + $_REQUEST['nights'] * 1;
$total = $subTotal + $tax;
$total = number_format($total, 2, '.', '');
$tax = number_format($tax, 2, '.', '');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Yellowstone Lodging</title>
    <meta charset="UTF-8">
    <meta name="author" content="Yellowstone Lodging">
    <meta name="description" content="All of our West Yellowstone studios, apartments and homes are completely furnished with everything you need for a self catered vacation!">
    <meta name="keywords" content="Yellowstone Lodging">

    <meta property="og:title" content="Yellowstone Lodging"/>
    <meta property="og:url" content="https://yellowstonelodging.biz/"/>
    <meta property="og:description" content="All of our West Yellowstone studios, apartments and homes are completely furnished with everything you need for a self catered vacation!"/>
    <meta property="og:image" content="https://yellowstonelodging.biz/images/banner/banner2.jpeg"/>



    <!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css"
          integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous"/>
    <!--<link rel="stylesheet" href="css/framework7.bundle.min.css">-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
          crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body BGCOLOR="#E1E1E1" TEXT="#000000" TOPMARGIN="0" style="background-color: #E1E1E1">

<div class="container no-padding">

    <div class="_banner text-center" style="width: 100%;background-image: url('./images/banner/banner2.jpeg');">
        <div class="_content">
            <h1 class="_title_heading">
                <a href="index.php">Yellowstone Lodging</a></h1>
            <a href="javascript:void(0)" onclick="window.history.back();" class="_rollBack"><i class="fas fa-arrow-left 3x"></i></a>
        </div>
    </div>
    <div class="jumbotron" style="margin-bottom: 0;background:white">
        <div class="container">
            <table class="table table-bordered  ">
                <thead>
                <tr>
                    <th colspan="2" class="text-center" style="font-size: 18px">Payment Details</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td width="30%" class="text-right"><b>Room Name :</b></td>
                    <td width="70%"><?php echo $_REQUEST['type_name']; ?></td>
                </tr>
                <tr>
                    <td width="30%" class="text-right"><b>Dates : </b></td>
                    <td width="70%">
                        <?php
                        echo date("M j, Y", $start);
                        echo ' - ' . date("M j, Y", $end) . ' (' . $_REQUEST['nights'] . ' Night';
                        if ($_REQUEST['nights'] > 1) {
                            echo '\'s';
                        }
                        ?>
                        )
                    </td>
                </tr>
                <tr>
                    <td width="30%" class="text-right"><b>Guests : </b></td>
                    <td width="70%"><?php echo $_REQUEST['guests']; ?></td>
                </tr>
                <tr>
                    <td width="30%" class="text-right"><b>Total Room :</b></td>
                    <td width="70%"><?php echo $_REQUEST['total_room']; ?></td>
                </tr>

                <tr style="background-color: rgba(230,227,206,0.56)"">
                    <td colspan="2" class="text-right p-1 pr-4" style="border-bottom: 0"><b>Subtotal
                            :&nbsp;&nbsp;$ <?php echo $subTotal ?>&nbsp;&nbsp; </b>
                    </td>
                </tr>
                <tr style="background-color: rgba(230,227,206,0.56)">
                    <td colspan="2" class="text-right p-1 pr-4" style="border: 0"><b>Tax
                            :&nbsp;&nbsp;$ <?php echo $tax; ?> &nbsp; </b></td>
                </tr>
                <tr style="background-color: #afa6a7">
                    <td colspan="2" class="text-right p-1 pr-4"><b>Total:&nbsp;&nbsp;$ <?php echo $total ?>
                            &nbsp;&nbsp; </b></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="container pt-3">
            <form id="checkoutForm" NAME="resform" onSubmit="checkexp(event);">
                <h1 class="form-header">Complete Your Reservation</h1>
                <div class="form-group row pb-2">
                    <label for="name" class="col-sm-4 col-form-label text_position">Name <span
                                class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="phone_homebus" class="col-sm-4 col-form-label text_position">Home/Business
                        Phone </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="phone_homebus" name="phone_homebus"
                               placeholder="Home/Business Phone" required>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="phone_cell" class="col-sm-4 col-form-label text_position">Cell Phone <span
                                class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="phone_cell" id="phone_cell"
                               placeholder="Cell Phone"
                               required>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="email" class="col-sm-4 col-form-label text_position">Email Address <span
                                class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" name="email" id="email"
                               placeholder="Email Address"
                               required>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="inputPassword" class="col-sm-4 col-form-label text_position">Re-type Email Address
                        <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" name="email2" id="email2"
                               placeholder="Re-type Email Address"
                               required>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="cc_num" class="col-sm-4 col-form-label text_position">Credit card number <span
                                class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="cc_num" id="cc_num"
                               placeholder="Credit card number"
                               required>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="cc_expdate_month" class="col-sm-4 col-form-label text_position">Expiration date
                        <span
                                class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <SELECT class="form-control" NAME="cc_expdate_month" ID="cc_expdate_month">
                            <?php
                            for ($i = 1; $i < 13; $i++) {
                                echo '<OPTION VALUE="' . date("m", mktime("0", "0", "0", $i, "1", "2005")) . '"';
                                if (isset($_REQUEST['cc_expdate_month']) && $_REQUEST['cc_expdate_month'] == $i || date("m") == $i): echo " SELECTED"; endif;
                                echo '>' . $i . ' (' . date("M", mktime("0", "0", "0", $i, "1", "2005")) . ')</OPTION>';
                            } ?>
                        </SELECT>
                    </div>
                    <div class="col-sm-4">
                        <SELECT class="form-control" NAME="cc_expdate_year" ID="cc_expdate_year">

                            <?php
                            for ($i = date("Y"); $i < (date("Y") + 11); $i++) {
                                echo '<OPTION VALUE="' . date("y", mktime("0", "0", "0", "1", "1", $i)) . '"';
                                if (isset($_REQUEST['cc_expdate_year']) && $_REQUEST['cc_expdate_year'] == date("y", mktime("0", "0", "0", "1", "1", $i))): echo " SELECTED"; endif;
                                echo '>' . $i . '</OPTION>';
                            } ?>
                        </SELECT>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="cc_scode" class="col-sm-4 col-form-label text_position">Security code <span class="text-danger">*</span> </label>
                    <div class="col-sm-8">
                        <input class="form-control" name="cc_scode"  placeholder="Security code " id="cc_scode" required>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label for="cc_zip" class="col-sm-4 col-form-label text_position">Zip/Postal code
                        <p style="font-size:8pt; font-weight:normal; color:#666666;">Please provide the zip/postal code associated with your card.
                            If your country doesn’t have zip/postal codes, please omit. </p></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="cc_zip" id="cc_zip" placeholder="Zip/Postal code">
                    </div>
                </div>

                <div class="form-group row pb-2">
                    <label for="comments" class="col-sm-4 col-form-label text_position">Comments </label>
                    <div class="col-sm-8">
                        <textarea class="form-control" name="comments" id="comments" placeholder="Comments" COLS="30"></textarea>
                    </div>
                </div>

                <INPUT TYPE="hidden" NAME="type" ID="type" VALUE="<?php echo $_REQUEST['type']; ?>">
                <INPUT TYPE="hidden" NAME="start_month" ID="start_month"
                       VALUE="<?php echo $_REQUEST['start_month']; ?>">
                <INPUT TYPE="hidden" NAME="start_day" ID="start_day" VALUE="<?php echo $_REQUEST['start_day']; ?>">
                <INPUT TYPE="hidden" NAME="start_year" ID="start_year"
                       VALUE="<?php echo $_REQUEST['start_year']; ?>">
                <INPUT TYPE="hidden" NAME="nights" ID="nights" VALUE="<?php echo $_REQUEST['nights']; ?>">
                <INPUT TYPE="hidden" NAME="guests" ID="guests" VALUE="<?php echo $_REQUEST['guests']; ?>">
                <INPUT TYPE="hidden" NAME="end_date" ID="guests" VALUE="<?php echo $end; ?>">
                <INPUT TYPE="hidden" NAME="total" ID="total" VALUE="<?php echo $total; ?>">
                <INPUT TYPE="hidden" NAME="total_room" ID="total" VALUE="<?php echo $_REQUEST['total_room']; ?>">

                <div class="form-group">
                    <div class="col-sm-12 text-right pt-4">
                        <input class="btn btn-lg btn-success" TYPE="submit" VALUE="Submit Reservation">
                    </div>
                </div>


                <button style="display: none" class="btn btn-default" data-toggle="modal"
                        data-target="#confirm-submit"
                        id="confirmSubmit">dfdf
                </button>
                <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title">By submitting this reservation, I understand: </h3>
                            </div>
                            <div class="modal-body">
                                <p><br>
                                <ul class="text-left text-success">
                                    <li>No changes or cancellations are permitted, and refunds will not be given.
                                    </li>
                                    <li>Children under 18 and pets are not permitted.</li>
                                    <li>Check in after 9.30 pm are not permitted.</li>
                                    <li>Refunds will not be given if  you do not check in by 9.30 pm.</li>
                                </ul>
                                <br>
                                        <h4>Thank you!</h4></p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="confirmFinalReservation">Confirm Reservation
                                </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>


        </div>
        <div class="col-md-12 text-center pt-5">
            <FONT FACE="Arial" SIZE="3">Need <A HREF="null" onClick="openhelp(); return false">help</A>?<BR>
                <hr size="1" width="100%">
                <FONT FACE="Arial" SIZE="2"><B>Return to:&nbsp;&nbsp;<A HREF="http://www.yellowstonelodging.biz">Yellowstone
                            Lodging</A></B><BR></FONT>
                <FONT FACE="Arial" SIZE="1"><I>Yellowstone Lodging will not be bound by any prices that are
                        generated maliciously or
                        in error, and that are not its regular prices as detailed on its web sites.</I></FONT>
        </div>
    </div>

    <button style="display: none " class="btn btn-default" data-toggle="modal" data-target="#confirm-submit1"
            id="error">Submit
    </button>
    <div class="modal fade" id="confirm-submit1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Please Input valid Information !</h3>
                </div>
                <div class="modal-body">
                    <ul class="text-left text-warning" id="error_message">

                    </ul>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js"
        integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>
<script>


    $(".check_in").flatpickr({
        minDate: '<?php echo date('Y-m-d') ?>',
        defaultDate: '<?php echo $d1 ?>',
        altInput: true,
        disableMobile: true,
        altFormat: 'd M, Y'

    });

    $(".check_out").flatpickr({
        defaultDate: '<?php echo $d2 ?>',
        minDate: new Date($(".check_in").val()).fp_incr(1), // 1 days from check_in
        altInput: true,
        disableMobile: true,
        altFormat: 'd M, Y'
    });

    function selectNight() {
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var check_in = new Date($(".check_in").val());
        var check_out = new Date($(".check_out").val());
        var diffDays = Math.round(Math.abs((check_in.getTime() - check_out.getTime()) / (oneDay)));
        $('#nights').val(diffDays);
    }

    function selectCheckout() {
        $('#nights').val(1);
        var check_in = new Date($(".check_in").val());
        $(".check_out").prop('disabled', false);
        if ($(".check_in")) {
            $(".check_out").flatpickr({
                defaultDate: new Date($(".check_in").val()).fp_incr(1),
                minDate: new Date($(".check_in").val()).fp_incr(1), // 1 days from check_in
                altInput: true,
                disableMobile: true,
                altFormat: 'd M, Y'
            });
        }

        // Loop over each select option
        $("#start_year > option").each(function () {
            // Check for the matching data
            if ($(this).val() == check_in.getFullYear()) {
                // Select the matched option
                $(this).prop("selected", true);
            }
        });

        // Loop over each select option
        $("#start_month > option").each(function () {
            // Check for the matching data
            if ($(this).val() == check_in.getMonth() + 1) {
                // Select the matched option
                $(this).prop("selected", true);
            }
        });

        // Loop over each select option
        $("#start_day > option").each(function () {
            // Check for the matching data
            if ($(this).val() == check_in.getDate()) {
                // Select the matched option
                $(this).prop("selected", true);
            }
        });
    }

    function openhelp() {
        open("alswestwardhelp.html", "helpwin", "width=100%,height=auto,location=no,menubar=no,resizable=yes,status=no,toolbar=no");
    }

    //CC Validation Script Author: Simon Tneoh (tneohcb@pc.jaring.my)
    var Cards = new Array();
    Cards[0] = new CardType("MasterCard", "51,52,53,54,55", "16");
    var MasterCard = Cards[0];
    Cards[1] = new CardType("VisaCard", "4", "13,16");
    var VisaCard = Cards[1];
    Cards[2] = new CardType("AmExCard", "34,37", "15");
    var AmExCard = Cards[2];
    var LuhnCheckSum = Cards[3] = new CardType();

    function checkexp(e) {
        e.preventDefault();
        e.stopPropagation();

        var r = true;
        var msg = '';
        if (document.getElementById("cc_num").value == "") {
            msg += ' <li>Please enter your credit card number.' + "</li>";
            r = false;
        }
        if (document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == "") {
            msg += ' <li>Please choose an expiration date for your credit card.' + "</li>";
            r = false;
        }
        if (r) {
            var tmpyear;
            if (document.getElementById("cc_expdate_year").value > 96) {
                tmpyear = "19" + document.getElementById("cc_expdate_year").value;
            } else if (document.getElementById("cc_expdate_year").value < 21) {
                tmpyear = "20" + document.getElementById("cc_expdate_year").value;
            } else {
                msg += 'The card expiration year is not valid.' + "\n";
            }
            tmpmonth = document.getElementById("cc_expdate_month").value;
            if (!(new CardType()).isExpiryDate(tmpyear, tmpmonth)) {
                msg += '<li>Your credit card has already expired.' + "</li>";
            }
            card = false;
            for (var n = 0; n < Cards.length; n++) {
                if (Cards[n].checkCardNumber(document.getElementById("cc_num").value, tmpyear, tmpmonth)) {
                    card = true;
                    break;
                }
            }
            if (!card) {
                msg += ' <li>Your credit card number does not appear to be valid.' + "</li>";
            }
        }
        if (document.getElementById("cc_zip").value == "") {
            msg += '<li> Please provide the zip or postal code  associated with your credit card .' + "</li>";
        }
        if (document.getElementById("email").value != document.getElementById("email2").value) {
            msg += ' <li>Email Address and Re-type Email Address do not match.</li> <li> Please check carefully to be sure you have entered the correct email address in both places.' + "</li>";
        }
        if (msg != "") {
            // alert(msg);
            $('#error_message').html(msg)
            $('#error').click()
        } else {


            $('#confirmSubmit').click()
            $('#confirmFinalReservation').on('click', function () {
                $.ajax({
                    type: 'POST',
                    url: 'http://localhost/bundu-admin/api/NewReserve.php',
                    data: $('#checkoutForm').serialize(),
                    success: function (res) {
                        swal("Reservation Success!", "Thanks for Choosing us!", "success")
                            .then((value) => {
                                if (value) {
                                    window.location = 'http://localhost/bund/reserve_success.php?id=' + res;
                                }
                            });

                    }
                });
            });
        }


    }


    /*************************************************************************\
     Object CardType([String cardtype, String rules, String len, int year,
     int month])
     cardtype    : type of card, eg: MasterCard, Visa, etc.
     rules       : rules of the cardnumber, eg: "4", "6011", "34,37".
     len         : valid length of cardnumber, eg: "16,19", "13,16".
     year        : year of expiry date.
     month       : month of expiry date.
     eg:
     var VisaCard = new CardType("Visa", "4", "16");
     var AmExCard = new CardType("AmEx", "34,37", "15");
     \*************************************************************************/
    function CardType() {
        var n;
        var argv = CardType.arguments;
        var argc = CardType.arguments.length;

        this.objname = "object CardType";

        var tmpcardtype = (argc > 0) ? argv[0] : "CardObject";
        var tmprules = (argc > 1) ? argv[1] : "0,1,2,3,4,5,6,7,8,9";
        var tmplen = (argc > 2) ? argv[2] : "13,14,15,16,19";

        this.setCardNumber = setCardNumber;  // set CardNumber method.
        this.setCardType = setCardType;  // setCardType method.
        this.setLen = setLen;  // setLen method.
        this.setRules = setRules;  // setRules method.
        this.setExpiryDate = setExpiryDate;  // setExpiryDate method.

        this.setCardType(tmpcardtype);
        this.setLen(tmplen);
        this.setRules(tmprules);
        if (argc > 4)
            this.setExpiryDate(argv[3], argv[4]);

        this.checkCardNumber = checkCardNumber;  // checkCardNumber method.
        this.getExpiryDate = getExpiryDate;  // getExpiryDate method.
        this.getCardType = getCardType;  // getCardType method.
        this.isCardNumber = isCardNumber;  // isCardNumber method.
        this.isExpiryDate = isExpiryDate;  // isExpiryDate method.
        this.luhnCheck = luhnCheck;// luhnCheck method.
        return this;
    }

    /*************************************************************************\
     boolean checkCardNumber([String cardnumber, int year, int month])
     return true if cardnumber pass the luhncheck and the expiry date is
     valid, else return false.
     \*************************************************************************/
    function checkCardNumber() {
        var argv = checkCardNumber.arguments;
        var argc = checkCardNumber.arguments.length;
        var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
        var year = (argc > 1) ? argv[1] : this.year;
        var month = (argc > 2) ? argv[2] : this.month;

        this.setCardNumber(cardnumber);
        this.setExpiryDate(year, month);

        if (!this.isCardNumber())
            return false;
        if (!this.isExpiryDate())
            return false;

        return true;
    }

    /*************************************************************************\
     String getCardType()
     return the cardtype.
     \*************************************************************************/
    function getCardType() {
        return this.cardtype;
    }

    /*************************************************************************\
     String getExpiryDate()
     return the expiry date.
     \*************************************************************************/
    function getExpiryDate() {
        return this.month + "/" + this.year;
    }

    /*************************************************************************\
     boolean isCardNumber([String cardnumber])
     return true if cardnumber pass the luhncheck and the rules, else return
     false.
     \*************************************************************************/
    function isCardNumber() {
        var argv = isCardNumber.arguments;
        var argc = isCardNumber.arguments.length;
        var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
        if (!this.luhnCheck())
            return false;

        for (var n = 0; n < this.len.size; n++)
            if (cardnumber.toString().length == this.len[n]) {
                for (var m = 0; m < this.rules.size; m++) {
                    var headdigit = cardnumber.substring(0, this.rules[m].toString().length);
                    if (headdigit == this.rules[m])
                        return true;
                }
                return false;
            }
        return false;
    }

    /*************************************************************************\
     boolean isExpiryDate([int year, int month])
     return true if the date is a valid expiry date,
     else return false.
     \*************************************************************************/
    function isExpiryDate() {
        var argv = isExpiryDate.arguments;
        var argc = isExpiryDate.arguments.length;

        year = argc > 0 ? argv[0] : this.year;
        month = argc > 1 ? argv[1] : this.month;

        if (!isNum(year + ""))
            return false;
        if (!isNum(month + ""))
            return false;
        today = new Date();
        expiry = new Date(year, month);
        if (today.getTime() > expiry.getTime())
            return false;
        else
            return true;
    }

    /*************************************************************************\
     boolean isNum(String argvalue)
     return true if argvalue contains only numeric characters,
     else return false.
     \*************************************************************************/
    function isNum(argvalue) {
        argvalue = argvalue.toString();

        if (argvalue.length == 0)
            return false;

        for (var n = 0; n < argvalue.length; n++)
            if (argvalue.substring(n, n + 1) < "0" || argvalue.substring(n, n + 1) > "9")
                return false;

        return true;
    }

    /*************************************************************************\
     boolean luhnCheck([String CardNumber])
     return true if CardNumber pass the luhn check else return false.
     Reference: http://www.ling.nwu.edu/~sburke/pub/luhn_lib.pl
     \*************************************************************************/
    function luhnCheck() {
        var argv = luhnCheck.arguments;
        var argc = luhnCheck.arguments.length;

        var CardNumber = argc > 0 ? argv[0] : this.cardnumber;

        if (!isNum(CardNumber)) {
            return false;
        }

        var no_digit = CardNumber.length;
        var oddoeven = no_digit & 1;
        var sum = 0;

        for (var count = 0; count < no_digit; count++) {
            var digit = parseInt(CardNumber.charAt(count));
            if (!((count & 1) ^ oddoeven)) {
                digit *= 2;
                if (digit > 9)
                    digit -= 9;
            }
            sum += digit;
        }
        if (sum % 10 == 0)
            return true;
        else
            return false;
    }

    /*************************************************************************\
     ArrayObject makeArray(int size)
     return the array object in the size specified.
     \*************************************************************************/
    function makeArray(size) {
        this.size = size;
        return this;
    }

    /*************************************************************************\
     CardType setCardNumber(cardnumber)
     return the CardType object.
     \*************************************************************************/
    function setCardNumber(cardnumber) {
        this.cardnumber = cardnumber;
        return this;
    }

    /*************************************************************************\
     CardType setCardType(cardtype)
     return the CardType object.
     \*************************************************************************/
    function setCardType(cardtype) {
        this.cardtype = cardtype;
        return this;
    }

    /*************************************************************************\
     CardType setExpiryDate(year, month)
     return the CardType object.
     \*************************************************************************/
    function setExpiryDate(year, month) {
        this.year = year;
        this.month = month;
        return this;
    }

    /*************************************************************************\
     CardType setLen(len)
     return the CardType object.
     \*************************************************************************/
    function setLen(len) {
// Create the len array.
        if (len.length == 0 || len == null)
            len = "13,14,15,16,19";

        var tmplen = len;
        n = 1;
        while (tmplen.indexOf(",") != -1) {
            tmplen = tmplen.substring(tmplen.indexOf(",") + 1, tmplen.length);
            n++;
        }
        this.len = new makeArray(n);
        n = 0;
        while (len.indexOf(",") != -1) {
            var tmpstr = len.substring(0, len.indexOf(","));
            this.len[n] = tmpstr;
            len = len.substring(len.indexOf(",") + 1, len.length);
            n++;
        }
        this.len[n] = len;
        return this;
    }

    /*************************************************************************\
     CardType setRules()
     return the CardType object.
     \*************************************************************************/
    function setRules(rules) {
// Create the rules array.
        if (rules.length == 0 || rules == null)
            rules = "0,1,2,3,4,5,6,7,8,9";

        var tmprules = rules;
        n = 1;
        while (tmprules.indexOf(",") != -1) {
            tmprules = tmprules.substring(tmprules.indexOf(",") + 1, tmprules.length);
            n++;
        }
        this.rules = new makeArray(n);
        n = 0;
        while (rules.indexOf(",") != -1) {
            var tmpstr = rules.substring(0, rules.indexOf(","));
            this.rules[n] = tmpstr;
            rules = rules.substring(rules.indexOf(",") + 1, rules.length);
            n++;
        }
        this.rules[n] = rules;
        return this;
    }

</script>
</body>
</html>