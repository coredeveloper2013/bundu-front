<?php
include_once("functions.php");

$url_part = '';
if (isset($_REQUEST['start_month']) && isset($_REQUEST['start_day']) && isset($_REQUEST['start_year'])) {
    $resultJson = api_call('https://bundubashers.com/api/lodging_price.php?start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights']);
    $resultArry = json_decode($resultJson, true);
    if (!empty($resultArry['success'][5])) {
        $eachLodgingArr = $resultArry['success'][5];
    }
    $url_part = '&start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Yellowstone apartments, cheap West Yellowstone lodging</title>
<style type="text/css">
#apDiv1 {
	position:absolute;
	left:73px;
	top:52px;
	width:189px;
	height:128px;
	z-index:1;
	color: #FFF;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
#apDiv2 {
	position:absolute;
	left:63px;
	top:303px;
	width:753px;
	height:732px;
	z-index:2;
}
</style>
<meta name="description" content="Cheap Yellowstone lodging, West Yellowstone apartment">
</head>

<body>
<div id="apDiv3" style="position: absolute; left: 851px; top: 101px; height: 114px; width:407px">
	<font face="Arial"><font color="#FFFFFF">
	<a href="index.htm" name="TOP0">
	<font color="#FFFFFF">HOME</font></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
					<u><br />
  	</u>
  	<font color="#FFFFFF">
	<a href="yellowstone-accommodation.htm"><font color="#FFFFFF">
	Studios</font></a></font><u><br>
	</u>
	<font color="#FFFFFF">
	<span style="text-decoration: none"><a href="yellowstone-apartment.htm">
	<font color="#FFFFFF">One Bedroom 
	Apartments</font></a><a href="yellowstone-apartments.htm"><br>
	<font color="#FFFFFF">Two Bedroom Apartments
	</font></a>
	</span></font><u><br />
	</u>
<a href="yellowstone_apartment.htm"><font color="#FFFFFF">
	Contact</font></a></font></div>
<div id="apDiv2" style="position: absolute; left: 93px; top: 359px">
  <table width="752" border="0" cellpadding="0" cellspacing="1">
    <tr>
      <td width="18">
				<font size="2" face="Arial">
				<img border="0" src="Yellowstone_lodging.png" width="12" height="12" align="left"></font></td>
      <td width="728"><font size="2" face="Arial"><strong>One bedroom 
		Yellowstone apartments&nbsp;&nbsp; </strong></font>
				<font face="Arial" style="font-size: 9pt">
				<em>Also available: <a href="yellowstone-apartments.htm">two bedroom apartments,</a>
		<a href="yellowstone-accommodation.htm">studios</a></em></font></td>
    </tr>
    <tr>
      <td colspan="2"><font size="2" face="Arial">These moderate level Yellowstone apartments are only a mile from the main entrance to Yellowstone National Park. They are fully furnished, with Internet, and have all the linens, sheets and towels you may need. This Yellowstone lodging also has pots, pans, knives and forks - in fact everything that may be required for a self catering Yellowstone vacation.  </font></td>
    </tr>
    <tr>
      <td colspan="2"><font face="Arial" size="2">As mentioned, the apartments have one 
						bedroom, with a queen bed in the bedroom.&nbsp; There is 
						a single fold out sofa sleeper.&nbsp; 
						The apartments also have a full bathroom, a 
      small kitchen, and a living/dining area.&nbsp;No more than three people are permitted. 
		Please take a look at the <a href="#Nightly_Rates">prices</a>.</font></td>
    </tr>
    <tr>
      <td colspan="2">
          <p><font size="2" face="Arial">
                      <?php
                        if (!empty($eachLodgingArr['price_arr'])) {
                            echo '<span style="font-weight: bold;">Price: $';
                            echo array_sum($eachLodgingArr['price_arr']) / count($eachLodgingArr['price_arr']);
                            echo '</span>&nbsp;&nbsp;&nbsp;';
                        }
                        ?>
                        Please <a
                    href="https://www.bundubashers.com/reserve_lodging_new.php?type=5<?php echo $url_part;?>">order
                    here and see the prices</a>.&nbsp; <br>
                </font></p>
          
          <font face="Arial" size="2"><b>Please order these 
						Yellowstone apartments <a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
		<font color="#FF0000">here</font></a></b>, </font> 
		<font size="2" face="Arial">but 
						note our </font>  
		<font color="#FFFFFF" face="Arial" size="2"> <a target="_blank" href="yellowstone-lodging-cancellation-policy.htm"> cancellation, payment 
		and check in policies</a></font><font face="Arial" size="2"> </font>
		<font face="Arial"> <font size="2">first!&nbsp;<span style="font-weight: 700"> </span>  
		</font> </font>
		<font size="2" face="Arial"><span style="font-weight: 700"> 
		<font color="#FF0000">NO CANCELLATIONS ACCEPTED!&nbsp; </font> </span>  
		Please be aware that your booking is not 
						confirmed until you receive a confirmation email from 
	  us.</font></td>
    </tr>
    <tr>
      <td colspan="2">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
				<img border="1" src="apart2-2.jpg" width="365" height="243" alt="Yellowstone lodging"></td>
				<td align="center">
				<img border="1" src="apt6-3.jpg" width="365" height="243" alt="Yellowstone apartment"></td>
			</tr>
		</table>
		</td>
    </tr>
    <tr>
      <td colspan="2">
		<p align="justify">
									<font face="Arial" size="2">Please take a 
									look at 
									<a href="yellowstone-lodging-images.htm">more pictures of each 
		apartment</a>.</font></td>
    </tr>
    <tr>
      <td colspan="2">
		<p align="center"><font size="2" face="Arial"><a href="#TOP">TOP</a></font></td>
    </tr>
    <tr>
      <td colspan="2"><div align="center"><font face="Arial" size="2"><strong>
		<a name="Nightly_Rates">Nightly Rates</a></strong></font></div></td>
    </tr>
    <tr>
      <td colspan="2" align="center">
				<table border="0" width="100%" cellpadding="0">
					<tr>
						<td align="center" width="106"><b>
						<font face="Arial" size="2">Nov 3 - Apr 15</font></b></td>
						<td align="center"><b><font face="Arial" size="2">Apr 16 
						- May 14</font></b></td>
						<td width="115" align="center"><b>
						<font face="Arial" size="2">May 15 - May 27</font></b></td>
						<td align="center" width="118"><b>
						<font face="Arial" size="2">May 28 - Jun 24</font></b></td>
						<td align="center" width="122"><b>
						<font face="Arial" size="2">Jun 25 - Sep 5</font></b></td>
						<td align="center" width="161"><b>
						<font face="Arial" size="2">Sep 6 - season end</font></b></td>
					</tr>
				</table>
				</td>
    </tr>
    <tr>
      <td colspan="2" align="center">
				<table border="0" width="100%" cellspacing="0" id="table2">
					<tr>
          <td align="center" width="51">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="54"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="57">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="55"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="53">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="55"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="57">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">&nbsp;Nights</span></font></td>
          				<td align="center" width="59"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="62">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="58"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="94">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="70"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
					</tr>
					<tr>
          <td align="center" width="51">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="54">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$109</font></a></td>
          <td align="center" width="57">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="55">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5"> <font face="Arial" size="2">$140</font></a></td>
          <td align="center" width="53">
			<font face="Arial" style="font-size: 9pt">1 
            </font></td>
          				<td align="center" width="55">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$148</font></a></td>
          <td align="center" width="57">
			<font face="Arial" style="font-size: 9pt">1 
            </font></td>
          				<td align="center">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5"> <font face="Arial" size="2">$199</font></a></td>
          <td align="center" width="62">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="58">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$235</font></a></td>
          <td align="center" width="94">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="70"><a href="https://www.bundubashers.com/reserve_lodging.php?type=5"> <font face="Arial" size="2">$195</font></a></td>
					</tr>
					<tr>
          <td align="center" width="51">
			<font face="Arial" style="font-size: 9pt">2 
            to 6 </font></td>
          				<td align="center" width="54">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$58</font></a></td>
          <td align="center" width="57">
			<font face="Arial" style="font-size: 9pt">2 
            to 6</font></td>
          				<td align="center" width="55">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$74</font></a></td>
          <td align="center" width="53">
			<font face="Arial" style="font-size: 9pt">2 
            to 6 </font></td>
          				<td align="center" width="55">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$95</font></a></td>
          <td align="center" width="57">
			<font face="Arial" style="font-size: 9pt">2 
            t&amp; up</font></td>
          				<td align="center">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5"> <font face="Arial" size="2">$169</font></a></td>
          <td align="center" width="62">
			<font face="Arial" style="font-size: 9pt">2 &amp; up</font></td>
          				<td align="center" width="58">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$199</font></a></td>
          <td align="center" width="94">
			<font face="Arial" style="font-size: 9pt">2 &amp; up</font></td>
          				<td align="center" width="70">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$155</font></a></td>
					</tr>
					<tr>
          <td align="center" width="51">
			<font face="Arial" style="font-size: 9pt">7 
            &amp; up</font></td>
          				<td align="center" width="54">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$54</font></a></td>
          <td align="center" width="57">
			<font face="Arial" style="font-size: 9pt">7 
            &amp; up</font></td>
          				<td align="center" width="55">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$69</font></a></td>
          <td align="center" width="53">
			<font face="Arial" style="font-size: 9pt">7 
            &amp; up</font></td>
          				<td align="center" width="55">
						<a href="https://www.bundubashers.com/reserve_lodging.php?type=5">
						<font face="Arial" size="2">$89</font></a></td>
					</tr>
				</table>
				</td>
    </tr>
    <tr>
      <td colspan="2" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><font face="Arial" size="2">For immediate attention, 
				please contact us at 406 646 1118, or 
				<a href="mailto:yellowstonelodging@gmail.com?subject=West Yellowstone lodging">email<span style="text-decoration: none"> </span> 
				</a>us.</font></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><font size="2" face="Arial">Different <a href="yellowstone-tours.htm">Yellowstone tour</a> options are offered from West Yellowstone. These include tours of both the upper and 
		lower loops of Yellowstone National Park, as well as Grand Teton National Park and Jackson Hole. </font></td>
    </tr>
    <tr>
      <td colspan="2">
		<p align="center"><font size="2" face="Arial"><a href="#TOP">TOP</a></font></td>
    </tr>
  </table>
</div>
<table width="851" border="0" cellpadding="0">
  <tr>
    <td width="847"><div align="center">
		<img border="0" src="lodging-in-yellowstone.png" width="1250" height="6940" alt="Yellowstone motel"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
