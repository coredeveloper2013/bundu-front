<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Yellowstone Lodging</title>
    <meta charset="UTF-8">
    <meta name="author" content="Yellowstone Lodging">
    <meta name="description" content="All of our West Yellowstone studios, apartments and homes are completely furnished with everything you need for a self catered vacation!">
    <meta name="keywords" content="Yellowstone Lodging">

    <meta property="og:title" content="Yellowstone Lodging"/>
    <meta property="og:url" content="https://yellowstonelodging.biz/"/>
    <meta property="og:description" content="All of our West Yellowstone studios, apartments and homes are completely furnished with everything you need for a self catered vacation!"/>
    <meta property="og:image" content="https://yellowstonelodging.biz/images/banner/banner2.jpeg"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css"
          integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous"/>
    <!--<link rel="stylesheet" href="css/framework7.bundle.min.css">-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body >

<div class="container no-padding">
    <div class="row">
        <div class="_banner text-center" style="width: 100%;background-image: url('./images/banner/banner1.jpeg');">
            <div class="_content">
                <h1 class="_title_heading"><a href="index.php">Yellowstone Lodging</a></h1>
                <a href="index.php" class="_rollBack"><i class="fas fa-arrow-left 3x"></i></a>
            </div>
        </div>

        <form action="searched_result.php" method="get" id="lodging_search">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="check_in">Check In Date <span
                                    class="text-danger"> *</span> </label>
                        <input class="form-control check_in" type="text" name="start_date"
                               placeholder="Check In Date"
                               value="<?php echo $_REQUEST['start_date'] ?>" readonly="readonly"
                               onchange="selectCheckout()" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="check_in">Check Out Date <span
                                    class="text-danger"> *</span></label>
                        <input class="form-control check_out" type="text" name="end_date"
                               placeholder="Check Out Date"
                               value="<?php echo $_REQUEST['end_date'] ?>" readonly="readonly" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="check_in">No. of Guests <span
                                    class="text-danger"> *</span></label>
                        <input class="form-control" type="number" name="adult" placeholder="No. of Adults" min="1"
                               value="<?php echo $_REQUEST['adult'] ?>" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="check_in">&nbsp;</label>
                        <button type="submit" class="btn btn-block btn-primary" onclick="validate()">Continue
                        </button>
                    </div>
                </div>
            </div>
        </form>

        <div class="col-md-12 text-primary" id="fetchingData" style="text-align: center;padding-bottom: 10px" >
            <div class="spin">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <div id="main_wrapper">
            <div class="list media-list" id="" v-if="Results.length > 0" >
                <div class="row">
                    <div class="EachResult" v-for="(result , index) in Results" id="search_value">
                        <a :href="(result.url != '') ? result.url :'javascript:void(0)' " class="_logo" :style="{'background-image' : result.image == 0 ? 'url(images/not-found/not-found.png)' : 'url('+result.image+')'}"></a>
                        <div class="_details">
                            <div class="_title">
                                <strong class="title"><a :href="(result.url != '') ? result.url :'javascript:void(0)' ">{{ result.name }}</a></strong> <br>
                                    <small>{{ result.description }}</small>
                                <br>
                                <small class="text-danger"
                                       v-if="(searchParam.adult) > (result.capacity * (result.available-result.rooms)) && result.price != 0 && (result.available-result.rooms) != 0 ">
                                    <b>For
                                        {{searchParam.adult}} <span v-if="searchParam.adult>1">guests</span><span
                                                v-else="">guest</span>
                                        this room is not available </b></small>
                                <small class="text-success"
                                       v-if="searchParam.adult > result.capacity && searchParam.adult <= (result.capacity * (result.available-result.rooms)) && result.price != 0">
                                    <b v-if="(result.capacity * (result.available-result.rooms))/searchParam.adult != 1">For
                                        {{searchParam.adult}} <span v-if="searchParam.adult>1">guests</span><span
                                                v-else="">guest</span> you will need minimum
                                        {{Math.ceil(searchParam.adult/result.capacity)}} rooms</b>
                                    <b v-else="">For {{searchParam.adult}} <span
                                                v-if="searchParam.adult>1">guests</span><span
                                                v-else="">guest</span> you will need minimum {{result.available-result.rooms}} rooms</b>
                                </small>
                            </div>
                            <div class="_status" >
                                <small>
                                    <!--                                    <b v-if="result.available != 0">Available Room : {{(result.available-result.rooms)}}</b><br>-->
                                    <!--                                    <b v-if="result.available == 0">Available Room : {{(result.available)}}</b>-->
                                    <b>Guests : {{result.capacity}} </b> per {{ result.name.split(":")[0].toLowerCase() }}<br>
                                    <h6 v-if="result.available-result.rooms == 0 || (searchParam.adult) > (result.capacity * (result.available-result.rooms)) || result.price == 0 "><span  class="badge badge-warning">Not Available</span></h6>
                                    <h6 v-else=""><span class="badge badge-success">Available</span></h6>
                                </small>
                            </div>
                            <div class="_price">
                                <button v-if="(result.available-result.rooms) != 0 && result.price != 0" type="button"
                                        class="btn btn-sm btn-default custom_label">
                                    <strong>${{result.price}}</strong>
                                    <small> per night</small>
                                </button>
                                <button v-else="" type="button" class="btn btn-sm btn-default custom_label">No price
                                    available</small></button>
                            </div>
                        </div>
                        <div class="_submission">
                            <div class="vertical3">
                                <form :id="'reserve_form'+index"
                                      action="reserve_lodgingNew.php" method="post">
                                    <div v-if="result.price != 0 && (searchParam.adult) <= (result.capacity * (result.available-result.rooms))" >
                                        <input type="hidden" name="type" :value="result.id">
                                        <input type="hidden" name="type_name" :value="result.name">
                                        <input type="hidden" name="start_date" value="<?php echo $_REQUEST['start_date'] ?>">
                                        <input type="hidden" name="end_date" value="<?php echo $_REQUEST['end_date'] ?>">
                                        <input type="hidden" name="start_year" :value="dateSlugs.start_year">
                                        <!--                                    <input type="hidden" name="total_room" v-model="searchParam.total_room">-->
                                        <input type="hidden" name="start_month" :value="dateSlugs.start_month">
                                        <input type="hidden" name="start_day" :value="dateSlugs.start_day">
                                        <input type="hidden" name="nights" :value="result.days">
                                        <input type="hidden" name="capacity" :value="result.capacity">
                                        <input type="hidden" name="price" :value="result.price">
                                        <input type="hidden" name="guests" :value="<?php echo $_REQUEST['adult'] ?>">
                                    </div>

                                    <div class="form-group _rooms">
                                        <label for="sel1" style="padding-right: 10px">Rooms  </label>
                                        <select class="form-control" :id="'total_room'+index" name="total_room" >
                                            <option>0</option>
                                            <option v-if="result.price != 0 && (searchParam.adult) <= (result.capacity * (result.available-result.rooms))" v-for="item in (result.available-result.rooms)">{{item}}</option>
                                        </select>
                                    </div>
                                    <div class="_rooms">
                                        <button v-if="result.price != 0 && (searchParam.adult) <= (result.capacity * (result.available-result.rooms))" type="submit" class="btn btn-primary custom_label form-inline"
                                                v-on:click="event.preventDefault();submitForm(index)">Reserve
                                        </button>
                                        <button v-else type="submit" class="btn btn-primary custom_label form-inline" disabled>Reserve
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/vue.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js"
        integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>
<script>


    function selectCheckout() {
        $(".check_out").prop('disabled', false);
        if ($(".check_in")) {
            var tomorrow = new Date($(".check_in").val());
            tomorrow.setDate(tomorrow.getDate() + 1);
            $(".check_out").flatpickr({
                allowInput: true,
                disableMobile: true,
                defaultDate: tomorrow,
                minDate: tomorrow, // 1 days from check_in
                altInput: true,
                altFormat: 'd F, Y'
            });
        }
    }

    new Vue({
        el: '#main_wrapper',
        data: {
            Results: [],
            error: null,
            dateSlugs: {
                start_year: '<?php echo date('Y', strtotime($_REQUEST['start_date'])) ?>',
                start_month: '<?php echo date('m', strtotime($_REQUEST['start_date'])) ?>',
                start_day: '<?php echo date('d', strtotime($_REQUEST['start_date'])) ?>',
            },
            searchParam: {
                start_date: '<?php echo $_REQUEST['start_date'] ?>',
                end_date: '<?php echo $_REQUEST['end_date'] ?>',
                adult: '<?php echo $_REQUEST['adult'] ?>',
                total_room: 0
            }
        },
        methods: {
            getSearchResult() {
                let THIS = this;
                $.ajax({
                    type: 'POST',
                    url: 'https://localhost/bundu-admin/api/NewApiLodging_price.php',
                    data: THIS.searchParam,
                    success: function (res) {
                        res = JSON.parse(res);
                        console.log(res);
                        if (res.status === 2000) {
                            THIS.Results = res.data;
                            setTimeout(function () {
                                $("#fetchingData").hide();
                            }, 1000)

                        } else {
                            // error message
                            THIS.error = res.error;
                        }
                    }
                });
            },
            submitForm(index) {
                var _this = this;
                var rooms = Math.ceil(_this.searchParam.adult / _this.Results[index].capacity);

                if ($('#total_room'+index).val() >= rooms && $('#total_room'+index).val() <= (_this.Results[index].available - _this.Results[index].rooms)) {

                    document.getElementById('reserve_form'+index).submit();

                } else if ($('#total_room'+index).val() > rooms) {
                    var msg = "You can select " + (_this.Results[index].available - _this.Results[index].rooms) + " rooms maximum!!";
                    swal("Sorry!!!!", msg, "warning", {
                        button: "Try Again",
                    })

                } else {
                    var msg = "You need to select " + rooms + " rooms minimum!!";
                    swal("Sorry!!!!", msg, "warning", {
                        button: "Try Again",
                    })
                }

            }
        },
        mounted() {

            this.getSearchResult();
            console.log(this.dateSlugs);
            setTimeout(function () {
                $('#main_wrapper').fadeIn()
            }, 1000);
            $(".check_in").flatpickr({
                allowInput: true,
                disableMobile: true,
                defaultDate: '<?php echo $_REQUEST['start_date'] ?>',
                minDate: '<?php echo date('Y-m-d') ?>',
                altInput: true,
                altFormat: 'd M, Y'
            });
            $(".check_out").flatpickr({
                allowInput: true,
                disableMobile: true,
                defaultDate: '<?php echo $_REQUEST['end_date'] ?>',
                minDate: new Date($(".check_in").val()).fp_incr(1), // 1 days from check_in
                altInput: true,
                altFormat: 'd M, Y'
            });

        }
    });

    // -- validate formdata --
    function validate() {
        $('.text-danger').remove();
        if ($('input[name=start_date]').val() == '') {
            _this = $('input[name=start_date]');
            _this.closest('.form-group').append('<span class="text-danger alert_start_date">Please select a check in date!</span>');
        } else {
            _this = $('input[name=start_date]');
            _this.closest('.form-group').find('.alert_start_date').remove();
        }
        if ($('input[name=end_date]').val() == '') {
            _this = $('input[name=end_date]');
            _this.closest('.form-group').append('<span class="text-danger alert_end_date">Please select a check out date!</span>');
        } else {
            _this = $('input[name=end_date]');
            _this.closest('.form-group').find('.alert_end_date').remove();
        }

        if ($('input[name=adult]').val() == '') {
            _this = $('input[name=adult]');
            _this.addClass('is-invalid');
            _this.closest('.form-group').append('<span class="text-danger alert_adult">Please enter number of adult!</span>');
        } else {
            _this = $('input[name=adult]');
            _this.removeClass('is-invalid');
            _this.closest('.form-group').find('.alert_adult').remove();
        }
        if ($('input[name=total_room]').val() == '') {
            _this = $('input[name=total_room]');
            _this.addClass('is-invalid');
            _this.closest('.form-group').append('<span class="text-danger alert_adult">Please enter number of rooms!</span>');
        } else {
            _this = $('input[name=total_room]');
            _this.removeClass('is-invalid');
            _this.closest('.form-group').find('.alert_adult').remove();
        }


    }
</script>
</body>
</html>
