<?php
include_once("functions.php");

$url_part = '';
if (isset($_REQUEST['start_month']) && isset($_REQUEST['start_day']) && isset($_REQUEST['start_year'])) {
    $resultJson = api_call('https://bundubashers.com/api/lodging_price.php?start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights']);
    $resultArry = json_decode($resultJson, true);
    if (!empty($resultArry['success'][23])) {
        $eachLodgingArr = $resultArry['success'][23];
    }
    $url_part = '&start_month='.$_REQUEST['start_month'].'&start_day='.$_REQUEST['start_day'].'&start_year='.$_REQUEST['start_year'].'&nights='.$_REQUEST['nights'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Large West Yellowstone apartment</title>
<style type="text/css">
#apDiv1 {
	position:absolute;
	left:73px;
	top:52px;
	width:199px;
	height:128px;
	z-index:1;
	color: #FFF;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
#apDiv2 {
	position:absolute;
	left:63px;
	top:303px;
	width:753px;
	height:732px;
	z-index:2;
}
</style>
<meta name="description" content="Large apartment in Yellowstone">
</head>

<body>
<div id="apDiv3" style="position: absolute; left: 844px; top: 108px; height: 114px; width:407px">
	<font face="Arial"><font color="#FFFFFF">
	<a href="index.htm" name="TOP0">
	<font color="#FFFFFF">HOME</font></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
					<u><br />
  	</u>
  	<font color="#FFFFFF">
	<a href="yellowstone-accommodation.htm"><font color="#FFFFFF">
	Studios</font></a></font><u><br>
	</u>
	<font color="#FFFFFF">
	<span style="text-decoration: none"><a href="yellowstone-apartment.htm">
	<font color="#FFFFFF">One Bedroom 
	Apartments</font></a><a href="yellowstone-apartments.htm"><br>
	<font color="#FFFFFF">Two Bedroom Apartments
	</font></a>
	</span></font><u><br />
	</u>
<a href="yellowstone_apartment.htm"><font color="#FFFFFF">
	Contact</font></a></font></div>
<div id="apDiv2" style="position: absolute; left: 94px; top: 358px; width:1085px">
  <table width="1084" border="0" cellpadding="0">
    <tr>
      <td width="18">
				<img border="0" src="Yellowstone_lodging.png" width="12" height="12" align="left"></td>
      <td width="1060"><strong><font face="Arial" style="font-size: 11pt">
		Yellowstone Two Bedroom Apartment</font><font size="2" face="Arial">&nbsp; </font>
		</strong>
		<font face="Arial" style="font-size: 9pt"><em>Also available: <a href="yellowstone-apartment.htm">one bedroom apartments</a>, <a href="yellowstone-accommodation.htm">studios</a></em></font></td>
    </tr>
    <tr>
      <td colspan="2"><font size="2" face="Arial">This Yellowstone lodging is only a mile from the main entrance to Yellowstone National Park. 
		It is a two bedroom townhouse, inside a log cabin. More details can be 
		seen <a href="#Below">below</a>.&nbsp; You can also
		<a href="#Nightly_Rates">look at the prices</a> and then
		<b>
		<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
		<font color="#FF0000">order here</font></a></b>.&nbsp; </font></td>
    </tr>
    <tr>
      <td colspan="2"><font size="2" face="Arial"><a name="Below">The downstairs
		</a>bedroom has two double beds, while the upstairs bedroom has two 
		queen beds. The upper level is accessed via a spiral staircase. Free 
		WiFi is available throughout the property, and cable TV in the living 
		room area.</font><p><font size="2" face="Arial">There is a full kitchen, 
		and the townhouse comes complete with all the knives, forks, linens, towels, crockery and cutlery that you may need. The maximum number of people permitted is six. There is one full bathroom, with one hot water heater, and if there are six people in the apartment, it is likely that some will have to shower in the morning and some in the evening. </font></td>
    </tr>
    <tr>
      <td colspan="2">
		<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="2">
					<tr>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-2.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-1.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
					</tr>
					<tr>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-3.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-13.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
					</tr>
					<tr>
						<td align="center" valign="top">
						<img border="1" src="yellowstone-two-bedroom-apartment-4.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-5.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
					</tr>
					<tr>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-6.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-7.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
					</tr>
					<tr>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-9.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-10.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
					</tr>
					<tr>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-11.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-12.jpg" width="530" height="398" alt="West Yellowstone house for rent"></td>
					</tr>
					<tr>
						<td align="center">
						<img border="1" src="yellowstone-two-bedroom-apartment-8.jpg" width="530" height="707" alt="West Yellowstone house for rent"></td>
						<td align="center">
						&nbsp;</td>
					</tr>
					<tr>
						<td align="center" colspan="2">
						&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
			</table>
		</td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">
		<p align="center"><font size="2" face="Arial"><a href="#TOP">TOP</a></font></td>
    </tr>
    <tr>
      <td colspan="2">
          <p><font size="2" face="Arial">
                      <?php
                        if (!empty($eachLodgingArr['price_arr'])) {
                            echo '<span style="font-weight: bold;">Price: $';
                            echo array_sum($eachLodgingArr['price_arr']) / count($eachLodgingArr['price_arr']);
                            echo '</span>&nbsp;&nbsp;&nbsp;';
                        }
                        ?>
                        Please <a
                    href="https://www.bundubashers.com/reserve_lodging_new.php?type=23<?php echo $url_part;?>">order
                    here and see the prices</a>.&nbsp; <br>
                </font></p>
          
          <font face="Arial" size="2"><a name="Pricing">Please 
		<font color="#FF0000"><b>order </b></font></a><b><font color="#FF0000">these 
						Yellowstone apartments </font> 
		<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
		<font color="#FF0000">here</font></a></b>, but 
						note our </font> 
		<font color="#FFFFFF" face="Arial" size="2"> <a target="_blank" href="yellowstone-lodging-cancellation-policy.htm"> cancellation, payment 
		and check in policies</a></font><span style="font-weight: 700; "><font face="Arial" size="2"> </font>
		</span>
		<font size="2" face="Arial">first!&nbsp;<span style="font-weight: 700"> 
		<font color="#FF0000">NO CANCELLATIONS ACCEPTED!&nbsp; </font> </span>Please be aware that your booking is not 
						confirmed until you receive a confirmation email from 
	  us.</font></td>
    </tr>
    <tr>
      <td colspan="2">
		<p align="center"><font face="Arial" size="2"><strong>
		<a name="Nightly_Rates">Nightly Rates</a></strong></font></td>
    </tr>
    <tr>
      <td colspan="2"><div align="center">
        <table border="0" width="100%" cellspacing="0">
          <tr>
            <td align="center">
				<table border="0" width="100%" cellpadding="0">
					<tr>
						<td align="center" width="106"><b>
						<font face="Arial" size="2">Nov 3 - Apr 15</font></b></td>
						<td align="center"><b><font face="Arial" size="2">Apr 16 
						- May 14</font></b></td>
						<td width="115" align="center"><b>
						<font face="Arial" size="2">May 15 - May 27</font></b></td>
						<td align="center" width="118"><b>
						<font face="Arial" size="2">May 28 - Jun 24</font></b></td>
						<td align="center" width="122"><b>
						<font face="Arial" size="2">Jun 25 - Sep 5</font></b></td>
						<td align="center" width="161"><b>
						<font face="Arial" size="2">Sep 6 - season end</font></b></td>
					</tr>
				</table>
				</td>
          </tr>
          <tr>
            <td align="center">
				<table border="0" width="100%" cellspacing="0" id="table2">
					<tr>
          <td align="center" width="49">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="50"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="60">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="58"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="56">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="56"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="59">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">&nbsp;Nights</span></font></td>
          				<td align="center" width="78"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="42">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="62"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
          <td align="center" width="56">
			<font face="Arial"><span style="font-size: 9pt; font-style: italic">
			Nights</span></font></td>
          				<td align="center" width="102"><font face="Arial">
						<span style="font-size: 9pt; font-style: italic">Price</span></font></td>
					</tr>
					<tr>
          <td align="center" width="49">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="50">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$119</font></a></td>
          <td align="center" width="60">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="58">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$149</font></a></td>
          <td align="center" width="56">
			<font face="Arial" style="font-size: 9pt">1 
            </font></td>
          				<td align="center" width="56">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$179</font></a></td>
          <td align="center" width="59">
			<font face="Arial" style="font-size: 9pt">1 
            </font></td>
          				<td align="center">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$240</font></a></td>
          <td align="center" width="42">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="62">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$279</font></a></td>
          <td align="center" width="56">
			<font face="Arial" style="font-size: 9pt">1</font></td>
          				<td align="center" width="102">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$249</font></a></td>
					</tr>
					<tr>
          <td align="center" width="49">
			<font face="Arial" style="font-size: 9pt">2 
            to 6 </font></td>
          				<td align="center" width="50">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$89</font></a></td>
          <td align="center" width="60">
			<font face="Arial" style="font-size: 9pt">2 
            to 6</font></td>
          				<td align="center" width="58">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$109</font></a></td>
          <td align="center" width="56">
			<font face="Arial" style="font-size: 9pt">2 
            to 6 </font></td>
          				<td align="center" width="56">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$159</font></a></td>
          <td align="center" width="59">
			<font face="Arial" style="font-size: 9pt">2 
            &amp; up</font></td>
          				<td align="center">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$209</font></a></td>
          <td align="center" width="42">
			<font face="Arial" style="font-size: 9pt">2 
            &amp; up</font></td>
          				<td align="center" width="62">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$249</font></a></td>
          <td align="center" width="56">
			<font face="Arial" style="font-size: 9pt">2 
            &amp; up</font></td>
          				<td align="center" width="102">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$219</font></a></td>
					</tr>
					<tr>
          <td align="center" width="49">
			<font face="Arial" style="font-size: 9pt">7 
            &amp; up</font></td>
          				<td align="center" width="50">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$85</font></a></td>
          <td align="center" width="60">
			<font face="Arial" style="font-size: 9pt">7 
            &amp; up</font></td>
          				<td align="center" width="58">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$104</font></a></td>
          <td align="center" width="56">
			<font face="Arial" style="font-size: 9pt">7 
            &amp; up</font></td>
          				<td align="center" width="56">
			<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			<font face="Arial" size="2">$149</font></a></td>
					</tr>
				</table>
				</td>
          </tr>
          <tr>
            <td align="center"><font size="2" face="Arial">Please
		<a href="https://www.bundubashers.com/reserve_lodging.php?type=23">
			order here</a>.</font></td>
          </tr>
          </table>
      </div></td>
    </tr>
    <tr>
      <td colspan="2"><font face="Verdana" size="2">For immediate attention, 
				please contact us at 406 646 1118, or 
				<a href="mailto:yellowstonelodging@gmail.com?subject=West Yellowstone lodging">email<span style="text-decoration: none"> </span> 
				</a>us.&nbsp; </font><font face="Arial" size="2">While you're in town, please consider one of our 
		<a target="_blank" href="http://www.yellowstonenationalparktours.com/">Yellowstone tours</a>. </font></td>
    </tr>
    <tr>
      <td colspan="2">
		<p align="center"><font size="2" face="Arial"><a href="#TOP">TOP</a></font></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
  </table>
</div>
<table width="851" border="0" cellpadding="0">
  <tr>
    <td width="847"><div align="center">
		<img border="0" src="lodging-in-yellowstone.png" width="1250" height="6940" alt="Yellowstone lodging"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</td>
  </tr>
</table>
</body>
</html>
