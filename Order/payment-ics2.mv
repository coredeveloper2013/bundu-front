<MvCOMMENT>
|
| Miva Order v1.x
| (c) 1997, 1998, 1999, 2000, 2001 Miva Corporation
|
| 2629 Ariane Drive
| San Diego, CA 92117
| 619-490-2570
| sales@miva.com
|
| $Source: /home/cvs/order/Order/payment-ics2.mv,v $
|
| CyberSource ICSv2 Payment Services
|
</MvCOMMENT>

<MvFUNCTION NAME = "Payment_Install" STANDARDOUTPUTLEVEL = "" ERROROUTPUTLEVEL = "syntax, expression">
	<MvASSIGN NAME = "l.ok" VALUE = 1>

	<MvASSIGN NAME = "l.directory" VALUE = "{ g.OrderPath $ 'ics2/' }">
	<MvIF EXPR = "{ NOT fexists( l.directory ) }">
		<MvIF EXPR = "{ NOT fmkdir( l.directory ) }">
			<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok"
				  VALUE = "{ Error( 'ORD-ICS-00001', 'Unable to create directory \'' $ encodeentities( l.directory ) $ '\'' ) }">
		</MvIF>
	</MvIF>

	<MvIF EXPR = "{ l.ok }">
		<MvCREATE NAME = "ICS2" 
				  DATABASE = "{ l.directory $ 'ics2.dbf' }"
				  FIELDS = "merchant	CHAR( 100 ),
							url			CHAR( 254 ),
							currency	CHAR( 3 ),
							store_cc	BOOL">
		<MvIF EXPR = "{ MvCREATE_Error }">
			<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00002', MvCREATE_Error ) }">
		<MvELSE>
			<MvASSIGN NAME = "ICS2.d.merchant" VALUE = "ICS2Test">
			<MvASSIGN NAME = "ICS2.d.url" VALUE = "http://ics2test.ic3.com">
			<MvASSIGN NAME = "ICS2.d.currency" VALUE = "USD">
			<MvASSIGN NAME = "ICS2.d.store_cc" VALUE = 0>

			<MvADD NAME = "ICS2">
		</MvIF>
	</MvIF>

	<MvIF EXPR = "{ l.ok }">
		<MvCREATE NAME = "ICS2Cards"
				  DATABASE = "{ l.directory $ 'ics2card.dbf' }"
				  FIELDS = "code		CHAR( 50 ),
						    name		CHAR( 100 ),
						    enabled		BOOL">
		<MvIF EXPR = "{ MvCREATE_Error }">
			<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00003', MvCREATE_Error ) }">
		</MvIF>
		
		<MvIF EXPR = "{ l.ok }">
			<MvMAKEINDEX NAME = "ICS2Cards"
						 INDEXFILE = "{ l.directory $ 'ics2card.mvx' }"
						 EXPRESSION = "{ ICS2Cards.d.code }"
						 FLAGS = "unique, ascending">
			<MvIF EXPR = "{ MvMAKEINDEX_Error }">
				<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00004', MvMAKEINDEX_Error ) }">
			</MvIF>
		</MvIF>

		<MvIF EXPR = "{ l.ok }">
			<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2Card_Insert( 'VISA', 'Visa', 1 ) }">
			<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2Card_Insert( 'MASTERCARD', 'MasterCard', 1 ) }">
			<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2Card_Insert( 'DISCOVER', 'Discover', 1 ) }">
			<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2Card_Insert( 'AMEX', 'American Express', 1 ) }">
			<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2Card_Insert( 'DINER', 'Diner\'s Club', 1 ) }">
		</MvIF>
	</MvIF>

	<MvIF EXPR = "{ l.ok }">
		<MvCREATE NAME = "ICS2Orders"
				  DATABASE = "{ l.directory $ 'ics2ordr.dbf' }"
				  FIELDS = "order_id	NUMBER,
							billed		BOOL,
							cc_fname	CHAR( 100 ),
							cc_lname	CHAR( 100 ),
							cc_number	CHAR( 25 ),
							cc_expmo	CHAR( 10 ),
							cc_expyr	CHAR( 10 ),
							auth_id		CHAR( 254 ),
							auth_amnt	NUMBER( 10.2 ),
							auth_avs	CHAR( 1 ),
							auth_code	CHAR( 6 ),
							auth_time	CHAR( 18 ),
							bill_amnt	NUMBER( 10.2 ),
							bill_time	CHAR( 18 ),
							bill_rmsg	CHAR( 254 )">
		<MvIF EXPR = "{ MvCREATE_Error }">
			<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00005', MvCREATE_Error ) }">
		</MvIF>

		<MvIF EXPR = "{ l.ok }">
			<MvMAKEINDEX NAME = "ICS2Orders"
						 INDEXFILE = "{ l.directory $ 'ics2ordr.mvx' }"
						 EXPRESSION = "{ ICS2Orders.d.order_id }"
						 FLAGS = "unique, ascending">
			<MvIF EXPR = "{ MvMAKEINDEX_Error }">
				<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00006', MvMAKEINDEX_Error ) }">
			</MvIF>
		</MvIF>
	</MvIF>

	<MvIF EXPR = "{ l.ok }">
		<MvCLOSE NAME = "ICS2">
		<MvCLOSE NAME = "ICS2Cards">
		<MvCLOSE NAME = "ICS2Orders">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Pack" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvPACK NAME = "ICS2">
		<MvPACK NAME = "ICS2Cards">
		<MvPACK NAME = "ICS2Orders">

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Uninstall" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.directory" VALUE = "{ g.OrderPath $ 'ics2/' }">

	<MvASSIGN NAME = "l.ok" VALUE = "{ fdelete( l.directory $ 'ics2.dbf' ) }">
	<MvASSIGN NAME = "l.ok" VALUE = "{ fdelete( l.directory $ 'ics2card.dbf' ) }">
	<MvASSIGN NAME = "l.ok" VALUE = "{ fdelete( l.directory $ 'ics2card.mvx' ) }">
	<MvASSIGN NAME = "l.ok" VALUE = "{ fdelete( l.directory $ 'ics2ordr.dbf' ) }">
	<MvASSIGN NAME = "l.ok" VALUE = "{ fdelete( l.directory $ 'ics2ordr.mvx' ) }">
	
	<MvASSIGN NAME = "l.ok" VALUE = "{ fdelete( l.directory ) }">

	<MvFUNCTIONRETURN VALUE = 1>
</MvFUNCTION>

<MvFUNCTION NAME = "Screen_Configure" STANDARDOUTPUTLEVEL = "text, html, compresswhitespace">
	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<HEAD><TITLE>Miva Order Administration: CyberSource ICSv2 Payment Services</TITLE></HEAD>

		<BODY BGCOLOR = "#ffffff">
		<MvIF EXPR = "{ len( g.Message ) }">
			<FONT FACE = "Arial, Helvetica" SIZE = "-1" COLOR = "#0000FF"><MvEVAL EXPR = "{ g.Message }"></FONT><BR><BR>
		</MvIF>

		<FONT FACE = "Arial, Helvetica"><B>CyberSource ICSv2 Payment Services</B><BR><BR>

		<TABLE BORDER = 1>
		<TR><TD BGCOLOR = "#dddddd">
			<TABLE BORDER = 0 CELLPADDING = 5 CELLSPACING = 0>

			<FORM METHOD = "post" ACTION = "&[ g.sessionurl ]">
			<INPUT TYPE = "hidden" NAME = "Action" VALUE = "UPNT">
			<INPUT TYPE = "hidden" NAME = "Screen" VALUE = "PMNT">

			<TR><TD ALIGN = "left" VALIGN = "middle" COLSPAN = 2>
				<FONT SIZE = "-1" FACE = "Arial, Helvetica">
				<B>URL to CyberSource ICSv2 Gateway:</B><BR>
				&nbsp;&nbsp;<INPUT TYPE = "text" NAME = "ICS2_URL" SIZE = 40 VALUE = "&[ ICS2.d.url:entities ]">
				</FONT>
			</TD></TR>

			<TR><TD ALIGN = "left" VALIGN = "middle" COLSPAN = 2>
				<FONT SIZE = "-1" FACE = "Arial, Helvetica">
				<B>Merchant ID:</B><BR>
				&nbsp;&nbsp;<INPUT TYPE = "text" NAME = "ICS2_Merchant" SIZE = 40 VALUE = "&[ ICS2.d.merchant:entities ]">
				</FONT>
			</TD></TR>

			<TR><TD ALIGN = "left" VALIGN = "middle" COLSPAN = 2>
				<FONT SIZE = "-1" FACE = "Arial, Helvetica">
				<B>Currency:</B><BR>
				&nbsp;&nbsp;<INPUT TYPE = "text" NAME = "ICS2_Currency" SIZE = 40 VALUE = "&[ ICS2.d.currency:entities ]">
				</FONT>
			</TD></TR>

			<TR><TD ALIGN = "left" VALIGN = "middle" COLSPAN = 2>
				<FONT SIZE = "-1" FACE = "Arial, Helvetica">
				<MvIF EXPR = "{ ICS2.d.store_cc }">
					<INPUT TYPE = "checkbox" NAME = "ICS2_Store_CC" VALUE = "Yes" CHECKED>Store Entire Credit Card Number
				<MvELSE>
					<INPUT TYPE = "checkbox" NAME = "ICS2_Store_CC" VALUE = "Yes">Store Entire Credit Card Number
				</MvIF>
				</FONT>
			</TD></TR>

			<TR><TD COLSPAN = 2>
				<HR>
			</TD></TR>

			<TR><TD COLSPAN = 2>
				<FONT SIZE = "-1" FACE = "Arial, Helvetica">
				<B>Process the Following Types of Credit Cards:</B>

				<TABLE BORDER = 0>
					<MvASSIGN NAME = "l.col" VALUE = 1>
					<MvGO NAME = "ICS2Cards" ROW = "top">
					
					<TR>
					<MvWHILE EXPR = "{ NOT ICS2Cards.d.EOF }">
						<MvIF EXPR = "{ l.col GT 2 }">
							<MvASSIGN NAME = "l.col" VALUE = 1>
							</TR><TR>
						</MvIF>

						<TD ALIGN = "left" VALIGN = "middle" NOWRAP>
							<FONT SIZE = "-1" FACE = "Arial, Helvetica">
							<MvIF EXPR = "{ ICS2Cards.d.enabled }">
								<INPUT TYPE = "checkbox" NAME = "ICS2_&[ ICS2Cards.d.code:entities ]" VALUE = "Yes" CHECKED>
							<MvELSE>
								<INPUT TYPE = "checkbox" NAME = "ICS2_&[ ICS2Cards.d.code:entities ]" VALUE = "Yes">
							</MvIF>

							<MvEVAL EXPR = "{ encodeentities( ICS2Cards.d.name ) }">
							</FONT>
						</TD>

						<MvASSIGN NAME = "l.col" VALUE = "{ l.col + 1 }">
						<MvSKIP NAME = "ICS2Cards" ROWS = 1>
					</MvWHILE>

					<MvIF EXPR = "{ l.col LE 2 }">
						</TR>
					</MvIF>
				</TABLE>
				</FONT>
			</TD></TR>		

			<TR><TD COLSPAN = 2>
				<HR>
			</TD></TR>

			<TR>
			<TD ALIGN = "left" VALIGN = "bottom">
				<FONT FACE = "Arial, Helvetica" SIZE = "-1">
				<INPUT TYPE = "submit" VALUE = "Update">
				</FONT>
			</TD>
			</FORM>
			</TABLE>
		</TD></TR>
		</TABLE>
		</FORM>

		</BODY>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Action_Configure" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvASSIGN NAME = "ICS2.d.url" VALUE = "{ g.ICS2_URL }">
		<MvASSIGN NAME = "ICS2.d.merchant" VALUE = "{ g.ICS2_Merchant }">
		<MvASSIGN NAME = "ICS2.d.currency" VALUE = "{ g.ICS2_Currency }">

		<MvIF EXPR = "{ g.ICS2_Store_CC }">
			<MvASSIGN NAME = "ICS2.d.store_cc" VALUE = 1>
		<MvELSE>
			<MvASSIGN NAME = "ICS2.d.store_cc" VALUE = 0>
		</MvIF>

		<MvUPDATE NAME = "ICS2">

		<MvGO NAME = "ICS2Cards" ROW = "top">
		<MvWHILE EXPR = "{ NOT ICS2Cards.d.EOF }">
			<MvIF EXPR = "{ g.ICS2_&[ ICS2Cards.d.code:entities ] }">
				<MvASSIGN NAME = "ICS2Cards.d.enabled" VALUE = 1>
			<MvELSE>
				<MvASSIGN NAME = "ICS2Cards.d.enabled" VALUE = 0>
			</MvIF>

			<MvUPDATE NAME = "ICS2Cards">
			<MvSKIP NAME = "ICS2Cards" ROWS = 1>
		</MvWHILE>

		<MvIF EXPR = "{ l.ok }">
			<MvASSIGN NAME = "g.Message" VALUE = "ICSv2 settings updated">
		</MvIF>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Select" STANDARDOUTPUTLEVEL = "text, html, compresswhitespace">
	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvGO NAME = "ICS2Cards" ROW = "top">
		<MvWHILE EXPR = "{ NOT ICS2Cards.d.EOF }">
			<MvIF EXPR = "{ ICS2Cards.d.enabled }">
				<OPTION VALUE = "&[ ICS2Cards.d.code:entities ]"><MvEVAL EXPR = "{ encodeentities( ICS2Cards.d.name ) }"></OPTION>
			</MvIF>

			<MvSKIP NAME = "ICS2Cards" ROWS = 1>
		</MvWHILE>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_URL" STANDARDOUTPUTLEVEL = "">
	<MvFUNCTIONRETURN VALUE = "">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Description" PARAMETERS = "data" STANDARDOUTPUTLEVEL = "">
	<MvFUNCTIONRETURN VALUE = "Credit Card">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Message" PARAMETERS = "data" STANDARDOUTPUTLEVEL = "">
	<MvFUNCTIONRETURN VALUE = "">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Hide_Additional_Fields" PARAMETERS = "data" STANDARDOUTPUTLEVEL = "">
	<MvFUNCTIONRETURN VALUE = 1>
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Count" PARAMETERS = "data" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.count" VALUE = 4>
	<MvIF EXPR = "{ g.ICS2_Error_Message }">
		<MvASSIGN NAME = "l.count" VALUE = 5>
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.count }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Prompt" PARAMETERS = "data, field_id" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.prompt" VALUE = "">

	<MvIF EXPR = "{ l.field_id EQ 1 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "First Name:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 2 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Last Name:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 3 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Card Number:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 4 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Expiration Date:">
	</MvIF>

	<MvIF EXPR = "{ g.ICS2_Error_Message AND ( l.field_id EQ 5 ) }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Error:">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.prompt }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Invalid" PARAMETERS = "data, field_id" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.invalid" VALUE = 0>

	<MvIF EXPR = "{ ( l.field_id EQ 1 ) AND g.ICS2_CardFirstName_Invalid }">
		<MvASSIGN NAME = "l.invalid" VALUE = 1>
	</MvIF>

	<MvIF EXPR = "{ ( l.field_id EQ 2 ) AND g.ICS2_CardLastName_Invalid }">
		<MvASSIGN NAME = "l.invalid" VALUE = 1>
	</MvIF>

	<MvIF EXPR = "{ ( l.field_id EQ 3 ) AND g.ICS2_CardNumber_Invalid }">
		<MvASSIGN NAME = "l.invalid" VALUE = 1>
	</MvIF>

	<MvIF EXPR = "{ ( l.field_id EQ 4 ) AND g.ICS2_CardExp_Invalid }">
		<MvASSIGN NAME = "l.invalid" VALUE = 1>
	</MvIF>

	<MvIF EXPR = "{ g.ICS2_Error_Message AND ( l.field_id EQ 5 ) }">
		<MvASSIGN NAME = "l.invalid" VALUE = 1>
	</MvIF>
	
	<MvFUNCTIONRETURN VALUE = "{ l.invalid }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Field" PARAMETERS = "data, field_id" STANDARDOUTPUTLEVEL = "text, html, compresswhitespace">
	<MvASSIGN NAME = "l.ok" VALUE = 1>

	<MvIF EXPR = "{ l.field_id EQ 1 }">
		<INPUT TYPE = "text" NAME = "ICS2_CardFirstName" SIZE = 40 VALUE = "&[ g.ICS2_CardFirstName:entities ]">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 2 }">
		<INPUT TYPE = "text" NAME = "ICS2_CardLastName" SIZE = 40 VALUE = "&[ g.ICS2_CardLastName:entities ]">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 3 }">
		<INPUT TYPE = "text" NAME = "ICS2_CardNumber" SIZE = 20 VALUE = "&[ g.ICS2_CardNumber:entities ]">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 4 }">
		<SELECT NAME = "ICS2_CardExp_Month">
		<OPTION VALUE = "">&lt;Select One&gt;</OPTION>

		<MvASSIGN NAME = "l.month" VALUE = 1>
		<MvWHILE EXPR = "{ l.month LE 12 }">
			<MvIF EXPR = "{ g.ICS2_CardExp_Month EQ l.month }">
				<OPTION VALUE = "&[ l.month ]" SELECTED><MvEVAL EXPR = "{ l.month }"></OPTION>
			<MvELSE>
				<OPTION VALUE = "&[ l.month ]"><MvEVAL EXPR = "{ l.month }"></OPTION>
			</MvIF>

			<MvASSIGN NAME = "l.month" VALUE = "{ l.month + 1 }">
		</MvWHILE>

		</SELECT>
		
		/

		<MvASSIGN NAME = "l.year" VALUE = "{ s.dyn_tm_year }">
		<MvASSIGN NAME = "l.count" VALUE = 1>

		<SELECT NAME = "ICS2_CardExp_Year">
		<OPTION VALUE = "">&lt;Select One&gt;</OPTION>

		<MvWHILE EXPR = "{ l.count LE 10 }">
			<MvIF EXPR = "{ g.ICS2_CardExp_Year EQ l.year }">
				<OPTION VALUE = "&[ l.year ]" SELECTED><MvEVAL EXPR = "{ l.year }"></OPTION>
			<MvELSE>
				<OPTION VALUE = "&[ l.year ]"><MvEVAL EXPR = "{ l.year }"></OPTION>
			</MvIF>
		
			<MvASSIGN NAME = "l.year" VALUE = "{ l.year + 1 }">
			<MvASSIGN NAME = "l.count" VALUE = "{ l.count + 1 }">
		</MvWHILE>

		</SELECT>
	</MvIF>

	<MvIF EXPR = "{ g.ICS2_Error_Message AND ( l.field_id EQ 5 ) }">
		<MvEVAL EXPR = "{ g.ICS2_Error_Message }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Validate" PARAMETERS = "pay_data" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.valid" VALUE = 1>

	<MvASSIGN NAME = "g.ICS2_CardFirstName" VALUE = "{ ltrim( rtrim( g.ICS2_CardFirstName ) ) }">
	<MvIF EXPR = "{ len( g.ICS2_CardFirstName ) EQ 0 }">
		<MvASSIGN NAME = "l.valid" VALUE = 0>
		<MvASSIGN NAME = "g.ICS2_CardFirstName_Invalid" VALUE = 1>
	</MvIF>

	<MvASSIGN NAME = "g.ICS2_CardLastName" VALUE = "{ ltrim( rtrim( g.ICS2_CardLastName ) ) }">
	<MvIF EXPR = "{ len( g.ICS2_CardLastName ) EQ 0 }">
		<MvASSIGN NAME = "l.valid" VALUE = 0>
		<MvASSIGN NAME = "g.ICS2_CardLastName_Invalid" VALUE = 1>
	</MvIF>

	<MvASSIGN NAME = "g.ICS2_CardNumber" VALUE = "{ glosub( glosub( ltrim( rtrim( g.ICS2_CardNumber ) ), '-', '' ), ' ', '' ) }">
	<MvIF EXPR = "{ len( g.ICS2_CardNumber ) EQ 0 }">
		<MvASSIGN NAME = "l.valid" VALUE = 0>
		<MvASSIGN NAME = "g.ICS2_CardNumber_Invalid" VALUE = 1>
	</MvIF>

	<MvIF EXPR = "{ ( g.ICS2_CardExp_Year LT s.dyn_tm_year ) OR 
					( ( g.ICS2_CardExp_Year EQ s.dyn_tm_year ) AND 
					  ( g.ICS2_CardExp_Month LT s.dyn_tm_mon ) ) }">
		<MvASSIGN NAME = "l.valid" VALUE = 0>
		<MvASSIGN NAME = "g.ICS2_CardExp_Invalid" VALUE = 1>
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.valid }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Report_Description" STANDARDOUTPUTLEVEL = "">
	<MvFUNCTIONRETURN VALUE = "Credit Card">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Report_Count" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.count" VALUE = 0>

	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvFIND NAME = "ICS2Orders" VALUE = "{ Orders.d.id }" EXACT>
		<MvIF EXPR = "{ NOT ICS2Orders.d.EOF }">
			<MvASSIGN NAME = "g.ICS2Order_Billed" VALUE = "{ ICS2Orders.d.billed }">
			<MvASSIGN NAME = "g.ICS2Order_CC_FirstName" VALUE = "{ ICS2Orders.d.cc_fname }">
			<MvASSIGN NAME = "g.ICS2Order_CC_LastName" VALUE = "{ ICS2Orders.d.cc_lname }">
			<MvASSIGN NAME = "g.ICS2Order_CC_Number" VALUE = "{ ICS2Orders.d.cc_number }">
			<MvASSIGN NAME = "g.ICS2Order_CC_ExpMonth" VALUE = "{ ICS2Orders.d.cc_expmo }">
			<MvASSIGN NAME = "g.ICS2Order_CC_ExpYear" VALUE = "{ ICS2Orders.d.cc_expyr }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Request_ID" VALUE = "{ ICS2Orders.d.auth_id }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Amount" VALUE = "{ ICS2Orders.d.auth_amnt }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_AVS" VALUE = "{ ICS2Orders.d.auth_avs }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Code" VALUE = "{ ICS2Orders.d.auth_code }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Time" VALUE = "{ ICS2Orders.d.auth_time }">
			<MvASSIGN NAME = "g.ICS2Order_Bill_Amount" VALUE = "{ ICS2Orders.d.bill_amnt }">
			<MvASSIGN NAME = "g.ICS2Order_Bill_Time" VALUE = "{ ICS2Orders.d.bill_time }">
			<MvASSIGN NAME = "g.ICS2Order_Bill_Message" VALUE = "{ ICS2Orders.d.bill_rmsg }">

			<MvASSIGN NAME = "l.count" VALUE = 8>

			<MvIF EXPR = "{ g.ICS2Order_Billed }">
				<MvASSIGN NAME = "l.count" VALUE = "{ l.count + 2 }">
			</MvIF>

			<MvIF EXPR = "{ len( g.ICS2Order_Bill_Message ) }">
				<MvASSIGN NAME = "l.count" VALUE = "{ l.count + 1 }">
			</MvIF>
		</MvIF>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.count }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Report_Label" PARAMETERS = "field_id" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.label" VALUE = "">

	<MvIF EXPR = "{ l.field_id EQ 1 }">
		<MvASSIGN NAME = "l.label" VALUE = "Card Holder:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 2 }">
		<MvASSIGN NAME = "l.label" VALUE = "Card Number:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 3 }">
		<MvASSIGN NAME = "l.label" VALUE = "Expiration Date:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 4 }">
		<MvASSIGN NAME = "l.label" VALUE = "Authorization Request ID:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 5 }">
		<MvASSIGN NAME = "l.label" VALUE = "Amount Authorized:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 6 }">
		<MvASSIGN NAME = "l.label" VALUE = "AVS Code:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 7 }">
		<MvASSIGN NAME = "l.label" VALUE = "Authorization Code:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 8 }">
		<MvASSIGN NAME = "l.label" VALUE = "Authorization Date/Time:">
	</MvIF>

	<MvIF EXPR = "{ g.ICS2Order_Billed }">
		<MvIF EXPR = "{ l.field_id EQ 9 }">
			<MvASSIGN NAME = "l.label" VALUE = "Amount Billed:">
		</MvIF>

		<MvIF EXPR = "{ l.field_id EQ 10 }">
			<MvASSIGN NAME = "l.label" VALUE = "Billing Date/Time:">
		</MvIF>

		<MvIF EXPR = "{ ( l.field_id EQ 11 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvASSIGN NAME = "l.label" VALUE = "Billing Status:">
		</MvIF>
	<MvELSE>
		<MvIF EXPR = "{ ( l.field_id EQ 9 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvASSIGN NAME = "l.label" VALUE = "Billing Status:">
		</MvIF>
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.label }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Report_Value" PARAMETERS = "field_id" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.value" VALUE = "">

	<MvIF EXPR = "{ l.field_id EQ 1 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_CC_FirstName ) $ ' ' $ encodeentities( g.ICS2Order_CC_LastName ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 2 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_CC_Number ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 3 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_CC_ExpMonth ) $ '/' $ encodeentities( g.ICS2Order_CC_ExpYear ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 4 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Auth_Request_ID ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 5 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Auth_Amount ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 6 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Auth_AVS ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 7 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Auth_Code ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 8 }">
		<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Auth_Time ) }">
	</MvIF>

	<MvIF EXPR = "{ g.ICS2Order_Billed }">
		<MvIF EXPR = "{ l.field_id EQ 9 }">
			<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Bill_Amount ) }">
		</MvIF>

		<MvIF EXPR = "{ l.field_id EQ 10 }">
			<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Bill_Time ) }">
		</MvIF>

		<MvIF EXPR = "{ ( l.field_id EQ 11 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Bill_Message ) }">
		</MvIF>
	<MvELSE>
		<MvIF EXPR = "{ ( l.field_id EQ 9 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvASSIGN NAME = "l.value" VALUE = "{ encodeentities( g.ICS2Order_Bill_Message ) }">
		</MvIF>
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.value }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_OrderEdit_Description" STANDARDOUTPUTLEVEL = "">
	<MvFUNCTIONRETURN VALUE = "Credit Card">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_OrderEdit_Count" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.count" VALUE = 0>

	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvFIND NAME = "ICS2Orders" VALUE = "{ Orders.d.id }" EXACT>
		<MvIF EXPR = "{ NOT ICS2Orders.d.EOF }">
			<MvASSIGN NAME = "g.ICS2Order_Billed" VALUE = "{ ICS2Orders.d.billed }">
			<MvASSIGN NAME = "g.ICS2Order_CC_FirstName" VALUE = "{ ICS2Orders.d.cc_fname }">
			<MvASSIGN NAME = "g.ICS2Order_CC_LastName" VALUE = "{ ICS2Orders.d.cc_lname }">
			<MvASSIGN NAME = "g.ICS2Order_CC_Number" VALUE = "{ ICS2Orders.d.cc_number }">
			<MvASSIGN NAME = "g.ICS2Order_CC_ExpMonth" VALUE = "{ ICS2Orders.d.cc_expmo }">
			<MvASSIGN NAME = "g.ICS2Order_CC_ExpYear" VALUE = "{ ICS2Orders.d.cc_expyr }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Request_ID" VALUE = "{ ICS2Orders.d.auth_id }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Amount" VALUE = "{ ICS2Orders.d.auth_amnt }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_AVS" VALUE = "{ ICS2Orders.d.auth_avs }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Code" VALUE = "{ ICS2Orders.d.auth_code }">
			<MvASSIGN NAME = "g.ICS2Order_Auth_Time" VALUE = "{ ICS2Orders.d.auth_time }">
			<MvASSIGN NAME = "g.ICS2Order_Bill_Amount" VALUE = "{ ICS2Orders.d.bill_amnt }">
			<MvASSIGN NAME = "g.ICS2Order_Bill_Time" VALUE = "{ ICS2Orders.d.bill_time }">
			<MvASSIGN NAME = "g.ICS2Order_Bill_Message" VALUE = "{ ICS2Orders.d.bill_rmsg }">

			<MvASSIGN NAME = "l.count" VALUE = 8>

			<MvIF EXPR = "{ g.ICS2Order_Billed }">
				<MvASSIGN NAME = "l.count" VALUE = "{ l.count + 2 }">
			</MvIF>

			<MvIF EXPR = "{ len( g.ICS2Order_Bill_Message ) }">
				<MvASSIGN NAME = "l.count" VALUE = "{ l.count + 1 }">
			</MvIF>
		</MvIF>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.count }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_OrderEdit_Prompt" PARAMETERS = "field_id" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.prompt" VALUE = "">

	<MvIF EXPR = "{ l.field_id EQ 1 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Card Holder:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 2 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Card Number:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 3 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Expiration Date:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 4 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Authorization Request ID:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 5 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Amount Authorized:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 6 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "AVS Code:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 7 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Authorization Code:">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 8 }">
		<MvASSIGN NAME = "l.prompt" VALUE = "Authorization Date/Time:">
	</MvIF>

	<MvIF EXPR = "{ g.ICS2Order_Billed }">
		<MvIF EXPR = "{ l.field_id EQ 9 }">
			<MvASSIGN NAME = "l.prompt" VALUE = "Amount Billed:">
		</MvIF>

		<MvIF EXPR = "{ l.field_id EQ 10 }">
			<MvASSIGN NAME = "l.prompt" VALUE = "Billing Date/Time:">
		</MvIF>

		<MvIF EXPR = "{ ( l.field_id EQ 11 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvASSIGN NAME = "l.prompt" VALUE = "Billing Status:">
		</MvIF>
	<MvELSE>
		<MvIF EXPR = "{ ( l.field_id EQ 9 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvASSIGN NAME = "l.prompt" VALUE = "Billing Status:">
		</MvIF>
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.prompt }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_OrderEdit_Field" PARAMETERS = "field_id" STANDARDOUTPUTLEVEL = "text, html, compresswhitespace">
	<MvASSIGN NAME = "l.ok" VALUE = 1>

	<MvIF EXPR = "{ l.field_id EQ 1 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_CC_FirstName ) $ ' ' $ encodeentities( g.ICS2Order_CC_LastName ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 2 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_CC_Number ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 3 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_CC_ExpMonth ) $ '/' $ encodeentities( g.ICS2Order_CC_ExpYear ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 4 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Auth_Request_ID ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 5 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Auth_Amount ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 6 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Auth_AVS ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 7 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Auth_Code ) }">
	</MvIF>

	<MvIF EXPR = "{ l.field_id EQ 8 }">
		<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Auth_Time ) }">
	</MvIF>

	<MvIF EXPR = "{ g.ICS2Order_Billed }">
		<MvIF EXPR = "{ l.field_id EQ 9 }">
			<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Bill_Amount ) }">
		</MvIF>

		<MvIF EXPR = "{ l.field_id EQ 10 }">
			<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Bill_Time ) }">
		</MvIF>

		<MvIF EXPR = "{ ( l.field_id EQ 11 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Bill_Message ) }">
		</MvIF>
	<MvELSE>
		<MvIF EXPR = "{ ( l.field_id EQ 9 ) AND ( len( g.ICS2Order_Bill_Message ) ) }">
			<MvEVAL EXPR = "{ encodeentities( g.ICS2Order_Bill_Message ) }">
		</MvIF>
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_OrderEdit_Update" STANDARDOUTPUTLEVEL = "">
	<MvFUNCTIONRETURN VALUE = 1>
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Order_Delete" PARAMETERS = "order_id, pay_data" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvFIND NAME = "ICS2Orders" VALUE = "{ l.pay_data }" EXACT>
		<MvIF EXPR = "{ NOT ICS2Orders.d.EOF }">
			<MvDELETE NAME = "ICS2Orders">
		</MvIF>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Authorize" PARAMETERS = "data, total" STANDARDOUTPUTLEVEL = "text,html">
	<MvASSIGN NAME = "l.pay_data" VALUE = "">

	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvASSIGN NAME = "l.ics_applications" VALUE = "ics_auth">
		<MvASSIGN NAME = "l.merchant_id" VALUE = "{ ICS2.d.merchant }">

		<MvASSIGN NAME = "l.merchant_ref_number" VALUE = "{ BasketList.d.order_id }">
		<MvASSIGN NAME = "l.currency" VALUE = "{ ICS2.d.currency }">

		<MvASSIGN NAME = "l.offer0" VALUE = "{ 'offerid:0^product_name:Miva Order^merchant_product_sku:merchant^product_code:default^amount:' $ 
											   ( l.total ROUND 2 ) $ '^quantity:1' }">

		<MvASSIGN NAME = "l.customer_firstname" VALUE = "{ g.ICS2_CardFirstName }">
		<MvASSIGN NAME = "l.customer_lastname" VALUE = "{ g.ICS2_CardLastName }">
		<MvASSIGN NAME = "l.customer_email" VALUE = "{ BasketList.d.bill_email }">
		<MvASSIGN NAME = "l.customer_phone" VALUE = "{ BasketList.d.bill_phone }">
		<MvASSIGN NAME = "l.customer_cc_number" VALUE = "{ g.ICS2_CardNumber }">
		<MvASSIGN NAME = "l.customer_cc_expmo" VALUE = "{ g.ICS2_CardExp_Month }">
		<MvASSIGN NAME = "l.customer_cc_expyr" VALUE = "{ g.ICS2_CardExp_Year }">
		
		<MvASSIGN NAME = "l.bill_address1" VALUE = "{ BasketList.d.bill_addr }">
		<MvASSIGN NAME = "l.bill_city" VALUE = "{ BasketList.d.bill_city }">
		<MvASSIGN NAME = "l.bill_state" VALUE = "{ BasketList.d.bill_state }">
		<MvASSIGN NAME = "l.bill_zip" VALUE = "{ BasketList.d.bill_zip }">
		<MvASSIGN NAME = "l.bill_country" VALUE = "{ BasketList.d.bill_cntry }">

		<MvASSIGN NAME = "l.ship_to_address1" VALUE = "{ BasketList.d.ship_addr }">
		<MvASSIGN NAME = "l.ship_to_city" VALUE = "{ BasketList.d.ship_city }">
		<MvASSIGN NAME = "l.ship_to_state" VALUE = "{ BasketList.d.ship_state }">
		<MvASSIGN NAME = "l.ship_to_zip" VALUE = "{ BasketList.d.ship_zip }">
		<MvASSIGN NAME = "l.ship_to_country" VALUE = "{ BasketList.d.ship_cntry }">

		<MIVA MvCOMMERCE_Error = "nonfatal, nodisplay">
		<MvCOMMERCE METAMETHOD = "ICS2"
					ACTION = "{ ICS2.d.url }"
					FIELDS = "ics_applications, merchant_id, merchant_ref_number, currency, offer0, customer_firstname, 
							  customer_lastname, customer_email, customer_phone, customer_cc_number, customer_cc_expmo,
							  customer_cc_expyr, bill_address1, bill_city, bill_state, bill_zip, bill_country, 
							  ship_to_address1, ship_to_city, ship_to_state, ship_to_zip, ship_to_country">
			<MvIF EXPR = "{ s.auth_rcode EQ 1 }">
				<MvIF EXPR = "{ ICS2.d.store_cc }">
					<MvASSIGN NAME = "l.cc_number" VALUE = "{ g.ICS2_CardNumber }">
				<MvELSE>
					<MvASSIGN NAME = "l.cc_number" VALUE = "{ substring( g.ICS2_CardNumber, 1, 6 ) }">
				</MvIF>

				<MvASSIGN NAME = "l.pay_data" VALUE = "{ ICS2Order_Insert( BasketList.d.order_id, 
																		   0,
																		   g.ICS2_CardFirstName,
																		   g.ICS2_CardLastName,
																		   l.cc_number,
																		   g.ICS2_CardExp_Month,
																		   g.ICS2_CardExp_Year,
																		   s.request_id,
																		   s.auth_auth_amount,
																		   s.auth_auth_avs,
																		   s.auth_auth_code,
																		   s.auth_auth_time,
																		   0,
																		   '',
																		   '' ) }">
			<MvELSE>
				<MvASSIGN NAME = "g.Authorization_Failure_Message" VALUE = "{ encodeentities( s.ics_rmsg ) }">

				<MvIF EXPR = "{ s.ics_rflag EQ 'DAVSNO' }">
					<MvASSIGN NAME = "g.Authorization_Failure_Message" VALUE = "The credit card does not pass the AVS check.">
				</MvIF>

				<MvIF EXPR = "{ s.ics_rflag EQ 'DCARDEXPIRED' }">
					<MvASSIGN NAME = "g.Authorization_Failure_Message" VALUE = "The credit card has expired">
					<MvASSIGN NAME = "g.ICS2_CardExp_Invalid" VALUE = 1>
				</MvIF>

				<MvIF EXPR = "{ s.ics_rflag EQ 'DINVALIDCARD' }">
					<MvASSIGN NAME = "g.Authorization_Failure_Message" VALUE = "Invalid credit card number">
					<MvASSIGN NAME = "g.ICS2_CardNumber_Invalid" VALUE = 1>
				</MvIF>
			</MvIF>
		</MvCOMMERCE>
		<MIVA MvCOMMERCE_Error = "fatal, display">

		<MvIF EXPR = "{ MvCOMMERCE_Error }">
			<MvASSIGN NAME = "g.Authorization_Failure_Message" VALUE = "{ MvCOMMERCE_Error }">
		</MvIF>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.pay_data }">
</MvFUNCTION>

<MvFUNCTION NAME = "Payment_Process" STANDARDOUTPUTLEVEL = "">
	<MvASSIGN NAME = "l.processed" VALUE = -1>

	<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Open() }">
	<MvIF EXPR = "{ l.ok }">
		<MvFIND NAME = "ICS2Orders" VALUE = "{ Orders.d.id }" EXACT>
		<MvIF EXPR = "{ ( NOT ICS2Orders.d.EOF ) AND ( NOT ICS2Orders.d.billed ) }">
			<MvASSIGN NAME = "l.processed" VALUE = 0>

			<MvASSIGN NAME = "l.ics_applications" VALUE = "ics_bill">
			<MvASSIGN NAME = "l.merchant_id" VALUE = "{ ICS2.d.merchant }">

			<MvASSIGN NAME = "l.merchant_ref_number" VALUE = "{ Orders.d.id }">
			<MvASSIGN NAME = "l.auth_request_id" VALUE = "{ ICS2Orders.d.auth_id }">
			<MvASSIGN NAME = "l.currency" VALUE = "{ ICS2.d.currency }">

			<MvASSIGN NAME = "l.offer0" VALUE = "{ 'offerid:0^product_name:Miva Order Order^merchant_product_sku:merchant^product_code:default^amount:' $ 
												   ( ICS2Orders.d.auth_amnt ROUND 2 ) $ '^quantity:1' }">

			<MIVA MvCOMMERCE_Error = "nonfatal, nodisplay">
			<MvCOMMERCE METAMETHOD = "ICS2"
					    ACTION = "{ ICS2.d.url }"
					    FIELDS = "ics_applications, merchant_id, merchant_ref_number, auth_request_id, currency, offer0">
				<MvASSIGN NAME = "ICS2Orders.d.bill_rmsg" VALUE = "{ s.bill_rmsg }">

				<MvIF EXPR = "{ s.bill_rcode EQ 1 }">
					<MvASSIGN NAME = "l.processed" VALUE = 1>

					<MvASSIGN NAME = "ICS2Orders.d.billed" VALUE = 1>
					<MvASSIGN NAME = "ICS2Orders.d.bill_amnt" VALUE = "{ s.bill_bill_amount }">
					<MvASSIGN NAME = "ICS2Orders.d.bill_time" VALUE = "{ s.bill_bill_request_time }">
				</MvIF>
			</MvCOMMERCE>
			<MIVA MvCOMMERCE_Error = "fatal, display">

			<MvIF EXPR = "{ MvCOMMERCE_Error }">
				<MvASSIGN NAME = "ICS2Orders.d.bill_rmsg" VALUE = "{ MvCOMMERCE_Error }">
			</MvIF>

			<MvUPDATE NAME = "ICS2Orders">
		</MvIF>

		<MvASSIGN NAME = "l.ok" VALUE = "{ ICS2_Close() }">
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.processed }">
</MvFUNCTION>

<MvFUNCTION NAME = "ICS2_Open" STANDARDOUTPUTLEVEL = "" ERROROUTPUTLEVEL = "syntax, expression">
	<MvASSIGN NAME = "l.ok" VALUE = 1>

	<MvASSIGN NAME = "l.directory" VALUE = "{ g.OrderPath $ 'ics2/' }">

	<MvOPEN NAME = "ICS2" DATABASE = "{ l.directory $ 'ics2.dbf' }">
	<MvIF EXPR = "{ MvOPEN_Error }">
		<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00007', MvOPEN_Error ) }">
	</MvIF>

	<MvIF EXPR = "{ l.ok }">
		<MvOPEN NAME = "ICS2Cards" DATABASE = "{ l.directory $ 'ics2card.dbf' }" INDEXES = "{ l.directory $ 'ics2card.mvx' }">
		<MvIF EXPR = "{ MvOPEN_Error }">
			<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00008', MvOPEN_Error ) }">
		</MvIF>
	</MvIF>

	<MvIF EXPR = "{ l.ok }">
		<MvOPEN NAME = "ICS2Orders" DATABASE = "{ l.directory $ 'ics2ordr.dbf' }" INDEXES = "{ l.directory $ 'ics2ordr.mvx' }">
		<MvIF EXPR = "{ MvOPEN_Error }">
			<MvDO FILE = "{ g.Library_Utilities }" NAME = "l.ok" VALUE = "{ Error( 'ORD-ICS-00009', MvOPEN_Error ) }">
		</MvIF>
	</MvIF>

	<MvFUNCTIONRETURN VALUE = "{ l.ok }">
</MvFUNCTION>

<MvFUNCTION NAME = "ICS2_Close" STANDARDOUTPUTLEVEL = "">
	<MvCLOSE NAME = "ICS2Orders">
	<MvCLOSE NAME = "ICS2Cards">
	<MvCLOSE NAME = "ICS2">

	<MvFUNCTIONRETURN VALUE = 1>
</MvFUNCTION>

<MvFUNCTION NAME = "ICS2Card_Insert" PARAMETERS = "code, name, enabled">
	<MvASSIGN NAME = "ICS2Cards.d.code" VALUE = "{ l.code }">
	<MvASSIGN NAME = "ICS2Cards.d.name" VALUE = "{ l.name }">
	<MvASSIGN NAME = "ICS2Cards.d.enabled" VALUE = "{ l.enabled }">

	<MvADD NAME = "ICS2Cards">
	<MvFUNCTIONRETURN VALUE = 1>
</MvFUNCTION>

<MvFUNCTION NAME = "ICS2Order_Insert" PARAMETERS = "order_id, billed, cc_fname, cc_lname, cc_number, cc_expmo, cc_expyr,
													auth_id, auth_amnt, auth_avs, auth_code, auth_time, bill_amnt, bill_time,
													bill_rmsg">
	<MvASSIGN NAME = "ICS2Orders.d.order_id" VALUE = "{ l.order_id }">
	<MvASSIGN NAME = "ICS2Orders.d.billed" VALUE = "{ l.billed }">
	<MvASSIGN NAME = "ICS2Orders.d.cc_fname" VALUE = "{ l.cc_fname }">
	<MvASSIGN NAME = "ICS2Orders.d.cc_lname" VALUE = "{ l.cc_lname }">
	<MvASSIGN NAME = "ICS2Orders.d.cc_number" VALUE = "{ l.cc_number }">
	<MvASSIGN NAME = "ICS2Orders.d.cc_expmo" VALUE = "{ l.cc_expmo }">
	<MvASSIGN NAME = "ICS2Orders.d.cc_expyr" VALUE = "{ l.cc_expyr }">
	<MvASSIGN NAME = "ICS2Orders.d.auth_id" VALUE = "{ l.auth_id }">
	<MvASSIGN NAME = "ICS2Orders.d.auth_amnt" VALUE = "{ l.auth_amnt }">
	<MvASSIGN NAME = "ICS2Orders.d.auth_avs" VALUE = "{ l.auth_avs }">
	<MvASSIGN NAME = "ICS2Orders.d.auth_code" VALUE = "{ l.auth_code }">
	<MvASSIGN NAME = "ICS2Orders.d.auth_time" VALUE = "{ l.auth_time }">
	<MvASSIGN NAME = "ICS2Orders.d.bill_amnt" VALUE = "{ l.bill_amnt }">
	<MvASSIGN NAME = "ICS2Orders.d.bill_time" VALUE = "{ l.bill_time }">
	<MvASSIGN NAME = "ICS2Orders.d.bill_rmsg" VALUE = "{ l.bill_rmsg }">

	<MvADD NAME = "ICS2Orders">

	<MvFUNCTIONRETURN VALUE = "{ ICS2Orders.d.order_id }">
</MvFUNCTION>

