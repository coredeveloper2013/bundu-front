<MvCOMMENT>
|
| Miva Order v1.x
| (c) 1997, 1998, 1999, 2000, 2001 Miva Corporation
|
| 2629 Ariane Drive
| San Diego, CA 92117
| 619-490-2570
| sales@miva.com
|
| $Source: /home/cvs/order/Order/lib/help.mv,v $
|
| Online Help
|
</MvCOMMENT>

<MvFUNCTION NAME = "Screen_Help" PARAMETERS = "section, field"
			STANDARDOUTPUTLEVEL = "text, html, compresswhitespace">
	<HTML>
	<HEAD><TITLE>Help</TITLE>

	<BODY BGCOLOR = "#ffffff">

	<!-- Registration -->
	<MvIF EXPR = "{ l.section EQ 'Registration' }">
		<!-- Owner (Multiple Fields) -->
		<MvIF EXPR = "{ l.field EQ 'Owner' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Owner/Address Information</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The information contained here is used for 
			registration purposes, and may be sent to
			Miva Corporation over the Internet when
			installing upgrades or updates.
			</P>
			</FONT>
		</MvIF>
	</MvIF>
	
	<!-- Administration Settings -->
	<MvIF EXPR = "{ l.section EQ 'Admin' }">
		<!-- Administration Session Timeout (Minutes) -->
		<MvIF EXPR = "{ l.field EQ 'Timeout' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Administration Session Timeout (Minutes)</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This value controls the maximum amount of time,
			in minutes, that a user may be idle before being
			automatically logged out of the Administration
			module.
			</P>
			</FONT>
		</MvIF>

		<!-- Administration basket Timeout (Minutes) -->
		<MvIF EXPR = "{ l.field EQ 'BasketTimeout' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Basket Timeout (Minutes)</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This value controls the maximum amount of time, in minutes, that a user may be idle 
			before being automatically logged out of the order form
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<!-- URLs -->
	<MvIF EXPR = "{ l.section EQ 'URL' }">
		<!-- Non-secure URL to Miva Order -->
		<MvIF EXPR = "{ l.field EQ 'Order' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Non-secure URL to Miva Order</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This value, which was originally auto-detected by
			the Miva Order setup program, is stored here
			for informational purposes.
			</P>
			</FONT>
		</MvIF>

		<!-- Secure URL to Miva Order -->
		<MvIF EXPR = "{ l.field EQ 'Order_Secure' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Secure URL to Miva Order</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			If this field contains a value, Miva Order Administration will use the URL configured
			here when displaying sensitive data, such as customer order information. 
			Note, however, that if you have logged into the Miva Order Administration via a 
			non-secure (SSL) connection, the value configured here will not be used.
			</P>
			</FONT>
		</MvIF>

		<!-- Secure URL to Administration -->
		<MvIF EXPR = "{ l.field EQ 'Admin_Secure' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Secure URL to Administration</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			If this field contains a value, Miva Order Administration
			will use the URL configured here when displaying 
			sensitive data, such as customer order information.
			</P>

			<P>
			Note, however, that if you have logged into the Miva Order
			Administration via a secure (SSL) connection, the value
			configured here will not be used.
			</P>
			</FONT>
		</MvIF>

		<!-- Root Directory for Graphics -->
		<MvIF EXPR = "{ l.field EQ 'ImageRoot' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Root Directory for Graphics</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Uploaded images will be stored in the directory 
			configured here.  The directory	entered into this 
			field should be a same directory referenced by the 
			<B>Base URL for Graphics</B> and <B>Secure Base URL 
			for Graphics</B>.
			</P>
			</FONT>
		</MvIF>

		<!-- Base URL for Graphics -->
		<MvIF EXPR = "{ l.field EQ 'Base' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Base URL for Graphics</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Miva Order will reference all images displayed on
			non-secure (SSL) pages via relative	paths from the 
			URL configured here.  The URL entered into this field
			should reference the same directory as the directory
			configured as the <B>Root Directory for Graphics</B>.
			</P>
			</FONT>
		</MvIF>

		<!-- Secure Base URL for Graphics -->
		<MvIF EXPR = "{ l.field EQ 'Base_Secure' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Secure Base URL for Graphics</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Miva Order will reference all images displayed on
			secure (SSL) pages via relative	paths from the 
			URL configured here.  If this field is left blank,
			Miva Order will use the value configured as the
			<B>Base URL for Graphics</B> instead.  This can
			cause some browsers to display a warning message
			about mixed secure and non-secure content.  The URL 
			entered into this field	should reference the same 
			directory as the directory configured as the 
			<B>Root Directory for Graphics</B>.
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<!-- Look & Feel -->
	<MvIF EXPR = "{ l.section EQ 'LookAndFeel' }">
		<!-- Body Background Color -->
		<MvIF EXPR = "{ l.field EQ 'Body_Background' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Background Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the background color
			of text displayed on the order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Background Image -->
		<MvIF EXPR = "{ l.field EQ 'Body_Image' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Background Image</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure a background image for
			the order form.  If you leave this field blank,
			then no background image will be used.
			</P>

			<P>
			You may upload a file from your PC by selecting the
			<B>Upload File</B> option and entering a local 
			filename or pressing the <B>Browse</B> button.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Text Color -->
		<MvIF EXPR = "{ l.field EQ 'Body_Text' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Text Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the color
			of text displayed on the order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Link Color -->
		<MvIF EXPR = "{ l.field EQ 'Body_Link' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Link Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the color of
			non-viewed, non-active links displayed on the
			order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Active Link Color -->
		<MvIF EXPR = "{ l.field EQ 'Body_ActiveLink' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Active Link Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the color of
			active links displayed on the order form.
			</P>

			<P>
			Active links are links which have been highlighted
			by the user, typically either by moving the mouse 
			over the link or clicking on the link, although
			this is browser dependent.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Viewed Link Color -->
		<MvIF EXPR = "{ l.field EQ 'Body_ViewedLink' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Viewed Link Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the color of
			viewed links displayed on the order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Font Face -->
		<MvIF EXPR = "{ l.field EQ 'Body_Font' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Font Face</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the face (style)
			of the font used for text on the order form.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Font Size -->
		<MvIF EXPR = "{ l.field EQ 'Body_FontSize' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Font Size</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the size
			of the font used for text on the order form.
			</P>

			<P>
			You may enter either an absolute size, such as <I>2</I>
			or <I>1</I>, or a relative size, such as <I>+1</I> or
			<I>-2</I>, or leave this field blank, which will allow
			the browser to use the default size.
			</P>
			</FONT>
		</MvIF>

		<!-- Body Font Face -->
		<MvIF EXPR = "{ l.field EQ 'Body_FontFace' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Body Font Face</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the face (style) of the font used for text on the
			order form. To enter more than one style, separate the styles by
			a comma (e.g. Arial, Helvetica).
			</P>
			</FONT>
		</MvIF>

		<!-- ProductList_ColumnHeader -->
		<MvIF EXPR = "{ l.field EQ 'ProductList_ColumnHeader' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Column Header Font Face</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the face (style) of the font used for the header row
			above the product list on the order form. To enter more than one style, separate the
			styles by a comma (e.g. Arial, Helvetica).
			</P>
			</FONT>
		</MvIF>

		<!-- ProductList_ColumnHeader -->
		<MvIF EXPR = "{ l.field EQ 'OrderForm_ColumnHeader' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Order Form Column Header Font Face</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the face (style) of the font used for the header row 
			above the customer information on the order form. To enter more than one style, separate
			the styles with a comma (e.g. Arial, Helvetica).
			</P>
			</FONT>
		</MvIF>

		<!-- Product List Column Header Foreground Color -->
		<MvIF EXPR = "{ l.field EQ 'ProductHeader_Foreground' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Column Header Foreground Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the color of
			the text in the header row above the product list
			on the order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Product List Column Header Background Color -->
		<MvIF EXPR = "{ l.field EQ 'ProductHeader_Background' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Column Header Background Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the background 
			color of the header row above the product list
			on the order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Product List Column Header Font Face -->
		<MvIF EXPR = "{ l.field EQ 'ProductHeader_Font' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Column Header Font Face</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the face (style)
			of the font used for the header row above the product list
			on the order form.
			</P>
			</FONT>
		</MvIF>

		<!-- Product List Column Header Font Size -->
		<MvIF EXPR = "{ l.field EQ 'ProductHeader_FontSize' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Column Header Font Size</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the size
			of the font used for the header row above the
			product list on the order form.
			</P>

			<P>
			You may enter either an absolute size, such as <I>2</I>
			or <I>1</I>, or a relative size, such as <I>+1</I> or
			<I>-2</I>, or leave this field blank, which will allow
			the browser to use the default size.
			</P>
			</FONT>
		</MvIF>

		<!-- Product List Alternate Background Color -->
		<MvIF EXPR = "{ l.field EQ 'ProductList_Alternate' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Alternate Background Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			If a value is entered into this field, it will
			be used as the background color for every other
			row in the product list, giving a "green bar"
			effect.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Order Form Column Header Foreground Color -->
		<MvIF EXPR = "{ l.field EQ 'Header_Foreground' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Order Form Column Header Foreground Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the color of
			the text in the header row above the customer
			information	on the order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<!-- Order Form Column Header Background Color -->
		<MvIF EXPR = "{ l.field EQ 'Header_Background' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Order Form Column Header Background Color</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to change the background 
			color of the header row above the customer
			information	on the order form.
			</P>

			<P>
			You may enter either a symbolic name, such as 
			<I>blue</I> or <I>red</I>, or a hexadecimal
			color code, such as <I>#ffffff</I> or <I>#00bc0a</I>.
			</P>
			</FONT>
		</MvIF>

		<MvIF EXPR = "{ l.field EQ 'Header_Font' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Order Form Column Header Font Face</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the face (style) of the font used for the header row 
			above the customer information on the order form. To enter more than one style, separate
			the styles with a comma (e.g. Arial, Helvetica).
			</P>
			</FONT>
		</MvIF>

		<!-- Order Form Column Header Font Face -->
		<MvIF EXPR = "{ l.field EQ 'Header_FontSize' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Order Form Column Header Font Size</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field allows you to configure the size
			of the font used for the header row above the
			customer information on the order form.
			</P>

			<P>
			You may enter either an absolute size, such as <I>2</I>
			or <I>1</I>, or a relative size, such as <I>+1</I> or
			<I>-2</I>, or leave this field blank, which will allow
			the browser to use the default size.
			</P>
			</FONT>
		</MvIF>

		<!-- Product List Header -->
		<MvIF EXPR = "{ l.field EQ 'ProductList_Header' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Header</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text entered into this field will be displayed
			immediately before the product list on the Miva
			Order order form.
			</P>

			<P>
			You may enter any combination of text and HTML.
			</P>
			</FONT>
		</MvIF>

		<!-- Product List Footer -->
		<MvIF EXPR = "{ l.field EQ 'ProductList_Footer' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product List Footer</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text entered into this field will be displayed
			immediately after the product list on the order form.
			</P>

			<P>
			You may enter any combination of text and HTML.
			</P>
			</FONT>
		</MvIF>

		<!-- Order Form Header -->
		<MvIF EXPR = "{ l.field EQ 'OrderForm_Header' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Order Form Header</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text entered into this field will be displayed
			immediately before the customer information on the
			order form.
			</P>

			<P>
			You may enter any combination of text and HTML.
			</P>
			</FONT>
		</MvIF>

		<!-- Order Form Footer -->
		<MvIF EXPR = "{ l.field EQ 'OrderForm_Footer' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Order Form Footer</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text entered into this field will be displayed
			immediately after the customer information on the
			order form.
			</P>

			<P>
			You may enter any combination of text and HTML.
			</P>
			</FONT>
		</MvIF>

		<!-- Thank You Message Header -->
		<MvIF EXPR = "{ l.field EQ 'Invoice_Header' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Thank You Message Header</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text entered into this field will be displayed
			immediately before the invoice on the Miva Order
			invoice page.
			</P>

			<P>
			You may enter any combination of text and HTML.
			</P>
			</FONT>
		</MvIF>

		<!-- Thank You Message Footer -->
		<MvIF EXPR = "{ l.field EQ 'Invoice_Footer' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Thank You Message Footer</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text entered into this field will be displayed
			immediately after the invoice on the Miva Order 
			invoice page.
			</P>

			<P>
			You may enter any combination of text and HTML.
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<!-- Order Notification -->
	<MvIF EXPR = "{ l.section EQ 'Notification' }">
		<!-- Enabled -->
		<MvIF EXPR = "{ l.field EQ 'Enabled' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Enabled</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			If this check-box is checked, Miva Order will send
			an Email notification message each time an order
			is placed.
			</P>
			</FONT>
		</MvIF>

		<!-- CC_Notification-->
		<MvIF EXPR = "{ l.field EQ 'CC' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>CC Notification</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Email notification messages sent by Miva Order will be CC'ed to this address. To enter
			multiple addresses, separate the addresses with a comma (e.g. abc@company.com, def@company.com)
			</P>
			</FONT>
		</MvIF>

		<!-- From -->
		<MvIF EXPR = "{ l.field EQ 'From' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>From</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Email notification messages sent by Miva Order
			will appear to come from this address.  If you
			select <B>Customer's Billing Email Address</B>,
			then the Email address entered by the customer
			as part of the ordering process will be used.
			Otherwise, you may enter any Email address
			to use as the sender.
			</P>
			</FONT>
		</MvIF>

		<!-- To -->
		<MvIF EXPR = "{ l.field EQ 'To' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>To</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Email notification messages sent by Miva Order
			will be sent to this address.
			</P>
			</FONT>
		</MvIF>

		<!-- Add Angle Brackets to Email Address -->
		<MvIF EXPR = "{ l.field EQ 'Add_Angle' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Add Angle Brackets to Email Addresses</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Some SMTP servers require that Email addresses
			be enclosed in angle brackets (i.e. &lt;email@sample.com&gt).
			If checked, Miva Order will automatically add
			angle brackets to Email addresses that do not
			already contain them.
			</P>
			</FONT>
		</MvIF>

		<!-- Subject -->
		<MvIF EXPR = "{ l.field EQ 'Subject' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Subject</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text configured here will be used as the
			subject of Email notification messages sent by
			Miva Order.
			</P>
			</FONT>
		</MvIF>

		<!-- Mail Host -->
		<MvIF EXPR = "{ l.field EQ 'MailHost' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Mail Host</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field should be set to either the IP address or
			hostname of your SMTP server.  Your ISP should be
			able to assist you in obtaining this address.
			</P>

			<P>
			<B>Note:</B>  Miva Order will not be able to send
			Email notification messages unless a valid Mail Host
			is configured.
			</P>
			</FONT>
		</MvIF>

		<!-- Header Text (Precedes Order Information) -->
		<MvIF EXPR = "{ l.field EQ 'Header' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Header Text (Precedes Order Information)</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			The text entered into this field will be included
			immediately before the order information in
			Email notification messages.
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<!-- States/Sales Tax -->
	<MvIF EXPR = "{ l.section EQ 'States' }">
		<!-- States/Sales Tax -->
		<MvIF EXPR = "{ l.field EQ 'States' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>States/Sales Tax</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Miva Order calculates sales tax based on the state
			of the shipping address of the customer.  In addition,
			the states listed here are used to generate the
			drop-down lists on the order form.
			</P>

			<P>
			If the tax rate for a state is set to 0%, then <I>no</I>
			tax will be assessed for customers in that state.  Otherwise
			the configured percentage of the order total will be
			added to the order.
			</P>
			</FONT>
		</MvIF>
	</MvIF>
 
	<!-- Shipping Method-->
	<MvIF EXPR = "{ l.section EQ 'Shipping' }">
		<!-- Accepted Credit Cards -->
		<MvIF EXPR = "{ l.field EQ 'BaseUnit' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Shipping/Base Unit</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Miva Order calculates shipping cost based on the shipping method 
			that the customer chooses.  In addition,
			the shipping methods listed here are used to generate the
			drop-down lists on the order form.
			</P>

			<P>
			If base charge and amount/weight are set to 0, then <I>no</I>
			shipping cost will be assessed for customers.  Otherwise
			the configured shipping cost will be
			added to the order.
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<!-- Accepted Credit Cards -->
	<MvIF EXPR = "{ l.section EQ 'Cards' }">
		<!-- Accepted Credit Cards -->
		<MvIF EXPR = "{ l.field EQ 'Cards' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Accepted Credit Cards</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Credit cards listed here will be displayed as options
			in a drop-down list on the order form.
			</P>

			<P>
			If the <B>Validate</B> check-box is checked for a given
			card, then Miva Order will validate credit card numbers
			entered using a LUHN-10 check.
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<!-- Add/Edit Product -->
	<MvIF EXPR = "{ l.section EQ 'Product' }">
		<!-- Product Code -->
		<MvIF EXPR = "{ l.field EQ 'Code' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Product Code</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			A product code is simply a unique identifier used
			to reference each product.  You may use any value
			you wish, but only one product may have a specific
			code. Your product List will be
			sorted alphabetically by the Product Code.
			</P>
			</FONT>
		</MvIF>

		<!-- Shipping Cost -->
		<MvIF EXPR = "{ l.field EQ 'Shipping' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Shipping Cost</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This field configures the per-unit cost of shipping
			a product.  If 0, then no shipping charges will be
			applied for the product.
			</P>
			</FONT>
		</MvIF>

		<!-- Taxable -->
		<MvIF EXPR = "{ l.field EQ 'Taxable' }">
			<P>
			<FONT FACE = "Arial, Helvetica"><B>Taxable</B></FONT>
			</P>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			This check-box controls whether or not a product
			is included in sales tax calculation.
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<!-- Create a Batch-->
	<MvIF EXPR = "{ l.section EQ 'Batch' }">
		<!-- Product Code -->
		<MvIF EXPR = "{ l.field EQ 'CreateBatch' }">
			</P>
			<FONT FACE = "Arial, Helvetica"><B>Create a Batch</B></FONT>

			<FONT FACE = "Arial, Helvetica" SIZE = "-1">
			<P>
			Create Batch will batch and remove your unbatched reports from the 'Unbatched' list. 
			Your batch will then be listed under your 'Batched' reports list.
			</P>
			</FONT>
		</MvIF>
	</MvIF>

	<FONT FACE = "Arial, Helvetica" SIZE = "-1">
	<A HREF = "JavaScript:window.close();">Close</A>
	</FONT>
	
	</BODY>
	</HTML>

	<MvFUNCTIONRETURN VALUE = 1>
</MvFUNCTION>
