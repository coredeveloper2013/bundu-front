#!/usr/bin/perl
#########################################################
#             PC Randomage Script                       #
#########################################################
#                                                       #
#                                                       #
# This script was created by:                           #
#                                                       #
# PerlCoders Web Specialties PTY.                       #
# http://www.perlcoders.com                             #
#                                                       #
# This script and all included modules, lists or        #
# images, documentation are copyright only to           #
# PerlCoders PTY (http://perlcoders.com) unless         #
# otherwise stated in the module.                       #
#                                                       #
# Purchasers are granted rights to use this script      #
# on any site they own. There is no individual site     #
# license needed per site.                              #
#                                                       #
# Any copying, distribution, modification with          #
# intent to distribute as new code will result          #
# in immediate loss of your rights to use this          #
# program as well as possible legal action.             #
#                                                       #
# This and many other fine scripts are available at     #
# the above website or by emailing the authors at       #
# staff@perlcoders.com or info@perlcoders.com           #
#                                                       #
#                                                       #
#########################################################
use strict;

# Edit variable $IMGDIR to fit your directory. Please specify a whole dir!
my $IMGDIR = '/www/yellowstonelodging/randomage/pics';


# Do not edit anything else.
use CGI;
my $q = new CGI;


if(! -d $IMGDIR) {
        print $q->header();
        print "ATTEMPTING TO USE NON-EXISTANT DIRECTORY. EXITING...";
        exit 1;
}

opendir(DH, "$IMGDIR");
my @IMAGES = sort grep !/^\.?\.$/, readdir DH;
closedir(DH);

my $total = @IMAGES; # Scalar context
my $rand = int(rand($total));
my $rand_img = "$IMGDIR/$IMAGES[$rand]";
my $buffer_length = 30*1024; # 30kb image

# .png format?
if($rand_img =~ /\.png$/) {
        print $q->header(-type => 'image/png');
} elsif($rand_img =~ /\.gif$/) { # .gif format?
        print $q->header(-type => 'image/gif');
} else { # Not .png nor .gif, using jpeg type.
        print $q->header(-type => 'image/jpeg');
}

open(IMG, "$IMGDIR/$IMAGES[$rand]");

{
local $/ = \$buffer_length;
binmode(IMG);
while(<IMG>) { print; }
}
close(IMG);

