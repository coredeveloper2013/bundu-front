<!DOCTYPE html>
<html lang="en">
<head>
    <title>Yellowstone lodging, West Yellowstone motel</title>
    <meta charset="utf-8">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css"
          integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous"/>
    <!--<link rel="stylesheet" href="css/framework7.bundle.min.css">-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="container" id="main_wrapper">
    <div class="row">
        <div class="jumbotron text-center" style="width: 100%;background-image: url('/bund/images/Al\'s Westward Ho 4.jpg');background-repeat: no-repeat;background-size: 1000px 200px">
            <a href="" class="btn btn-warning" style="float: left!important;"><i class="fas fa-arrow-left 3x"></i></a>
            <h1 ><a href="index.php" style="color: #FFA500">Yellowstone lodging</a></h1>
        </div>
        <form class="custom_form" action="searched_result-old.php" method="get" id="lodging_search">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="check_in">Check In Date <span class="text-danger"> *</span> </label>
                        <input class="form-control check_in" type="text" name="start_date" placeholder="Check In Date"
                               value="<?php echo $_REQUEST['start_date'] ?>" readonly="readonly"
                               onchange="selectCheckout()" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="check_in">Check Out Date <span class="text-danger"> *</span></label>
                        <input class="form-control check_out" type="text" name="end_date" placeholder="Check Out Date"
                               value="<?php echo $_REQUEST['end_date'] ?>" readonly="readonly" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label class="control-label" for="check_in">No. of Guests <span class="text-danger"> *</span></label>
                        <input class="form-control" type="number" name="adult" placeholder="No. of Adults" min="1"
                               value="<?php echo $_REQUEST['adult'] ?>" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label class="control-label" for="rooms">No. of Rooms  <span class="text-danger"> *</span></label>
                        <input class="form-control" type="number" name="total_room" placeholder="Rooms" min="1" v-model="searchParam.total_room"
                               value="<?php if (isset($_REQUEST['total_room'])) { echo $_REQUEST['total_room']; } else { echo $_REQUEST['rooms']; } ?>" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label class="control-label" for="check_in">&nbsp;</label>
                        <button type="submit" class="btn btn-block btn-primary" onclick="validate()">Continue</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-md-12 text-primary" style="text-align: center;padding-bottom: 10px">
            <p id="error_message"></p>
        </div>
        <div class="col-md-12 text-primary" id="fetchingData" style="text-align: center;padding-bottom: 10px">
            <div class="spin">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span class="sr-only">Loading...</span>
            </div>

        </div>
        <div class="list media-list" id="">

            <table class="table" v-if="Results.length > 0">
                <tr class="verticalAlign" v-for="(result , index) in Results">
                    <td class="pl-0">

                        <img v-if="result.image == 0" :src="'images/not-found/not-found.png'" height="100" width="100"/>
                        <img v-else="" :src="result.image" height="100" width="100"/>

                    </td>
                    <td style="width: 220px;padding-left: 0px;padding-right: 0px">
                        <strong class="title">{{ result.name }}</strong> <br>
                        <small>{{ result.description }}</small>
                        <br>

                        <small class="text-danger"
                               v-if="(searchParam.adult) > (result.capacity * (result.available-result.rooms)) && result.price != 0 && (result.available-result.rooms) != 0 "><b>For
                                {{searchParam.adult}} <span v-if="searchParam.adult>1">guests</span><span v-else="">guest</span> this room is not available </b></small>
                        <small class="text-success"
                               v-if="searchParam.adult > result.capacity && searchParam.adult <= (result.capacity * (result.available-result.rooms)) && result.price != 0">
                            <b v-if="(result.capacity * (result.available-result.rooms))/searchParam.adult != 1">For {{searchParam.adult}} <span v-if="searchParam.adult>1">guests</span><span v-else="">gst</span> you will need {{Math.ceil(searchParam.adult/result.capacity)}} rooms</b>
                            <b v-else="">For {{searchParam.adult}} <span v-if="searchParam.adult>1">guests</span><span v-else="">guest</span> you will need {{result.available}} rooms</b>
                        </small>



                    </td>
                    <td style="width: 200px;">
                        <small><strong>Check In &nbsp;&nbsp;
                                :</strong> <?php echo date('d M, Y', strtotime($_REQUEST['start_date'])) ?></small>
                        <br>
                        <small><strong>Check Out
                                :</strong> <?php echo date('d M, Y', strtotime($_REQUEST['end_date'])) ?></small>
                        <span v-if="result.available-result.rooms == 0 || (searchParam.adult) > (result.capacity * (result.available-result.rooms)) || result.price == 0 " class="badge badge-warning">Not Available</span>
                        <span v-else="" class="badge badge-success">Available</span>
                    </td>
                    <td>
                        <small>
                            <b v-if="result.available != 0" >Available Room : {{(result.available-result.rooms)}}</b><br>
                            <b v-if="result.available == 0" >Available Room : {{(result.available)}}</b>
                            <b>Guests : {{result.capacity}} </b>  per {{ result.name.split(":")[0] }}<br>
                        </small>
                    </td>
                    <td>
                        <button v-if="(result.available-result.rooms) != 0 && result.price != 0" type="button" class="btn btn-sm btn-default custom_label">
                            <strong>${{result.price}}</strong>
                            <small> per night</small>
                        </button>
                        <button v-else="" type="button" class="btn btn-sm btn-default custom_label">No price available</small></button>
                    </td>
                    <td>
                        <!--                        <form action="https://localhost/bundu-admin/reserve-lodging.php" method="post">-->
                        <form id="reserve_form" v-if="result.price != 0 && (searchParam.adult) <= (result.capacity * (result.available-result.rooms))" action="https://localhost/bund/reserve_lodging.php" method="post">
                            <input type="hidden" name="type" :value="result.id">
                            <input type="hidden" name="type_name" :value="result.name">
                            <input type="hidden" name="start_date" value="<?php echo $_REQUEST['start_date'] ?>">
                            <input type="hidden" name="end_date" value="<?php echo $_REQUEST['end_date'] ?>">
                            <input type="hidden" name="start_year" :value="dateSlugs.start_year">

<!--                            <div v-if="searchParam.adult > result.capacity && searchParam.adult <= (result.capacity * result.available)">-->
<!--                                <input type="text" v-if="(result.capacity * result.available)/searchParam.adult != 1" name="total_room" :value="Math.ceil(searchParam.adult/result.capacity)">-->
<!--                                <input type="text" v-else="" name="total_room" :value="result.available">-->
<!--                            </div>-->

<!--                            <div v-if="searchParam.adult <=  result.capacity">-->
<!--                                <input type="hidden" name="total_room" :value="1">-->
<!--                            </div>-->
<!--                            <div v-else="">-->
                                <input type="hidden" name="total_room" v-model="searchParam.total_room">
<!--                                <input type="text" name="total_room" :value="--><?php //if (isset($_REQUEST['total_room'])) { echo $_REQUEST['total_room']; } else { echo $_REQUEST['rooms']; } ?><!--">-->
<!--                            </div>-->

                            <input type="hidden" name="start_month" :value="dateSlugs.start_month">
                            <input type="hidden" name="start_day" :value="dateSlugs.start_day">
                            <input type="hidden" name="nights" :value="result.days">
                            <input type="hidden" name="capacity" :value="result.capacity">
                            <input type="hidden" name="price" :value="result.price">
                            <input type="hidden" name="guests" :value="<?php echo $_REQUEST['adult'] ?>">

                            <button type="submit"  class="btn btn-sm btn-primary custom_label" v-on:click="event.preventDefault();submitForm(index)">Reserve</button>
<!--                            <button type="submit" class="btn btn-sm btn-primary custom_label">Reserve</button>-->
                        </form>
                        <button type="button" v-if="(searchParam.adult)> (result.capacity * (result.available-result.rooms)) || result.price == 0 " class="btn btn-sm btn-primary custom_label"
                                disabled>Reserve
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/vue.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js"
        integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>
<script>



    function selectCheckout() {
        $(".check_out").prop('disabled', false);
        if ($(".check_in")) {
            var tomorrow = new Date($(".check_in").val());
            tomorrow.setDate(tomorrow.getDate() + 1);
            $(".check_out").flatpickr({
                allowInput:true,
                defaultDate: tomorrow,
                minDate: tomorrow, // 1 days from check_in
                altInput: true,
                altFormat: 'd F, Y'
            });
        }
    }

    new Vue({
        el: '#main_wrapper',
        data: {
            Results: [],
            error: null,
            dateSlugs: {
                start_year: '<?php echo date('Y', strtotime($_REQUEST['start_date'])) ?>',
                start_month: '<?php echo date('m', strtotime($_REQUEST['start_date'])) ?>',
                start_day: '<?php echo date('d', strtotime($_REQUEST['start_date'])) ?>',
            },
            searchParam: {
                start_date: '<?php echo $_REQUEST['start_date'] ?>',
                end_date: '<?php echo $_REQUEST['end_date'] ?>',
                adult: '<?php echo $_REQUEST['adult'] ?>',
                total_room: '<?php if (isset($_REQUEST['total_room'])) { echo $_REQUEST['total_room']; } else { echo $_REQUEST['rooms']; } ?>'
            }
        },
        methods: {
            getSearchResult() {
                let THIS = this;
                $.ajax({
                    type: 'POST',
                    url: 'http://localhost/bundu-admin/api/NewApiLodging_price.php',
                    data: THIS.searchParam,
                    success: function (res) {
                        res = JSON.parse(res);
                        console.log(res);
                        if (res.status === 2000) {
                            THIS.Results = res.data;
                          setTimeout(function () {
                                $("#fetchingData").hide();
                            }, 1000)

                        } else {
                            // error message
                            THIS.error = res.error;
                        }
                    }
                });
            },
            submitForm(index){
                var _this = this;
                var rooms = Math.ceil(_this.searchParam.adult/_this.Results[index].capacity);

                if (_this.searchParam.total_room >= rooms && _this.searchParam.total_room <= (_this.Results[index].available-_this.Results[index].rooms) ){

                    swal({
                        title: "Are you sure?",
                        text: "You want to reserve this lodge !!",
                        icon: "warning",
                        button: "Confirm",
                    })
                        .then((value) => {
                            if (value){
                                document.getElementById('reserve_form').submit();
                            }
                        });

                }else if (_this.searchParam.total_room > rooms) {
                    var msg ="You can select "+(_this.Results[index].available-_this.Results[index].rooms)+" rooms maximum!!";
                    swal("Sorry!!!!", msg, "warning", {
                        button: "Try Again",
                    })

                }else{
                    var msg ="You need to select "+rooms+" rooms minimum!!";
                    swal("Sorry!!!!", msg, "warning", {
                        button: "Try Again",
                    })
                }

            }
        },
        mounted() {

            this.getSearchResult();
            console.log(this.dateSlugs);
            setTimeout(function () {
                $('#main_wrapper').fadeIn()
            }, 1000);
            $(".check_in").flatpickr({
                allowInput:true,
                defaultDate: '<?php echo $_REQUEST['start_date'] ?>',
                minDate: '<?php echo date('Y-m-d') ?>',
                altInput: true,
                altFormat: 'd M, Y'
            });
            $(".check_out").flatpickr({
                allowInput:true,
                defaultDate: '<?php echo $_REQUEST['end_date'] ?>',
                minDate: new Date($(".check_in").val()).fp_incr(1), // 1 days from check_in
                altInput: true,
                altFormat: 'd M, Y'
            });

        }
    });

    // -- validate formdata --
    function validate() {
        $('.text-danger').remove();
        if ($('input[name=start_date]').val() == '') {
            _this = $('input[name=start_date]');
            _this.closest('.form-group').append('<span class="text-danger alert_start_date">Please select a check in date!</span>');
        } else {
            _this = $('input[name=start_date]');
            _this.closest('.form-group').find('.alert_start_date').remove();
        }
        if ($('input[name=end_date]').val() == '') {
            _this = $('input[name=end_date]');
            _this.closest('.form-group').append('<span class="text-danger alert_end_date">Please select a check out date!</span>');
        } else {
            _this = $('input[name=end_date]');
            _this.closest('.form-group').find('.alert_end_date').remove();
        }

        if ($('input[name=adult]').val() == '') {
            _this = $('input[name=adult]');
            _this.addClass('is-invalid');
            _this.closest('.form-group').append('<span class="text-danger alert_adult">Please enter number of adult!</span>');
        } else {
            _this = $('input[name=adult]');
            _this.removeClass('is-invalid');
            _this.closest('.form-group').find('.alert_adult').remove();
        }
        if ($('input[name=total_room]').val() == '') {
            _this = $('input[name=total_room]');
            _this.addClass('is-invalid');
            _this.closest('.form-group').append('<span class="text-danger alert_adult">Please enter number of rooms!</span>');
        } else {
            _this = $('input[name=total_room]');
            _this.removeClass('is-invalid');
            _this.closest('.form-group').find('.alert_adult').remove();
        }


    }
</script>
</body>
</html>
