#!/usr/bin/perl
#
# whois search engine
#
# 
# this script does an automatic whois lookup 
# on your UNIX server  -   1997 Caf� Society
# 
# This Script is Freeware And Was Obtained From Cafe Society 
# at http://cafe-society.com
# 
# Many anonymous Perl authors should be thanked for the
# creation of this script as it is made up of snippets
# of code from various different scripts with additional
# code integrated by Cafe Society

# Enter Your Personal Variables

$prevpage = "whois.htm";
$cgiurl = "http://yellowstonelodging.biz/whois/whois.cgi";
$whoimg = "";

# Do not modify script after this point (unless you know what you're doing!)
   
$formdata=<STDIN>;
$formdata=~s/\s+$//;


foreach (split(/&/, $formdata))
{
($name, $value)=split(/=/, $_);
$name=~s/\+/ /g;
$name=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;
$value=~s/\+/ /g;
$value=~s/%([0-9|A-F]{2})/pack(C,hex($1))/eg;

$data{$name}=$value;
}
if ($data{'url'} =~ /[^\w\.-]/)	{
	@whois = ("This is not a valid domain name.\n");
} else	{
	@whois = `whois $data{'url'}`;
}
print "Content-type:text/html\n\n";

print <<EndOfHtml;
<HTML>
<HEAD>
<TITLE>Whois Database Query Page</TITLE>
</HEAD>
<BODY TOPMARGIN="0" LEFTMARGIN="0" BGCOLOR = "#ffffff" TEXT= "#000000" LINK="#FF0000" ALINK="#FF0000" VLINK="FF0000">
<center><p>

<!-- PUT YOU NAME BELOWW -->

<font size=5><b>Your Name Here</b></font>
<p></center>


<TABLE WIDTH="625" ALIGN="CENTER" VALIGN="TOP" BORDER="0" CELLSPACING="0" CELLPADDING="0">
<TR>
<TD>
<!-- THIS IS THE BLUE STRIPE WHICH ANNOYINGLY DOESN'T APPEAR IN NETSCAPE!  GET MSIE3 NOW!-->
</TD>
<TD>
<CENTER>
<BR><BR>
<!-- YOU MAY NEED TO MODIFY THE SIZE OF THE IMAGE ON THE FOLLOWING LINE
     IF YOU USE YOUR OWN -->
<!--<IMG SRC="$whoimg" ALT="Whois Lookup">-->
</CENTER>
<FONT FACE="VERDANA" SIZE="1"><!--VERDANA IS INSTALLED AS STANDARD WITH MSIE3! GET IT NOW!-->
<B>Result of Previous Search At Foot Of This Page<br>Perform a New Search(?):</B></FONT><FONT FACE="VERDANA" SIZE="2">

<!--Make sure you change the following action="" statement to reflect the location of the whois.pl script in your cgi-bin-->
<ul><FORM ACTION="$cgiurl" METHOD="POST">

<li>Domain Name:<br>
WWW.<INPUT TYPE="text" SIZE="25" NAME="url"></li><br>&nbsp</br>
<li><INPUT TYPE="submit" name="B1" value="Click Here"> To Search The Whois Database</li>
</FORM></ul>

<!-- Make sure to change the A HREF="" statement on the next line to reflect the page which called this script-->
<ul><li><A HREF="$prevpage">Return To Previous Page</a></li></ul>
<h2 align="center">whois $data{'url'}</h2>
</TD>
</TR>
</FONT>
</TABLE>
</BODY>
</HTML>
EndOfHtml
    ;

print "<pre>";
print @whois;
print "</pre>";
