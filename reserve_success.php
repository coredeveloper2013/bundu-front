<?
error_reporting(0);
@date_default_timezone_set('America/Denver');


@ini_set("session.gc_maxlifetime","10800");
if(!isset($_SESSION)){ session_start();	}

//echo $_GET[id];



echo '<HTML>

<HEAD>
	<TITLE>Yellowstone Lodging Reservations</TITLE>
	<style><!--
	.res1 { font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #000000; }
	.check_in, .check_out {background-color: initial !important;}
	--></style>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css" integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous" />
</HEAD>

<BODY BGCOLOR="#E1E1E1" TEXT="#000000" TOPMARGIN="0" style="background-color: #E1E1E1">

<CENTER>'."\n\n\n\n";


echo '<br> <div class="col-md-7">
<TABLE BORDER="0" WIDTH="'.$fullwidth. '" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF">
<TR>
<TD ALIGN="center">
	<div class="jumbotron text-center">
            <h1 style="color: #FFA500">Yellowstone lodging <br></h1>
            <h2 style="color: #ff9301">Reserve Successful</h2>
        </div>' ."\n";

echo "<div id='successMessage'>";
echo '<FONT FACE="Arial" SIZE="3"><BR><span class="alert alert-info">Your order number: <B class="reserve_id">'.$_GET[id].'</B></span><BR><BR><div style="padding: 30px 50px;">Thank you for your order.  If you placed it during regular business hours in the USA you can expect an email response from us within two hours.    If the order were placed outside regular business hours, we will be in touch when we open in the morning.<BR><BR>Please note that this order is not confirmed until you receive an email confirmation from us.  <SPAN STYLE="color:#cc0000;">Please do not make any arrangements that are dependent on this lodging until we send you a confirmation.</div></SPAN><BR><BR></FONT>'."\n\n";
echo '<a class="btn btn-success" href="/bund/index.php"><i></i>Back</a><Br>';
echo '<FONT FACE="Arial" SIZE="3"><BR>The system encountered an error while processing your order.<BR>We apologize for the inconvenience.<BR>Please contact us to complete your reservation.<BR><B>Toll Free: (866) 646-1118</B><BR>Your order number: <B class="reserve_id">'.$_GET[id].'</B><BR><BR></FONT>'."\n\n";
echo "</div>";
echo '</TD></TR></TABLE></div>'

?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js" integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>
</CENTER>
</BODY>

</HTML>