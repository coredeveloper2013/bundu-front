<!DOCTYPE html>
<html lang="en">
<head>
    <title>Yellowstone lodging, West Yellowstone motel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="jumbotron text-center">
            <h1>Yellowstone studios </h1>
        </div>
        <div>
            <div class="carousel carousel-slider">
                <a class="carousel-item" href="#one!"><img src="studio3-3.jpg"></a>
                <a class="carousel-item" href="#two!"><img src="apart2-2.jpg"></a>
                <a class="carousel-item" href="#three!"><img src="apt6-3.jpg"></a>
                <a class="carousel-item" href="#four!"><img src="yellowstone-two-bedroom-apartment-2.jpg"></a>
                <a class="carousel-item" href="#five!"><img src="west-yellowstone-studio-8_10.jpg"></a>
                <a class="carousel-item" href="#five!"><img src="6-yellowstone-apartment-4.jpg"></a>
            </div>
            <div class="d-flex justify-content-center">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus.</p>
            </div>
            <h4 align="center">Nightly Rates</h4>
            <table class="table table-bordered table-striped highlight table-responsive">
                <thead>
                <tr>
                    <th colspan="2">Nov 3 - Apr 15</th>
                    <th colspan="2">Apr 16 - May 14</th>
                    <th colspan="2">May 15 - May 27</th>
                    <th colspan="2">May 28 - Jun 24</th>
                    <th colspan="2">Jun 25 - Sep 5</th>
                    <th colspan="2">Sep 6 - season end</th>
                </tr>
                <tr>
                    <th>Nights</th>
                    <th>Price</th>
                    <th>Nights</th>
                    <th>Price</th>
                    <th>Nights</th>
                    <th>Price</th>
                    <th>Nights</th>
                    <th>Price</th>
                    <th>Nights</th>
                    <th>Price</th>
                    <th>Nights</th>
                    <th>Price</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td>1</td>
                    <td>$99</td>
                    <td>1</td>
                    <td>$130</td>
                    <td>1</td>
                    <td>$138</td>
                    <td>1</td>
                    <td>$199</td>
                    <td>1</td>
                    <td>$225</td>
                    <td>1</td>
                    <td>$185</td>
                </tr>
                <tr>
                    <td>2 to 6</td>
                    <td>$54</td>
                    <td>2 to 6</td>
                    <td>$69</td>
                    <td>2 to 6</td>
                    <td>$85</td>
                    <td>2 to 6</td>
                    <td>$159</td>
                    <td>2 to 6</td>
                    <td>$189</td>
                    <td>2 to 6</td>
                    <td>$145</td>
                </tr>
                <tr>
                    <td>7 & up</td>
                    <td>$49</td>
                    <td>7 & up</td>
                    <td>$64</td>
                    <td>7 & up</td>
                    <td>$79</td>
                    <td>7 & up</td>
                    <td>N/A</td>
                    <td>7 & up</td>
                    <td>N/A</td>
                    <td>7 & up</td>
                    <td>N/A</td>
                </tr>
                </tbody>
            </table>
            <div class="submit_button"><a href="reserve_lodging.php" align="center" class="btn waves-effect waves-light" type="submit" name="action">Reserve
                <i class="material-icons right"></i>
                </a></div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script>
    $(document).ready(function(){
        $('.carousel').carousel({
            fullWidth: true,
            indicators: true
        });
    });
</script>
</body>
</html>
